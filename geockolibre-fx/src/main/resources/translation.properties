###
# #%L
# This file is part of GEOCKOLIBRE
# ==================================================
# Copyright (C) 2016 GEOCKOLIBRE
# ==================================================
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/gpl-3.0.html>.
# #L%
###
logindialog.title = Geocaching.com login
logindialog.userName = Username:
logindialog.password = Password:
logindialog.rememberMe = Remember login
logindialog.rememberMeWarning = Beware: If "Remember login" is checked, your password will be stored in a local file. \
 This file is hard to read but not impossible to read. 
logindialog.ok = Login
logindialog.cancel = Cancel

messagedialog.ok = Ok
messagedialog.cancel = Cancel
messagedialog.yes = Yes
messagedialog.no = No

size.NANO = Nano
size.MICRO = Micro
size.SMALL = Small
size.REGULAR = Regular
size.LARGE = Large
size.OTHER = Other
size.UKNOWN = Unknown
size.VIRTUAL = Virtual
size.NOT_CHOSEN = Not Chosen

loginProgress.title = Logging in
loginProgress.logginIn = Checking login ...
loginProgress.loadingUserDetails = Loading user details ...
login.failed = Username or password incorrect
geockolibre.exit = Exit
geockolibre.search = Search

main.title = Geockolibre - Logged in as {0}
searchLocation.title = Cache search
searchLocation.simpleLocation = Location:
location.invalid.tooltip = Location must be in one of the following formats:\n\
	MinDec (e.g. N 46\u00B0 28.222' W 063\u00B0 30.956')\n\
	DegDec (e.g. 46.47037 -63.51593)\n\
	DMS (e.g. N 46\u00B0 28' 13.332" W 63\u00B0 30' 57.348")\n\
	The degree, minute, and second symbols are optional.

searchLocation.useHomeLocation = Use home location

searchLocation.fileLocation = File:
searchLocation.selectFile = Select file ...
searchLocation.file.content = File {0} contains {1} track(s), {2} route(s) and {3} waypoint(s), summing to {4} locations and a total length of {5} km
searchLocation.selectInputFile = Select GPX file
searchLocation.errorFile.title = Error reading file
searchLocation.errorFile.message = Error reading file {0}
searchLocation.errorFile.content = Exception {1}

searchLocation.radius = Search radius:
searchLocation.km = km
searchLocation.miles = miles
searchLocation.maxResults = Maximum results:
searchLocation.filterDifficultyTerrain = Filter Difficulty/Terrain
searchLocation.filterSize = Filter size
searchLocation.filterType = Filter type
searchLocation.placedOnOrAfter = Placed on or after
searchLocation.placedOnOrBefore = Placed on or before
searchLocation.foundInLast = Found in last
searchLocation.foundInLastDays = days
searchLocation.hasNotBeenFound = Has not been found
searchLocation.ignoreOwn = Ignore own
searchLocation.ignoreFound = Ignore my finds
searchLocation.ignoreDisabled = Ignore disabled
searchLocation.containsTrackable = Contains trackable
searchLocation.hasAtLeastFavorites = Has at least
searchLocation.points = favourite points
searchLocation.containsKeyword = Title contains keyword
searchLocation.includeFullLogs = Include full logs in output
searchLocation.nrLogs = Logs to include
searchLocation.editFilter = Edit
searchLocation.filterAttributes = Filter attributes
searchLocation.filterSize = Filter size
searchLocation.filterType = Filter type

difficultyTerrain.title = Filter on Difficulty/Terrain
difficultyTerrain.terrain = Terrain
difficultyTerrain.difficulty = Difficulty
difficultyTerrain.selectAll = Select all
difficultyTerrain.invert = Invert selection
difficultyTerrain.cancel = Cancel
difficultyTerrain.ok = Ok

attributes.title = Filter on attributes
attributes.attribute = Attribute
attributes.include = Include
attributes.exclude = Exclude
attributes.ignore = Ignore
attributes.cancel = Cancel
attributes.ok = Ok

searchHides.title = My hides
searchHides.user.title = Whose cache hides to download:
searchHides.user.me = mine
searchHides.user.other = user:
searchHides.nrOfCaches.title = How many caches to download:
searchHides.nrOfCaches.all = all
searchHides.nrOfCaches.last = last
searchHides.nrOfCaches.since = all found since
searchHides.includeFullLogs = Include full logs in output

searchFinds.title = My finds
searchFinds.user.title = Whose cache finds to download:
searchFinds.user.me = mine
searchFinds.user.other = user:
searchFinds.nrOfCaches.title = How many caches to download:
searchFinds.nrOfCaches.all = all
searchFinds.nrOfCaches.last = last
searchFinds.nrOfCaches.since = all found since

search.title = About to search for caches
search.populating = Populating {0} caches ...
search.found = Found {0} caches matching criteria
search.found.withMax = Found {0} caches of {1} matching criteria
search.progressDistance = Searched {0}km of total distance {1}km
search.downloading = Downloading cache {0} - {1}
search.progressCount = Checking cache {0} of {1}

searchErrorDialog.cancel = Stop search
searchErrorDialog.ignore = Ignore once
searchErrorDialog.ignoreAll = Ignore all errors
searchErrorDialog.message = Failed importing cache {1}: \n\
   {0}
searchErrorDialog.title = Failed importing cache {0}

searchResultdialog.showOnMap = Show on map
searchResultdialog.openInBrowser = Open in browser
searchResultdialog.saveFile = Save single .gpx file
searchResultdialog.saveFolder = Save in folder
searchResultdialog.done = Done
searchResultDialog.found = Found {0} caches.
searchResultDialog.selectOutputFile = Select file to write to
searchResultDialog.selectOutputFolder = Select folder to write caches to
searchResultDialog.overwrite.title = Overwrite existing files?
searchResultDialog.overwrite.message = {0} of the {1} caches already exist in this folder.
searchResultDialog.overwrite.question = Do you wish to overwrite?

searchResultDialog.openBrowser.title = Opening a large number of caches.
searchResultDialog.openBrowser.header = Are you sure you want to open {0} caches?
searchResultDialog.openBrowser.warning = You have asked to open {0} caches.\n\
	Such a large number of open windows or tabs can cause your computer to slow down or \
	even crash your browser.\n\
	Do you still want do do it?
	
mapGenerator.title = Generating map
mapGenerator.generating = Generating description...
mapGenerator.compressing = Compressing description...
mapGenerator.uploading = Uploading description...
mapGenerator.reading = Reading map...
mapGenerator.openingBrowser = Redirecting browser window...