package org.geockolibre.fx;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.geockolibre.scrape.Attribute;
import org.geockolibre.scrape.Cache;
import org.geockolibre.scrape.CacheSize;
import org.geockolibre.scrape.CacheType;
import org.geockolibre.scrape.DifficultyTerrain;
import org.geockolibre.scrape.DistanceUnitEnum;
import org.geockolibre.scrape.InclusionEnum;
import org.geockolibre.scrape.ListSearcher;
import org.geockolibre.scrape.Location;
import org.geockolibre.scrape.Login;
import org.geockolibre.scrape.MultiListSearcher;
import org.geockolibre.scrape.MultiSearchProgressListener;
import org.geockolibre.scrape.ResultFilter;
import org.geockolibre.scrape.SearchProgressListener;
import org.geockolibre.scrape.Searcher;

public class LocationResultFilter implements ResultFilter {
	private Login login;
	private List<List<Location>> locations;
	private Double distance;
	private DistanceUnitEnum distanceUnit;
	private int maxCaches;
	private boolean checkFavouritePoints;
	private int minFavPoints;
	private boolean checkPlacedOnOrAfter;
	private LocalDate placedOnOrAfter;
	private boolean checkPlacedOnOrBefore;
	private LocalDate placedOnOrBefore;
	
	private boolean containsTrackable;
	
	private boolean checkFoundDate;
	private LocalDate lastFoundDate;
	private boolean notFound;
	
	private boolean ignoreFinds;
	private boolean ignoreOwn;
	private boolean ignoreDisabled;
	
	private boolean checkDT;
	private Collection<DifficultyTerrain> acceptedDts = new HashSet<>();
	private boolean checkSize;
	private Collection<CacheSize> acceptedSizes = EnumSet.noneOf(CacheSize.class);
	private boolean checkType;
	private Collection<CacheType> acceptedTypes = EnumSet.noneOf(CacheType.class);
	private boolean checkAttributes;
	private Map<Attribute, InclusionEnum> attributes = new EnumMap<>(Attribute.class);
	private boolean checkKeyword;
	private String keyword;
	private int numberOfLogs;
	private Collection<String> downloaded = ConcurrentHashMap.newKeySet(); 


	@Override
	public Searcher createSearcher(Login login, SearchProgressListener spl) {
		downloaded.clear();
		if (locations.size()==1 && locations.get(0).size()==1) {
			return ResultFilter.super.createSearcher(login, spl);
		} else {
			return new MultiListSearcher(login, (MultiSearchProgressListener)spl);
		}
	}
	
	@Override
	public void startSearch(Searcher listSearcher) {
		double radius = distance;
		// convert miles to km if we need to
		boolean useMiles = distanceUnit==DistanceUnitEnum.MILES;
		if (useMiles) {
			radius *= 1.609344;
		}
		// check if we ignore our own caches
		boolean ignoreFoundAndOwn = ignoreFinds & ignoreOwn;
		
		try {
			if (locations.size()==1 && locations.get(0).size()==1) {
				((ListSearcher)listSearcher).findCachesCloseTo(locations.get(0).get(0), radius, ignoreFoundAndOwn, 0, false);
			} else {
				((MultiListSearcher)listSearcher).findCachesCloseTo(locations, radius, ignoreFoundAndOwn, 0, false);
			}
		} catch (IOException | ParseException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public int getMaxFind() {
		return maxCaches;
	}
	@Override
	public boolean checkPreDownload(Cache cache) {
		// Already checked
		if (!downloaded.add(cache.getCacheCode())) {
			return false;
		}
		// check things that can be checked before complete cache download:
		// - number of favourite points
		if (checkFavouritePoints && cache.getFavourited() < minFavPoints) {
			return false;
		}
		// - placement date on or after
		if (checkPlacedOnOrAfter || checkPlacedOnOrBefore) {
			LocalDate placedAt=
				    Instant.ofEpochMilli(cache.getPlacedAt()).atZone(ZoneId.systemDefault()).toLocalDate();			
			if (checkPlacedOnOrAfter && placedAt.isBefore(placedOnOrAfter)) {
				return false;
			}
			// - placement date on or before
			if (checkPlacedOnOrBefore && placedAt.isAfter(placedOnOrBefore)) {
				return false;
			}
		}
		// - trackables
		if (containsTrackable && cache.getTrackableCount() == 0) {
			return false;
		}
		// - disabled or not
		if (ignoreDisabled && cache.isDisabled()) {
			return false;
		}
		// - owned or not
		if (ignoreOwn && cache.getOwner().getName().equals(login.getUserName())) {
			return false;
		}
		// - found by us or not
		if (ignoreFinds && cache.isFoundByUser()) {
			return false;
		}
		// - last find date or not found at all
		if (notFound && cache.getLastFoundDate() != null) {
			return false;
		}
		if (checkFoundDate) {
			if (cache.getLastFoundDate() == null) {
				return false;
			}
			LocalDate cacheLastFound =
				    Instant.ofEpochMilli(cache.getLastFoundDate()).atZone(ZoneId.systemDefault()).toLocalDate();			
			if (cacheLastFound.isBefore(lastFoundDate)) {
				return false;
			}
		}
		// - type
		if (checkType) {
			if (!this.acceptedTypes.contains(cache.getCacheType())) {
				return false;
			}
		}
		if (ignoreDisabled) {
			if (cache.getAttributes().contains(Attribute.FIRSTAID_YES)) {
				return false;
			}
		}
		
		// check if the "needs maintenance" attribute is checked for
		if (checkAttributes) {
		}
		// keyword matching
		if (checkKeyword) {
			// check the title for keywords
			if (keyword != null) {
				if (!cache.getName().toLowerCase().contains(keyword.toLowerCase())) {
					return false;
				}
			}
		}
		// - D/T
		if (checkDT) {
			String dtString = cache.getDifficultyRating() + "/" + cache.getTerrainRating();
			dtString = dtString.replaceAll("\\.0", "");
			DifficultyTerrain dt = DifficultyTerrain.parse(dtString);
			if (!this.acceptedDts.contains(dt)) {
				return false;
			}
		}
		// - size
		if (checkSize) {
			if (!this.acceptedSizes.contains(cache.getCacheSize())) {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean checkPostDownload(Cache cache) {
		// check things that can only be checked after complete cache download:
		// - check that it's not a premium cache that we don't have access to
		if (cache.isUnavailableToUs())
		{
			return false;
		}
		// - attributes
		if (checkAttributes) {
			Set<Attribute> cacheAttributes = cache.getAttributes();
			// check that none of the excluded attributes exist
			for (Attribute a : cacheAttributes) {
				if (attributes.get(a) == InclusionEnum.EXCLUDE) {
					return false;
				}
			}
			
			// check that all included attributes exist
			if (attributes.entrySet().stream().filter(e->e.getValue()==InclusionEnum.INCLUDE && 
					!cacheAttributes.contains(e.getKey())).findAny().isPresent()) {
				return false;
			}
		}

		try {
			while(cache.getLogs().size()<numberOfLogs && cache.retrieveMoreLogs(login, 
					numberOfLogs - cache.getLogs().size())) {
					// just loop until we run out of logs
			}
			cache.limitLogs(numberOfLogs);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public int getNumberOfLocations() {
		return locations==null?0:locations.stream().map(List::size).reduce(0, Integer::sum);
	}

	public void setLogin(Login login) {
		this.login = login;
	}
	public void setLocation(Location location) {
		this.locations = Collections.singletonList(Collections.singletonList(location));
	}
	public void setLocations(List<List<Location>> locations) {
		this.locations = locations;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

	public void setDistanceUnit(DistanceUnitEnum distanceUnit) {
		this.distanceUnit = distanceUnit;
	}

	public void setMaxCaches(int maxCaches) {
		this.maxCaches = maxCaches;
	}

	public void setCheckFavouritePoints(boolean checkFavouritePoints) {
		this.checkFavouritePoints = checkFavouritePoints;
	}

	public void setMinFavPoints(int minFavPoints) {
		this.minFavPoints = minFavPoints;
	}

	public void setCheckPlacedOnOrAfter(boolean checkPlacedOnOrAfter) {
		this.checkPlacedOnOrAfter = checkPlacedOnOrAfter;
	}

	public void setPlacedOnOrAfter(LocalDate placedOnOrAfter) {
		this.placedOnOrAfter = placedOnOrAfter;
	}

	public void setCheckPlacedOnOrBefore(boolean checkPlacedOnOrBefore) {
		this.checkPlacedOnOrBefore = checkPlacedOnOrBefore;
	}

	public void setPlacedOnOrBefore(LocalDate placedOnOrBefore) {
		this.placedOnOrBefore = placedOnOrBefore;
	}

	public void setContainsTrackable(boolean containsTrackable) {
		this.containsTrackable = containsTrackable;
	}

	public void setCheckFoundDate(boolean checkFoundDate) {
		this.checkFoundDate = checkFoundDate;
	}

	public void setLastFoundDate(LocalDate lastFoundDate) {
		this.lastFoundDate = lastFoundDate;
	}

	public void setNotFound(boolean notFound) {
		this.notFound = notFound;
	}

	public void setIgnoreFinds(boolean ignoreFinds) {
		this.ignoreFinds = ignoreFinds;
	}

	public void setIgnoreOwn(boolean ignoreOwn) {
		this.ignoreOwn = ignoreOwn;
	}

	public void setIgnoreDisabled(boolean ignoreDisabled) {
		this.ignoreDisabled = ignoreDisabled;
	}

	public void setCheckDT(boolean checkDT) {
		this.checkDT = checkDT;
	}

	public void setAcceptedDts(Collection<DifficultyTerrain> acceptedDts) {
		this.acceptedDts = acceptedDts;
	}

	public void setCheckSize(boolean checkSize) {
		this.checkSize = checkSize;
	}

	public void setAcceptedSizes(Collection<CacheSize> acceptedSizes) {
		this.acceptedSizes = acceptedSizes;
	}

	public void setCheckType(boolean checkType) {
		this.checkType = checkType;
	}

	public void setAcceptedTypes(Collection<CacheType> acceptedTypes) {
		this.acceptedTypes = acceptedTypes;
	}

	public void setCheckAttributes(boolean checkAttributes) {
		this.checkAttributes = checkAttributes;
	}

	public void setAttributes(Map<Attribute, InclusionEnum> attributes) {
		this.attributes = attributes;
	}

	public void setCheckKeyword(boolean checkKeyword) {
		this.checkKeyword = checkKeyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public void setMinNumberOfLogs(int minNumberOfLogs) {
		this.numberOfLogs = minNumberOfLogs;
	}
}
