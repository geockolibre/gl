package org.geockolibre.fx;

import java.util.Collection;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.geockolibre.scrape.DifficultyTerrain;
import org.geockolibre.scrape.DifficultyTerrain.Grade;
import org.springframework.context.MessageSource;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;

public class FilterDifficultyTerrainDialog extends AbstractDialogController {
	@Inject
	private MessageSource messageSource;

	@FXML GridPane checkboxGrid;
	@FXML Button selectAllButton;
	@FXML Button invertButton;
	@FXML Button cancelButton;
	@FXML Button okButton;
	
	private Map<Grade, CheckBox> difficultyAll = new EnumMap<>(Grade.class);
	private Map<Grade, CheckBox> terrainAll = new EnumMap<>(Grade.class);
	private Map<DifficultyTerrain, CheckBox> checkBoxes = new HashMap<>();
	
	private Consumer<Set<DifficultyTerrain>> callback;
	
	public void initialize() {
		selectAllButton.setOnAction(e->selectAll());
		invertButton.setOnAction(e->invert());
		cancelButton.setOnAction(e->close());
		okButton.setOnAction(e->finishSelection());
		
		checkboxGrid.getColumnConstraints().add(new ColumnConstraints(80, 40, 90, Priority.SOMETIMES, HPos.LEFT, false));
		for (Grade terr:Grade.values()) {
			CheckBox terrainAllCb = new CheckBox("T="+terr);
			terrainAll.put(terr, terrainAllCb);
			terrainAllCb.setOnAction(e->selectAllForTerrain(terr));
			checkboxGrid.add(terrainAllCb, terr.ordinal()+1, 0);
			checkboxGrid.getColumnConstraints().add(new ColumnConstraints(80, 40, 160, Priority.SOMETIMES, HPos.LEFT, true));
		}
		for (Grade diff:Grade.values()) {
			CheckBox difficultyAllCb = new CheckBox("D="+diff);
			difficultyAll.put(diff, difficultyAllCb);
			difficultyAllCb.setOnAction(e->selectAllForDifficulty(diff));
			checkboxGrid.add(difficultyAllCb, 0, diff.ordinal()+1);
			for (Grade terr:Grade.values()) {
				DifficultyTerrain dt = new DifficultyTerrain(diff, terr);
				CheckBox cb = new CheckBox(dt.toString());
				cb.setOnAction(e->checkAllDiffTerrain());
				checkBoxes.put(dt, cb);
				checkboxGrid.add(cb, terr.ordinal()+1, diff.ordinal()+1);
			}
		}
	}
	
	private void checkAllDiffTerrain() {
		EnumSet<Grade> fullDiffs = EnumSet.allOf(Grade.class); 
		EnumSet<Grade> fullTerrains = EnumSet.allOf(Grade.class);
		checkBoxes.forEach((d,c)-> {
			if (!c.isSelected()) {
				fullDiffs.remove(d.getDifficulty());
				fullTerrains.remove(d.getTerrain());
			}
		});
		difficultyAll.forEach((d,c)->c.setSelected(fullDiffs.contains(d)));
		terrainAll.forEach((t,c)->c.setSelected(fullTerrains.contains(t)));
	}
	
	private void selectAllForDifficulty(Grade grade) {
		final boolean selected = difficultyAll.get(grade).isSelected();
		checkBoxes.forEach((dt,c)-> {
			if (dt.getDifficulty().equals(grade)) {
				c.setSelected(selected);
			}
		});
		checkAllDiffTerrain();
	}
	private void selectAllForTerrain(Grade grade) {
		final boolean selected = terrainAll.get(grade).isSelected();
		checkBoxes.forEach((dt,c)-> {
			if (dt.getTerrain().equals(grade)) {
				c.setSelected(selected);
			}
		});
		checkAllDiffTerrain();
	}

	private void selectAll() {
		checkBoxes.forEach((dt,c)->c.setSelected(true));
		checkAllDiffTerrain();
	}
	
	private void invert() {
		checkBoxes.forEach((dt,c)->c.setSelected(!c.isSelected()));
		checkAllDiffTerrain();
	}
	
	
	public void show(Collection<DifficultyTerrain> selected, 
				Consumer<Set<DifficultyTerrain>> callback) {
		this.callback = callback;
		checkBoxes.forEach((dt,c)-> c.setSelected(selected.contains(dt)));
		checkAllDiffTerrain();
		super.show(true);
	}
	@Override
	protected void initDialogStage(Stage stage) {
		super.initDialogStage(stage);
		stage.setTitle(messageSource.getMessage("difficultyTerrain.title", null,null));
	}
	
	
	public void finishSelection() {
		Set<DifficultyTerrain> selected = checkBoxes.entrySet().stream().filter(e->e.getValue().isSelected()).map(e->e.getKey()).collect(Collectors.toSet());
		close();
		callback.accept(selected);
	}
}
