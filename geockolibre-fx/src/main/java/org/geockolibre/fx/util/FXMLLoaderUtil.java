package org.geockolibre.fx.util;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.io.InputStream;
import java.util.ResourceBundle;

import javax.inject.Inject;
import javax.inject.Named;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.util.Callback;

@Named
public class FXMLLoaderUtil {
	@Inject
	private ResourceBundle resourceBundle;
    @SuppressWarnings("unchecked")
	public <T extends Controller> NodeWithController<T> loadForController(String resource, T controller) {
        try (InputStream fxmlStream = FXMLLoaderUtil.class.getResourceAsStream(resource)) {
            final FXMLLoader loader = new FXMLLoader();
            loader.setResources(resourceBundle);
            loader.setControllerFactory(c->controller);
            return new NodeWithController<T>(loader.load(fxmlStream), (T)loader.getController());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public <T extends Controller> NodeWithController<T> load(String resource, Callback<Class<?>, Object> controllerFactory) {
        try (InputStream fxmlStream = FXMLLoaderUtil.class.getResourceAsStream(resource)) {
            final FXMLLoader loader = new FXMLLoader();
            loader.setControllerFactory(controllerFactory);
            loader.setResources(resourceBundle);
            return new NodeWithController<T>(loader.load(fxmlStream), loader.getController());

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static final class NodeWithController<T extends Controller> {
        final Node node;
        final T controller;

        public NodeWithController(Node node, T controller) {
            this.node = node;
            this.controller = controller;
            this.controller.setView(node);
        }
        public T getController() {
            return controller;
        }
        public Node getNode() {
            return node;
        }
    }
}
