package org.geockolibre.fx.util;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import org.geockolibre.scrape.util.concurrent.Execution.ExecutionState;
import org.geockolibre.scrape.util.concurrent.ProgressListener;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ProgressController<V> implements ProgressListener {
    /**
     * Used to send workDone updates in a thread-safe manner from the subclass
     * to the FX application thread and workDone related properties. AtomicReference
     * is used so as to coalesce updates such that we don't flood the event queue.
     */
    private AtomicReference<ProgressUpdate> progressUpdate = new AtomicReference<>();

    /**
     * Used to send message updates in a thread-safe manner from the subclass
     * to the FX application thread. AtomicReference is used so as to coalesce
     * updates such that we don't flood the event queue.
     */
    private AtomicReference<String> messageUpdate = new AtomicReference<>();

    /**
     * Used to send message updates in a thread-safe manner from the subclass
     * to the FX application thread. AtomicReference is used so as to coalesce
     * updates such that we don't flood the event queue.
     */
    private AtomicReference<String> actionUpdate = new AtomicReference<>();

    /**
     * Used to send title updates in a thread-safe manner from the subclass
     * to the FX application thread. AtomicReference is used so as to coalesce
     * updates such that we don't flood the event queue.
     */
    private AtomicReference<String> titleUpdate = new AtomicReference<>();

    /**
     * Used to send value updates in a thread-safe manner from the subclass
     * to the FX application thread. AtomicReference is used so as to coalesce
     * updates such that we don't flood the event queue.
     */
    private AtomicReference<ExecutionState> stateUpdate = new AtomicReference<>();
    
    private AtomicBoolean cancelled = new AtomicBoolean(); 
    
    private ObjectProperty<Boolean> cancelledProperty = new SimpleObjectProperty<>(this, "cancelled", false);
    public final ReadOnlyObjectProperty<Boolean> cancelledProperty() { checkThread(); return cancelledProperty; }

    private ObjectProperty<ExecutionState> state = new SimpleObjectProperty<>(this, "state", ExecutionState.NONE);
    public final ExecutionState getState() { checkThread(); return state.get(); }
    public final ReadOnlyObjectProperty<ExecutionState> stateProperty() { checkThread(); return state; }
    
    private final DoubleProperty workDone = new SimpleDoubleProperty(this, "workDone", -1);
    public final double getWorkDone() { checkThread(); return workDone.get(); }
    public final ReadOnlyDoubleProperty workDoneProperty() { checkThread(); return workDone; }

    private final DoubleProperty totalWork = new SimpleDoubleProperty(this, "totalWork", -1);
    public final double getTotalWork() { checkThread(); return totalWork.get(); }
    public final ReadOnlyDoubleProperty totalWorkProperty() { checkThread(); return totalWork; }

    private final DoubleProperty progress = new SimpleDoubleProperty(this, "progress", -1);
    private void setProgress(double value) { checkThread(); progress.set(value); }
    public final double getProgress() { checkThread(); return progress.get(); }
    public final ReadOnlyDoubleProperty progressProperty() { checkThread(); return progress; }

    private final BooleanProperty running = new SimpleBooleanProperty(this, "running", false);
    public final boolean isRunning() { checkThread(); return running.get(); }
    public final ReadOnlyBooleanProperty runningProperty() { checkThread(); return running; }

    private final StringProperty message = new SimpleStringProperty(this, "message", "");
    public final String getMessage() { checkThread(); return message.get(); }
    public final ReadOnlyStringProperty messageProperty() { checkThread(); return message; }

    private final StringProperty action = new SimpleStringProperty(this, "action", "");
    public final String getAction() { checkThread(); return action.get(); }
    public final ReadOnlyStringProperty actionProperty() { checkThread(); return action; }

    private final StringProperty title = new SimpleStringProperty(this, "title", "");
    public final String getTitle() { checkThread(); return title.get(); }
    public final ReadOnlyStringProperty titleProperty() { checkThread(); return title; }
    
    /**
     * Updates the <code>message</code> property. Calls to updateMessage
     * are coalesced and run later on the FX application thread, so calls
     * to updateMessage, even from the FX Application thread, may not
     * necessarily result in immediate updates to this property, and
     * intermediate message values may be coalesced to save on event
     * notifications.
     * <p>
     *     <em>This method is safe to be called from any thread.</em>
     * </p>
     *
     * @param message the new message
     */
    public void updateMessage(String message) {
        if (Platform.isFxApplicationThread()) {
            this.message.set(message);
        } else if (messageUpdate.getAndSet(message) == null) {
        	Platform.runLater(() -> {
                ProgressController.this.message.set(messageUpdate.getAndSet(null));
            });
        }
    }

    /**
     * Updates the <code>message</code> property. Calls to updateMessage
     * are coalesced and run later on the FX application thread, so calls
     * to updateMessage, even from the FX Application thread, may not
     * necessarily result in immediate updates to this property, and
     * intermediate message values may be coalesced to save on event
     * notifications.
     * <p>
     *     <em>This method is safe to be called from any thread.</em>
     * </p>
     *
     * @param message the new message
     */
    public void updateAction(String action) {
        if (Platform.isFxApplicationThread()) {
            this.action.set(action);
        } else if (actionUpdate.getAndSet(action) == null) {
        	Platform.runLater(() -> {
                ProgressController.this.action.set(actionUpdate.getAndSet(null));
            });
        }
    }

    /**
     * Updates the <code>title</code> property. Calls to updateTitle
     * are coalesced and run later on the FX application thread, so calls
     * to updateTitle, even from the FX Application thread, may not
     * necessarily result in immediate updates to this property, and
     * intermediate title values may be coalesced to save on event
     * notifications.
     * <p>
     *     <em>This method is safe to be called from any thread.</em>
     * </p>
     *
     * @param title the new title
     */
    public void updateTitle(String title) {
        if (Platform.isFxApplicationThread()) {
            this.title.set(title);
        } else if (titleUpdate.getAndSet(title) == null) {
        	Platform.runLater(() -> {
                ProgressController.this.title.set(titleUpdate.getAndSet(null));
            });
        }
    }

    /**
     * Updates the <code>title</code> property. Calls to updateTitle
     * are coalesced and run later on the FX application thread, so calls
     * to updateTitle, even from the FX Application thread, may not
     * necessarily result in immediate updates to this property, and
     * intermediate title values may be coalesced to save on event
     * notifications.
     * <p>
     *     <em>This method is safe to be called from any thread.</em>
     * </p>
     *
     * @param title the new title
     */
    public void updateState(ExecutionState state) {
        if (Platform.isFxApplicationThread()) {
            this.state.set(state);
        } else if (stateUpdate.getAndSet(state) == null) {
        	Platform.runLater(() -> {
                ProgressController.this.state.set(stateUpdate.getAndSet(null));
            });
        }
    }
    
    /*
     * IMPLEMENTATION
     */
    private void checkThread() {
        if (!Platform.isFxApplicationThread()) {
            throw new IllegalStateException("Task must only be used from the FX Application Thread");
        }
    }

	@Override
	public void setCancelled() {
		if (!cancelled.getAndSet(true)) {
	        if (Platform.isFxApplicationThread()) {
	            this.cancelledProperty.set(true);
	        } else {
	        	Platform.runLater(() -> {
	                ProgressController.this.cancelledProperty.set(true);
	            });
	        }
		}
	}

	@Override
	public boolean isCanceled() {
		return cancelled.get();
	}
    /**
     * A struct like class that contains the last workDone update information.
     * What we do when updateProgress is called, is we create a new ProgressUpdate
     * object and store it. If it was null, then we fire off a new Runnable
     * using RunLater, which will eventually read the latest and set it to null
     * atomically. If it was not null, then we simply update it.
     */
    private static final class ProgressUpdate {
        private final double workDone;
        private final double totalWork;

        private ProgressUpdate(double p, double m) {
            this.workDone = p;
            this.totalWork = m;
        }
    }
	@Override
	public void updateProgress(double workDone, double max) {
        // Adjust Infinity / NaN to be -1 for both workDone and max.
        if (Double.isInfinite(workDone) || Double.isNaN(workDone)) {
            workDone = -1;
        }

        if (Double.isInfinite(max) || Double.isNaN(max)) {
            max = -1;
        }

        if (workDone < 0) {
            workDone = -1;
        }

        if (max < 0) {
            max = -1;
        }

        // Clamp the workDone if necessary so as not to exceed max
        if (workDone > max) {
            workDone = max;
        }

        if (Platform.isFxApplicationThread()) {
            _updateProgress(workDone, max);
        } else if (progressUpdate.getAndSet(new ProgressUpdate(workDone, max)) == null) {
            Platform.runLater(() -> {
                final ProgressUpdate update = progressUpdate.getAndSet(null);
                _updateProgress(update.workDone, update.totalWork);
            });
        }
    }

    private void _updateProgress(double done, double max) {
    	totalWork.set(max);
    	workDone.set(done);
        if (done == -1) {
            setProgress(-1);
        } else {
            setProgress(done / max);
        }
    }
}
