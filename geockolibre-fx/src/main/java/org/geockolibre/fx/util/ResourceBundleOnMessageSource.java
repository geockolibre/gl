package org.geockolibre.fx.util;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Enumeration;
import java.util.ResourceBundle;

import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;

public class ResourceBundleOnMessageSource extends ResourceBundle {

	public final MessageSource messageSource;
	public ResourceBundleOnMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	@Override
	protected Object handleGetObject(String key) {
		try {
			return messageSource.getMessage(key, null, null);
		} catch (NoSuchMessageException e) {
			return null;
		}
	}
	
	@Override
	public boolean containsKey(String key) {
		try {
			messageSource.getMessage(key, null, null);
			return true;
		} catch (NoSuchMessageException e) {
			return false;
		}
	}

	@Override
	public Enumeration<String> getKeys() {
		return null;
	}

	
}
