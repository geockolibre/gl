package org.geockolibre.fx.util;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;

import javafx.application.Platform;

public class FxUtil {
    // Private constructor to avoid instantiation
    private FxUtil() {}

    public static final void runInJfx(final Runnable r) {
        if (Platform.isFxApplicationThread()) {
            r.run();
        } else {
            Platform.runLater(r);
        }
    }

    public static final void runInJfxAndWait(final Runnable r) {
        if (Platform.isFxApplicationThread()) {
            r.run();
        } else {
        	AtomicBoolean finished = new AtomicBoolean(false);
            Platform.runLater(()-> {
            	try {
            		r.run();
            	} finally {
            		finished.set(true);
            	}
            });
            while (!finished.get()) {
            	Thread.yield();
            }
        }
    }

    public static final Timer runLater(long timeout, final Runnable r) {
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                timer.cancel();
                Platform.runLater(r);
            }
        },timeout);
        return timer;
    }

    public static final Timer runInBackground(long timeout, final Runnable r) {
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                timer.cancel();
                r.run();
            }
        },timeout);
        return timer;
    }
}
