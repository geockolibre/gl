package org.geockolibre.fx.util;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericApplicationContext;

@Named
public class SpringFXMLLoader {
    @Inject
    private ApplicationContext applicationContext;
    @Inject
    private FXMLLoaderUtil fxmlLoaderUtil;
    
    public <T extends Controller> T load(String url) {
        return fxmlLoaderUtil.<T>load(url, aClass -> getBean(aClass, url)).getController();
    }
    

    private <T> T getBean(Class<T> aClass, String url) {
        if (aClass.getAnnotation(Named.class)!=null) {
            return applicationContext.getBean(aClass);
        } else {
        	String id = createId(url);
            if (applicationContext.containsBean(id)) {
                return applicationContext.getBean(id, aClass);
            }
            AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
            T bean = beanFactory.createBean(aClass);
            ((GenericApplicationContext)applicationContext).getBeanFactory().registerSingleton(id, bean);
            return bean;
        }
    }
    
    private String createId(String url) {
    	return url.replaceAll("[^a-zA-Z0-9_]", ".");
    }

    public void releaseController(Object bean) {
        if (bean.getClass().getAnnotation(Named.class)!=null) {
            return;
        }
        if (bean.getClass().getAnnotation(Named.class)!=null) {
            return;
        }
        final BeanDefinitionRegistry factory = (BeanDefinitionRegistry) applicationContext.getAutowireCapableBeanFactory();
        for (Map.Entry<String, ?> beanEntry : ((DefaultListableBeanFactory) factory).getBeansOfType(bean.getClass()).entrySet()) {
            if (bean == beanEntry.getValue()) {
                ((DefaultListableBeanFactory) factory).destroySingleton(beanEntry.getKey());
            }
        }
    }
}
