package org.geockolibre.fx;

import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;

import org.apache.commons.lang3.RandomUtils;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

public class Credentials {
	private static final String UTF_8 = "UTF-8";
	private static final String SPLITCHAR = "é";
	private static final String MASK = "#!Ööç_";
	private static final String EQUALSMASK = "èÔë§à";
	private final String username;
	private final String password;
	private final boolean rememberMe;
	public Credentials(String username, String password, boolean rememberMe) {
		this.username = username;
		this.password = password;
		this.rememberMe = rememberMe;
	}
	public String getUsername() {
		return username;
	}
	public String getPassword() {
		return password;
	}
	public boolean isRememberMe() {
		return rememberMe;
	}
	
	public String getScramble() {
		try {
			byte[] usernameBytes = username.getBytes(UTF_8);
			byte[] passwordBytes = password.getBytes(UTF_8);
			byte[] scramblePassword = combine(passwordBytes, usernameBytes);
			Encoder encoder = Base64.getEncoder();
			String encodedPasswd = encoder.encodeToString(scramblePassword);
			String encodedUsername = encoder.encodeToString(usernameBytes);
			encodedPasswd = encodedPasswd.replaceAll("(.{"+RandomUtils.nextInt(encodedPasswd.length()/2, (encodedPasswd.length()/3)*2)+"})", "$1"+SPLITCHAR);
			for (int i=0; i<4;i++) {
				int idx = RandomUtils.nextInt(0, EQUALSMASK.length());
				encodedPasswd = encodedPasswd.replaceFirst("=", EQUALSMASK.substring(idx, idx+1));
				encodedUsername = encodedUsername.replaceFirst("=", EQUALSMASK.substring(idx, idx+1));
			}
			String scramble = new StringBuilder()
					.append(encodedUsername)
					.append(SPLITCHAR)
					.append(encodedPasswd).toString();
			for (int i=0; i<8; i++) {
				int idx= RandomUtils.nextInt(0, MASK.length());
				int pos= RandomUtils.nextInt(0,  scramble.length());
				scramble = scramble.substring(0, pos)+MASK.charAt(idx)+scramble.substring(pos);
			}
			return scramble.replaceAll("(.{"+RandomUtils.nextInt(4,10)+"})", "$1 ");
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e); // Should not happend. UTF-8 is always supported.
		}
	}
	public static Credentials createFromScramble(String scramble) {
		try {
			String usp = scramble.replaceAll(" ", "").replaceAll("["+MASK+"]", "");
			int nlIdx = usp.indexOf(SPLITCHAR);
			if (nlIdx<=0) {
				return new Credentials(null, null, false);
			} else {
				Decoder decoder = Base64.getDecoder();
				byte[] usernameBytes = decoder.decode(usp.substring(0, nlIdx).replaceAll("["+EQUALSMASK+"]", "="));
				String username = new String(usernameBytes, UTF_8);
				byte[] scrambledPasswordBytes = decoder.decode(usp.substring(nlIdx+1).replaceAll(SPLITCHAR, "").replaceAll("["+EQUALSMASK+"]", "="));
				byte[] passwordBytes = combine(scrambledPasswordBytes, usernameBytes);
				String password = new String(passwordBytes, UTF_8);
				return new Credentials(username, password, true);
			}
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e); // Should not happend. UTF-8 is always supported.
		}
	}
	private static byte[] combine(byte[] input, byte[] mask) {
		int idx = 0;
		int maskIdx = 0;
		byte[] output = new byte[input.length];
		for (byte passwd : input) {
			output[idx] = (byte) (passwd ^ mask[maskIdx]);
			idx++;
			maskIdx++;
			if (maskIdx >= mask.length) {
				maskIdx = 0;
			}
		}
		return output;
	}
}
