package org.geockolibre.fx;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.function.Consumer;

import org.apache.commons.lang3.StringUtils;
import org.geockolibre.fx.util.AbstractController;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class LoginController extends AbstractController {
	@FXML private TextField loginField;
	@FXML private PasswordField passwordField;
	@FXML private CheckBox rememberLogin;
	@FXML private Label rememberMeWarning;
	
	@FXML private Button okButton;
	@FXML private Button cancelButton;
	
	private Consumer<Result> callback;
	
	private Result result;
	public static class Result {
		final String login;
		final String password;
		final Boolean remember;
		final boolean doLogin;
		private Result(String login, String password, Boolean remember, boolean doLogin) {
			this.login = login;
			this.password = password;
			this.remember = remember;
			this.doLogin = doLogin;
		}
	}
	
	public void initialize() {
		loginField.setOnAction(e->passwordField.requestFocus());
		rememberMeWarning.setWrapText(true);
		passwordField.setOnAction(
				e -> {okButton.requestFocus();setResult(true);});
		okButton.setOnAction(e-> setResult(true));
		cancelButton.setOnAction(e-> setResult(false));
	}

	private void setResult(boolean doLogin) {
		result = new Result(loginField.getText(), passwordField.getText(), rememberLogin.isSelected(), doLogin);
		loginField.setDisable(true);
		passwordField.setDisable(true);
		okButton.setDisable(true);
		cancelButton.setDisable(true);
		rememberLogin.setDisable(true);
		callback.accept(result);
	}
	
	public void retry() {
		loginField.setDisable(false);
		passwordField.setDisable(false);
		okButton.setDisable(false);
		cancelButton.setDisable(false);
		rememberLogin.setDisable(false);
	}

	public Result getResult() {
		return result;
	}
	
	public void setCallback(Consumer<Result> callback) {
		this.callback = callback;
	}
	
	public void setLogin(String login) {
		if (login!=null) {
			loginField.setText(StringUtils.trimToEmpty(login));
		}
	}
	public void setPassword(String password) {
		if (password!=null) {
			passwordField.setText(StringUtils.trimToEmpty(password));
		}
	}
	public void setRememberMe(boolean rememberMe) {
		rememberLogin.setSelected(rememberMe);
	}
}
