package org.geockolibre.fx;

import java.util.EnumMap;
import java.util.Map;
import java.util.function.Consumer;

import javax.inject.Inject;

import org.geockolibre.scrape.Attribute;
import org.geockolibre.scrape.InclusionEnum;
import org.springframework.context.MessageSource;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class FilterAttributeDialog extends AbstractDialogController {
	@Inject
	private MessageSource messageSource;

	@FXML Button cancelButton;
	@FXML Button okButton;
	@FXML GridPane attributePane;
	
	private Map<Attribute, InclusionEnum> selected = new EnumMap<>(Attribute.class);
	private Map<Attribute, Map<InclusionEnum, RadioButton>> radioButtons = new EnumMap<>(Attribute.class);
	private Consumer<Map<Attribute, InclusionEnum>> callback;
	
	public void initialize() {
		cancelButton.setOnAction(e->close());
		okButton.setOnAction(e->finishSelection());
		
		int row = 1;
		for (Attribute att : Attribute.values()) {
			selected.put(att, InclusionEnum.IGNORE);
			ImageView image = new ImageView("/org/geockolibre/fx/img/attribute_"+att.name().toLowerCase().replace("_yes","").replace("_no", "")+".png");
			image.getStyleClass().add("inc_"+att.getInc());
			if (att.getInc()==0) {
				ImageView overlay = new ImageView("/org/geockolibre/fx/img/attribute__strikethru.png");
				attributePane.add(new StackPane(image,overlay), 0, row);
			} else {
				attributePane.add(image, 0, row);
			}
			attributePane.add(new Label(att.getDescription()), 1, row);
			EnumMap<InclusionEnum, RadioButton> rbmap = new EnumMap<>(InclusionEnum.class);
			radioButtons.put(att, rbmap);
			ToggleGroup group = new ToggleGroup();
			RadioButton incl = new RadioButton();
			RadioButton excl = new RadioButton();
			RadioButton ignore = new RadioButton();
			ignore.setSelected(true);
			rbmap.put(InclusionEnum.INCLUDE, incl);
			rbmap.put(InclusionEnum.EXCLUDE, excl);
			rbmap.put(InclusionEnum.IGNORE, ignore);
			incl.setOnAction(e-> setIfSelected(att, incl, InclusionEnum.INCLUDE));
			excl.setOnAction(e-> setIfSelected(att, excl, InclusionEnum.EXCLUDE));
			ignore.setOnAction(e-> setIfSelected(att, ignore, InclusionEnum.IGNORE));
			incl.setToggleGroup(group);
			excl.setToggleGroup(group);
			ignore.setToggleGroup(group);
			attributePane.add(incl, 2, row);
			attributePane.add(excl, 3, row);
			attributePane.add(ignore, 4, row);
			row++;
		}
	}
		
	private void setIfSelected(Attribute att, RadioButton rb, InclusionEnum val) {
		if (rb.isSelected()) {
			selected.put(att,  val);
		}
	}
	
	
	public void show(Map<Attribute, InclusionEnum> selected, 
				Consumer<Map<Attribute, InclusionEnum>> callback) {
		this.callback = callback;
		this.selected.putAll(selected);
		radioButtons.forEach((a, m)-> m.forEach((i, rb) -> rb.setSelected(i.equals(this.selected.get(a)))));
		super.show(true);
	}
	@Override
	protected void initDialogStage(Stage stage) {
		super.initDialogStage(stage);
		stage.setTitle(messageSource.getMessage("attributes.title", null,null));
	}
	
	
	public void finishSelection() {
		close();
		callback.accept(selected);
	}
}
