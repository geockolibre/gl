package org.geockolibre.fx;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.Optional;

import javax.inject.Inject;

import org.geockolibre.fx.util.FxUtil;
import org.geockolibre.fx.util.ProgressController;
import org.geockolibre.fx.util.SpringFXMLLoader;
import org.geockolibre.gpx.GpxFolderWriter;
import org.geockolibre.gpx.GpxWriter;
import org.geockolibre.scrape.Cache;
import org.geockolibre.scrape.util.concurrent.Execution;
import org.geockolibre.scrape.util.concurrent.MultiThreadUtil;
import org.springframework.context.MessageSource;

import javafx.application.HostServices;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class SearchResultDialog extends AbstractDialogController {
	@FXML Label messageLabel;
	@FXML Button showOnMapButton;
	@FXML Button openInBrowserButton;
	@FXML Button saveFileButton;
	@FXML Button saveFolderButton;
	@FXML Button doneButton;
	
	@Inject
	private Identity identity;
	@Inject
	private MessageSource messageSource;
	@Inject
	private SpringFXMLLoader fxmlLoader;
	@Inject
	private HostServices hostServices;
	
	private File cacheFile;
	private int cacheCount;
	
	public void initialize() {
		doneButton.setOnAction(e->close());
		showOnMapButton.setOnAction(e->showOnMap());
		openInBrowserButton.setOnAction(e->openInBrowser());
		saveFileButton.setOnAction(e->saveToFile());
		saveFolderButton.setOnAction(e->saveToFolder());
	}
	public void show(File cacheFile, int cacheCount) {
		this.cacheCount = cacheCount;
		this.cacheFile = cacheFile;
		messageLabel.setText(messageSource.getMessage("searchResultDialog.found", new Object[] {cacheCount}, null));
		super.show(false);
	}
	
	private void saveToFile() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle(messageSource.getMessage("searchResultDialog.selectOutputFile", null, null));
		fileChooser.setSelectedExtensionFilter(new ExtensionFilter("gpx files", ".gpx"));
		File outputFile = fileChooser.showSaveDialog(getDialogStage());
		if (outputFile != null) {
			if (!outputFile.getName().toLowerCase().endsWith(".gpx")) {
				outputFile = new File(outputFile.getAbsolutePath()+".gpx");
			}
			ProgressController<Void> progress = new ProgressController<>();
			GpxWriter gpxWriter = new GpxWriter(cacheCount, cacheFile, identity.getUserIdManager(), outputFile.getAbsolutePath());
			ProgressDialog progressDialog = fxmlLoader.load("/org/geockolibre/fx/progress.fxml");
			progressDialog.setOwner(getView().getScene().getWindow());
			progressDialog.setProgressController(progress);
			progressDialog.show(false);
			
			MultiThreadUtil.<Void>executeWithCallback((l)->{gpxWriter.run(l); return null;}, 
					e->FxUtil.runInJfx(()->writeDone(e, progressDialog)), progress);
			
		}
		
	}
	
	private void saveToFolder() {
		DirectoryChooser fileChooser = new DirectoryChooser();
		fileChooser.setTitle(messageSource.getMessage("searchResultDialog.selectOutputFolder", null, null));
		File outputFile = fileChooser.showDialog(getDialogStage());
		if (outputFile != null) {
			ProgressController<Void> progress = new ProgressController<>();
			GpxFolderWriter gpxWriter = new GpxFolderWriter(cacheCount, cacheFile, identity.getUserIdManager(), outputFile.getAbsolutePath());
			int nrOverwrites = gpxWriter.checkOverwrite();
			if (nrOverwrites >0) {
				Alert alert = new Alert(AlertType.CONFIRMATION);
				alert.setTitle(messageSource.getMessage("searchResultDialog.overwrite.title", null,null));
				alert.setHeaderText(messageSource.getMessage("searchResultDialog.overwrite.message", new Object[] {nrOverwrites, cacheCount}, null));
				alert.setContentText(messageSource.getMessage("searchResultDialog.overwrite.question", new Object[] {nrOverwrites, cacheCount}, null));
				ButtonType buttonTypeYes = new ButtonType(messageSource.getMessage("messagedialog.yes", null,null),ButtonData.YES);
				ButtonType buttonTypeNo = new ButtonType(messageSource.getMessage("messagedialog.no", null,null),ButtonData.NO);
				ButtonType buttonTypeCancel = new ButtonType(messageSource.getMessage("messagedialog.cancel", null,null),ButtonData.CANCEL_CLOSE);
				alert.getButtonTypes().setAll(buttonTypeYes, buttonTypeNo, buttonTypeCancel);
				Optional<ButtonType> result = alert.showAndWait();
				gpxWriter.setOverwrite(result.get() == buttonTypeYes);
				if (result.get()==buttonTypeCancel) {
					return;
				}
			}
				
			ProgressDialog progressDialog = fxmlLoader.load("/org/geockolibre/fx/progress.fxml");
			progressDialog.setOwner(getView().getScene().getWindow());
			progressDialog.setProgressController(progress);
			progressDialog.show(false);
			
			MultiThreadUtil.<Void>executeWithCallback((l)->{gpxWriter.run(l); return null;}, 
					e->FxUtil.runInJfx(()->writeDone(e, progressDialog)), progress);
			
		}
	}
	
	private void openInBrowser() {
		if (cacheCount > 10) {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle(messageSource.getMessage("searchResultDialog.openBrowser.title", null,null));
			alert.setHeaderText(messageSource.getMessage("searchResultDialog.openBrowser.header", new Object[] {cacheCount}, null));
			alert.setContentText(messageSource.getMessage("searchResultDialog.openBrowser.warning", new Object[] {cacheCount}, null));
	
			ButtonType buttonTypeYes = new ButtonType(messageSource.getMessage("messagedialog.yes", null,null),ButtonData.YES);
			ButtonType buttonTypeNo = new ButtonType(messageSource.getMessage("messagedialog.no", null,null),ButtonData.NO);
			alert.getButtonTypes().setAll(buttonTypeYes, buttonTypeNo);
			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() != buttonTypeYes) {
				return;
			}
		}
		try(ObjectInputStream input = new ObjectInputStream(new FileInputStream(cacheFile))) {
			for (int x = 0; x < cacheCount; x++) {
				Cache c = (Cache) input.readObject();
				hostServices.showDocument("http://coord.info/" + c.getCacheCode());
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}

	}

	private void showOnMap() {
		ProgressController<Void> progress = new ProgressController<>();
		MapGenerator mapGenerator = new MapGenerator(cacheCount, cacheFile, identity.getLogin().getUserName(), messageSource, hostServices);
		ProgressDialog progressDialog = fxmlLoader.load("/org/geockolibre/fx/progress.fxml");
		progressDialog.setOwner(getView().getScene().getWindow());
		progressDialog.setProgressController(progress);
		progressDialog.show(false);
			
		MultiThreadUtil.<Void>executeWithCallback((l)->{mapGenerator.run(l); return null;}, 
				e->FxUtil.runInJfx(()->writeDone(e, progressDialog)), progress);
	}
	

	private void writeDone(Execution<Void> e, ProgressDialog progressDialog) {
		fxmlLoader.releaseController(progressDialog);
		if (e.getError()!=null) {
			e.getError().printStackTrace();
		}
	}
	
	public void close() {
		cacheFile.delete();
		super.close();
	}
}
