package org.geockolibre.fx;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.geockolibre.fx.util.FxUtil;
import org.geockolibre.fx.util.ProgressController;
import org.geockolibre.fx.util.SpringFXMLLoader;
import org.geockolibre.scrape.Login;
import org.geockolibre.scrape.LoginService;
import org.geockolibre.scrape.util.UserIdManager;
import org.geockolibre.scrape.util.concurrent.Execution;
import org.geockolibre.scrape.util.concurrent.MultiThreadUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericApplicationContext;

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Geockolibre extends Application {
	private static final Logger LOGGER = LoggerFactory.getLogger(Geockolibre.class);
	private ApplicationContext context;
	private Stage primaryStage;
	private SpringFXMLLoader loader;
	private MessageSource messages;
	private LoginService loginService;
	private Identity identity;
	private LoginController loginController;
	
	public static void main(String[] args) {
		LOGGER.info("Starting Geockolibre");
		Geockolibre.launch(args);
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.getIcons().add(new Image(Geockolibre.class.getResourceAsStream("/logo_16.png")));
		primaryStage.getIcons().add(new Image(Geockolibre.class.getResourceAsStream("/logo_32.png")));
		primaryStage.getIcons().add(new Image(Geockolibre.class.getResourceAsStream("/logo_64.png")));
		primaryStage.getIcons().add(new Image(Geockolibre.class.getResourceAsStream("/logo_128.png")));
        context = new AnnotationConfigApplicationContext(GeockolibreConfig.class);
        ((GenericApplicationContext)context).getBeanFactory().registerSingleton("hostServices", getHostServices());

        loader = context.getBean(SpringFXMLLoader.class);
        messages = context.getBean(MessageSource.class);
        loginService = context.getBean(LoginService.class);
        identity = context.getBean(Identity.class);
        this.primaryStage = primaryStage;
        tryLogin();
	}
	
	private void tryLogin() {
        if (loginController==null) {
        	loadLoginScreen();
        }
        loginController.retry();
	}

	private void loadLoginScreen() {
		loginController = loader.load("/org/geockolibre/fx/login.fxml");
		Scene scene = new Scene((Parent)loginController.getView());
		primaryStage.setScene(scene);
		loginController.setCallback(this::doLogin);
		CredentialsStore credentialsStore = context.getBean(CredentialsStore.class);
		Credentials credentials = credentialsStore.loadCredentials();
		if (credentials.isRememberMe()) {
			loginController.setLogin(credentials.getUsername());
			loginController.setPassword(credentials.getPassword());
			loginController.setRememberMe(true);
		} 
		primaryStage.setTitle(messages.getMessage("logindialog.title", null, null));
		primaryStage.show();
	}

	private void doLogin(LoginController.Result result) {
		if (result.doLogin) {
	        Login login = new Login(result.login, result.password);
			CredentialsStore credentialsStore = context.getBean(CredentialsStore.class);
			credentialsStore.saveCredentials(new Credentials(result.login, result.password, result.remember));
			ProgressDialog progressDialog = loader.load("/org/geockolibre/fx/progress.fxml");
			ProgressController<Login> progress = new ProgressController<>();
			progressDialog.setProgressController(progress);
			progressDialog.setOwner(primaryStage);
			progressDialog.show(false);
			MultiThreadUtil.executeWithCallback((l)->loginService.authenticate(login,l), 
					e->FxUtil.runInJfx(()->loginDone(e, progressDialog)), progress);
		} else {
			System.exit(0);
		}
	}
	
	private void loginDone(Execution<Login> loginExecution, ProgressDialog progressDialog) {
		loader.releaseController(progressDialog);
		switch (loginExecution.getState()) {
		case CANCELLED:
			System.exit(0);
			break;
		case FAILED: {
			LOGGER.error("Login failed", loginExecution.getError());
			MessageDialog errorDialog = loader.load("/org/geockolibre/fx/messageDialog.fxml");
			errorDialog.setOwner(primaryStage);
			errorDialog.setMessage(loginExecution.getError().getMessage(), "Login failed");
			errorDialog.show(true);
			loader.releaseController(errorDialog);
			tryLogin();
			break;
		} 
		case FINISHED: {
			identity.setLogin(loginExecution.getResult());
			identity.setUserIdManager(new UserIdManager(identity.getLogin()));
			identity.getUserIdManager().loadDb();
			MainController mainWindow = loader.load("/org/geockolibre/fx/main.fxml");
			primaryStage.setScene(new Scene((Parent)mainWindow.getView()));
			primaryStage.setTitle(messages.getMessage("main.title", new Object[] {identity.getLogin().getUserName()}, null));
			primaryStage.centerOnScreen();
			primaryStage.show();
			break;
		}
		default:
			throw new IllegalStateException("State not possible at end of execution "+loginExecution.getState());
		} 
	}
}
