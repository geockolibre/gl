package org.geockolibre.fx;

import org.geockolibre.scrape.SearchErrorHandler;
import org.geockolibre.scrape.SearchErrorHandler.SearchErrorHandlerEnum;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class SearchErrorDialog extends AbstractDialogController {
	@FXML Label messageLabel;
	@FXML Button cancelButton;
	@FXML Button ignoreButton;
	@FXML Button ignoreAllButton;
	
	SearchErrorHandler.SearchErrorHandlerEnum result;
	
	
	public void initialize() {
		cancelButton.setOnAction(e->close(SearchErrorHandlerEnum.STOP));
		ignoreButton.setOnAction(e->close(SearchErrorHandlerEnum.IGNORE));
		ignoreAllButton.setOnAction(e->close(SearchErrorHandlerEnum.IGNORE_ALL));
	}
	
	private void close(SearchErrorHandler.SearchErrorHandlerEnum result) {
		this.result = result;
		close();
	}
	
	public SearchErrorHandler.SearchErrorHandlerEnum show() {
		super.show(true);
		return result;
	}

	public void setMessage(String message, String title) {
		getDialogStage().setTitle(title);
		messageLabel.setText(message);
	}
}
