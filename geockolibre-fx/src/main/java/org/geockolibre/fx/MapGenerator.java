package org.geockolibre.fx;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.geockolibre.scrape.Cache;
import org.geockolibre.scrape.CacheType;
import org.geockolibre.scrape.Location;
import org.geockolibre.scrape.util.Tools;
import org.geockolibre.scrape.util.UserAgentFaker;
import org.geockolibre.scrape.util.WebClient;
import org.geockolibre.scrape.util.concurrent.ProgressListener;
import org.springframework.context.MessageSource;

import javafx.application.HostServices;

/**
 * Generates a comma separated value description of the listed caches, uploads
 * them to gpsvisualiser.com and displays the result.
 * 
 */
public class MapGenerator {

	private String ourname;
	private File cacheFile;
	private int cacheCount;
	private MessageSource messageSource;
	private HostServices hostServices;

	/**
	 * Create a new map generator.
	 * 
	 * @param cacheCount
	 *            the number of caches to generate a map for.
	 * @param cacheFile
	 *            the file containing the serialised caches.
	 * @param ourname
	 *            the name of the user that owns the caches to be marked with an
	 *            "our" icon.
	 */
	public MapGenerator(int cacheCount, File cacheFile, String ourname, MessageSource messageSource, HostServices hostServices) {
		this.cacheCount = cacheCount;
		this.cacheFile = cacheFile;
		this.ourname = ourname;
		this.messageSource = messageSource;
		this.hostServices = hostServices;
	}

	public void run(ProgressListener listener)  {
		listener.updateTitle(messageSource.getMessage("mapGenerator.title", null,null));
		listener.updateAction(messageSource.getMessage("mapGenerator.title", null,null));
		listener.updateProgress(0, 100);
		listener.updateMessage(messageSource.getMessage("mapGenerator.generating", null,null));
		StringBuilder url = new StringBuilder("name,desc,lat,lon,icon_size,sym");
		try (ObjectInputStream input = new ObjectInputStream(new FileInputStream(cacheFile))) {
			String newline = "\n";
			for (int x = 0; x < cacheCount; x++) {
				Cache c = (Cache) input.readObject();
				StringBuilder tmp = new StringBuilder(newline);
				tmp.append(escape(c.getName()));
				tmp.append(",");
				StringBuilder desc = new StringBuilder();
				desc.append("<a href=\"http://coord.info/");
				desc.append(c.getCacheCode());
				desc.append("\" target=\"_blank\">");
				desc.append(c.getCacheCode());
				desc.append("</a><br>");
				desc.append(c.getCacheType());
				desc.append("<br>D/T: ");
				desc.append(c.getDifficultyRating());
				desc.append("/");
				desc.append(c.getTerrainRating());
				desc.append("<br>Size: ");
				desc.append(c.getCacheSize().toString());
				tmp.append(escape(desc.toString()));
				tmp.append(",");
				Location loc = c.getLocation();
				tmp.append(shortify(loc.getLatitude().toDecimalString()));
				tmp.append(",");
				tmp.append(shortify(loc.getLongitude().toDecimalString()));
				tmp.append(",16x16,");
				tmp.append(getIconUrlString(c));

				url.append(tmp);
			}
		} catch (IOException|ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
		listener.updateMessage(messageSource.getMessage("mapGenerator.compressing", null,null));
		listener.updateProgress(10, 100);
		// compress the string
		ByteArrayOutputStream data = new ByteArrayOutputStream();
		try (ZipOutputStream out = new ZipOutputStream(data)) {
			out.setLevel(9);
			out.putNextEntry(new ZipEntry("file.csv"));
			byte[] tmp = url.toString().getBytes("UTF-8");
			out.write(tmp);
			out.closeEntry();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		listener.updateMessage(messageSource.getMessage("mapGenerator.uploading", null,null));
		listener.updateProgress(30, 100);

		// submit the string to the form
		WebClient wc = new WebClient();
		wc.setRequestMethod("POST");
		wc.setUserAgent(UserAgentFaker.getRandomUserAgent());
		Map<String, String> params = new HashMap<String, String>();
		params.put("format", "google");
		params.put("convert_format", "");
		params.put("form", "google");
		params.put("google_full_screen", "1");
		params.put("zoom_control", "3d");
		// leaving the following in skips the output page and returns the
		// results directly
		params.put("return_image", "1");
		Map<String, byte[]> files = new HashMap<String, byte[]>();
		files.put("uploaded_file_1@tmp.zip", data.toByteArray());

		listener.updateProgress(31, 100);

		try {
			wc.submitForm("http://www.gpsvisualizer.com/map?output_home", params, files);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		listener.updateMessage(messageSource.getMessage("mapGenerator.reading", null,null));
		listener.updateProgress(80, 100);

		// parse the result, get the address
		String contents = wc.getContentsAsString();
		// enable mouswheel zoom
		contents = contents.replace("gv_options.mousewheel_zoom = false;", "gv_options.mousewheel_zoom = true;");
		// create a temporary file
		try {
			File tmpFile = File.createTempFile("gpxsearcher", ".html");
			tmpFile.deleteOnExit();
			String address = tmpFile.getAbsolutePath();
			if (address.indexOf(":") == 1) {
				// windows local path
				address = "/" + address;
			}
			// recode backslashes
			address = address.replaceAll("\\\\", "/");
			// spaces not allowed
			address = address.replaceAll(" ", "%20");
			address = "file://" + address;
			
			// write the map to temp file
			FileWriter fw = new FileWriter(tmpFile);
			fw.write(contents);
			fw.close();
			listener.updateMessage(messageSource.getMessage("mapGenerator.openingBrowser", null,null));
			listener.updateProgress(90, 100);
			
			hostServices.showDocument(address);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		// keep dialog open for a little bit longer
		Tools.sleep(500);
		listener.updateProgress(100, 100);
	}

	/**
	 * @param cacheType
	 * @return
	 */
	private String getIconUrlString(Cache c) {
		StringBuilder res = new StringBuilder("https://gitlab.com/geockolibre/gl/raw/master/geockolibre-fx/img/");
		res.append(getId(c));
		res.append(".png");
		return res.toString();
	}

	/**
	 * @param cacheType
	 * @return
	 */
	private String getId(Cache c) {
		String res = "";
		if (c.isFoundByUser()) {
			res = "found";
		} else if (c.getHider().getName().equals(ourname)) {
			res = "own";
		} else {
			CacheType cacheType = c.getCacheType();
			switch (cacheType) {
			case TRADITIONAL:
				res = "trad";
				break;
			case MULTI:
				res = "multi";
				break;
			case VIRTUAL:
				res = "virtual";
				break;
			case LETTERBOX:
				res = "letter";
				break;
			case EVENT:
				res = "event";
				break;
			case MYSTERY:
				res = "myst";
				break;
			case WEBCAM:
				res = "webcam";
				break;
			case CITO:
				res = "cito";
				break;
			case WHERIGO:
				res = "wherigo";
				break;
			case EARTH_CACHE:
				res = "earth";
				break;
			case MEGA_EVENT:
				res = "megaevent";
				break;
			default:
				// use blank as the default
				res = "blank";
				break;
			}

		}
		if (c.isDisabled()) {
			res += "d";
		}
		return res;
	}

	/**
	 * Format the string for inclusion in gpsvisualiser.com's URL format.
	 * 
	 * @param string
	 * @return
	 */
	private Object escape(String string) {
		string = string.replaceAll(",", "");
		return string;
	}

	/**
	 * Round decimal number to five digits after decimal point.
	 * 
	 * 
	 * @param decimalString
	 * @return
	 */
	private Object shortify(String decimalString) {
		String res = decimalString;
		int pointIndex = res.indexOf(".");
		if (pointIndex >= 0) {
			int digits = res.length() - pointIndex - 1;
			if (digits > 5) {
				// get the last six digits
				String sub = res.substring(0, pointIndex + 7);
				// round
				double d = Double.parseDouble(sub);
				d = Math.round(d * 100000.0) / 100000.0;
				// convert to string
				res = Double.toString(d);
				// get string to last 5 digits
				pointIndex = res.indexOf(".");
				if (pointIndex >= 0) {
					digits = res.length() - pointIndex - 1;
					if (digits > 5) {
						res = res.substring(0, pointIndex + 6);
					}
				}
			}
		}
		return res;
	}
}
