package org.geockolibre.fx;

import java.io.File;
import java.io.IOException;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.time.LocalDate;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.geockolibre.fx.util.AbstractController;
import org.geockolibre.fx.util.DecimalSeparatorTolerantDoubleStringConverter;
import org.geockolibre.fx.util.SpringFXMLLoader;
import org.geockolibre.gpx.reader.GPX;
import org.geockolibre.scrape.Attribute;
import org.geockolibre.scrape.CacheSize;
import org.geockolibre.scrape.CacheType;
import org.geockolibre.scrape.DifficultyTerrain;
import org.geockolibre.scrape.DistanceUnitEnum;
import org.geockolibre.scrape.InclusionEnum;
import org.geockolibre.scrape.Location;
import org.geockolibre.scrape.ResultFilter;
import org.geockolibre.scrape.SearchProgressListener;
import org.geockolibre.scrape.util.LocationUtil;
import org.geockolibre.scrape.util.concurrent.ProgressListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory.DoubleSpinnerValueFactory;
import javafx.scene.control.SpinnerValueFactory.IntegerSpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class SearchLocationController extends AbstractController implements SearchController {
	private static final Logger LOGGER = LoggerFactory.getLogger(SearchLocationController.class);
	private static final double RADIUSFACTOR = 1 / Math.sin(Math.toRadians(60));
	private List<CacheType> FILTERABLE_TYPES = Arrays.asList(CacheType.TRADITIONAL, CacheType.EVENT, CacheType.CITO,
			CacheType.MYSTERY, CacheType.MEGA_EVENT, CacheType.EARTH_CACHE,
			CacheType.MULTI, CacheType.VIRTUAL, CacheType.WHERIGO,
			CacheType.WEBCAM, CacheType.LETTERBOX, CacheType.GPSADVENTURE);
	@Inject
	private MessageSource messageSource;
	@Inject
	private SpringFXMLLoader fxmlLoader;
	@Inject
	private Identity identity;
	
	private FilterDifficultyTerrainDialog filterDifficultyTerrainDialog;
	private FilterAttributeDialog filterAttributeDialog;
	
	@FXML ToggleGroup locationToggleGroup;
	@FXML RadioButton singleLocationButton;
	@FXML TextField locationField;
	@FXML Button homeLocationButton;
	@FXML RadioButton locationFromFileButton;
	@FXML Label locationFileLabel;
	@FXML Button selectLocationFileButton;
	@FXML Spinner<Double> radiusSpinner;
	@FXML ComboBox<String> radiusUnit;
	@FXML Spinner<Integer> resultsSpinner;
	@FXML CheckBox filterDifficultyTerrain;
	@FXML Button editDifficultyTerrain;
	@FXML CheckBox filterAttributes;
	@FXML Button editAttributes;
	@FXML CheckBox filterSize;
	@FXML CheckBox filterType;
	@FXML GridPane filterSizeBox;
	@FXML GridPane filterTypeBox;
	@FXML CheckBox filterPlacedOnOrAfter;
	@FXML DatePicker placedOnOrAfter;
	@FXML CheckBox filterPlacedOnOrBefore;
	@FXML DatePicker placedOnOrBefore;
	@FXML CheckBox filterFoundInLast;
	@FXML Spinner<Integer> foundInLast;
	@FXML CheckBox hasNotBeenFound;
	@FXML CheckBox filterIgnoreOwn;
	@FXML CheckBox filterIgnoreFound;
	@FXML CheckBox filterIgnoreDisabled;
	@FXML CheckBox containsTrackable;
	@FXML CheckBox filterNrFavourites;
	@FXML Spinner<Integer> minNrFavourites;
	@FXML CheckBox filterKeyword;
	@FXML TextField searchKeyword;
	@FXML CheckBox includeFullLogs;
	@FXML Spinner<Integer> nrLogs;
	// TODO ADD reference to http://openrouteservice.org/
	
	
	private Set<DifficultyTerrain> diffTerrFilter = new HashSet<>();
	private Map<Attribute, InclusionEnum> attributeFilter = new EnumMap<>(Attribute.class);
	private Map<CacheSize, CheckBox> sizeCheckboxes = new EnumMap<>(CacheSize.class);
	private Map<CacheType, CheckBox> typeCheckboxes = new EnumMap<>(CacheType.class);
	private GPX gpxContent;
	
	@PostConstruct
	protected void loadDialogs() {
		filterDifficultyTerrainDialog = fxmlLoader.load("/org/geockolibre/fx/filterDifficultyTerrainDialog.fxml");		
		filterAttributeDialog = fxmlLoader.load("/org/geockolibre/fx/filterAttributeDialog.fxml");		
	}
	
	public void initialize() {
		locationField.disableProperty().bind(singleLocationButton.selectedProperty().not());
		homeLocationButton.disableProperty().bind(singleLocationButton.selectedProperty().not());
		singleLocationButton.setSelected(true);
		locationField.textProperty().addListener((ob, oldVal, newVal) -> {
			if (!StringUtils.isEmpty(newVal)) {
				try {
					new Location(newVal);
					locationField.getStyleClass().remove("invalid");
					locationField.setTooltip(null);
				} catch (Exception e) {
					if (locationField.getStyleClass().add("invalid")) {
						locationField.setTooltip(new Tooltip(messageSource.getMessage("location.invalid.tooltip", null, null)));
					}
				}
			} else {
				locationField.getStyleClass().remove("invalid");
				locationField.setTooltip(null);
			}
		});
		if (identity.getLogin()!=null && identity.getLogin().getHomeLocation()!=null) {
			locationField.setText(identity.getLogin().getHomeLocation().toString());
		}
		homeLocationButton.setOnAction(e->locationField.setText(identity.getLogin().getHomeLocation().toString()));
		

		selectLocationFileButton.disableProperty().bind(locationFromFileButton.selectedProperty().not());
		selectLocationFileButton.setOnAction(e->selectGpxFile());
		locationFileLabel.setWrapText(true);
		locationFileLabel.setMinHeight(50);
		locationFileLabel.setMaxWidth(600);
		radiusUnit.getItems().add(0, messageSource.getMessage("searchLocation.km", null, null));
		radiusUnit.getItems().add(1, messageSource.getMessage("searchLocation.miles", null, null));
		radiusUnit.getSelectionModel().select(0);
		radiusSpinner.setEditable(true);
		radiusSpinner.setValueFactory(new DoubleSpinnerValueFactory(0.1,1000, 5, 0.5));
		radiusSpinner.getValueFactory().setConverter(new DecimalSeparatorTolerantDoubleStringConverter());
		// hook in a formatter with the same properties as the factory
		TextFormatter<Double> radiusFormatter = new TextFormatter<Double>(radiusSpinner.getValueFactory().getConverter(), radiusSpinner.getValueFactory().getValue());
		radiusSpinner.getEditor().setTextFormatter(radiusFormatter);
		radiusSpinner.getValueFactory().valueProperty().bindBidirectional(radiusFormatter.valueProperty());		

		resultsSpinner.setEditable(true);
		resultsSpinner.setValueFactory(new IntegerSpinnerValueFactory(0, Integer.MAX_VALUE, 5));
		TextFormatter<Integer> resultsFormatter = new TextFormatter<Integer>(resultsSpinner.getValueFactory().getConverter(), resultsSpinner.getValueFactory().getValue());
		resultsSpinner.getEditor().setTextFormatter(resultsFormatter);
		resultsSpinner.getValueFactory().valueProperty().bindBidirectional(resultsFormatter.valueProperty());		
		
		editDifficultyTerrain.disableProperty().bind(filterDifficultyTerrain.selectedProperty().not());
		editDifficultyTerrain.setOnAction(e->editFilterDifficultyTerrain());
		editAttributes.disableProperty().bind(filterAttributes.selectedProperty().not());
		editAttributes.setOnAction(e->editFilterAttribute());
		
		int col = 0;
		int row = 0;
		for (CacheSize size: CacheSize.values()) {
			CheckBox cb = new CheckBox(messageSource.getMessage("size."+size.name(), null,null));
			cb.disableProperty().bind(filterSize.selectedProperty().not());
			sizeCheckboxes.put(size, cb);
			
			filterSizeBox.add(cb, col, row);
			col++;
			if (col>2) {
				col = 0;
				row ++;
			}
		}
		
		col = 0;
		row = 0;
		for (CacheType type: FILTERABLE_TYPES) {
			CheckBox cb = new CheckBox(type.getDescription());
			cb.disableProperty().bind(filterType.selectedProperty().not());
			typeCheckboxes.put(type, cb);
			filterTypeBox.add(cb, col, row);
			col++;
			if (col>3) {
				col = 0;
				row ++;
			}
		}
		
		placedOnOrAfter.disableProperty().bind(filterPlacedOnOrAfter.selectedProperty().not());
		placedOnOrBefore.disableProperty().bind(filterPlacedOnOrBefore.selectedProperty().not());
		foundInLast.setEditable(true);
		foundInLast.disableProperty().bind(filterFoundInLast.selectedProperty().not());
		foundInLast.setValueFactory(new IntegerSpinnerValueFactory(0, Integer.MAX_VALUE, 1));
		TextFormatter<Integer> foundInLastFormatter = new TextFormatter<Integer>(foundInLast.getValueFactory().getConverter(), foundInLast.getValueFactory().getValue());
		foundInLast.getEditor().setTextFormatter(foundInLastFormatter);
		foundInLast.getValueFactory().valueProperty().bindBidirectional(foundInLastFormatter.valueProperty());		

		filterFoundInLast.setOnAction(e-> { if (filterFoundInLast.isSelected()) { hasNotBeenFound.setSelected(false); }});
		hasNotBeenFound.setOnAction(e-> { if (hasNotBeenFound.isSelected()) { filterFoundInLast.setSelected(false); }});
		minNrFavourites.disableProperty().bind(filterNrFavourites.selectedProperty().not());
		minNrFavourites.setValueFactory(new IntegerSpinnerValueFactory(0, Integer.MAX_VALUE, 1));
		TextFormatter<Integer> minNrFavouritesFormatter = new TextFormatter<Integer>(minNrFavourites.getValueFactory().getConverter(), minNrFavourites.getValueFactory().getValue());
		minNrFavourites.getEditor().setTextFormatter(minNrFavouritesFormatter);
		minNrFavourites.getValueFactory().valueProperty().bindBidirectional(minNrFavouritesFormatter.valueProperty());		

		nrLogs.setEditable(true);
		nrLogs.disableProperty().bind(includeFullLogs.selectedProperty());
		nrLogs.setValueFactory(new IntegerSpinnerValueFactory(0, Integer.MAX_VALUE, 25, 25));
		TextFormatter<Integer> nrLogsFormatter = new TextFormatter<Integer>(nrLogs.getValueFactory().getConverter(), nrLogs.getValueFactory().getValue());
		nrLogs.getEditor().setTextFormatter(nrLogsFormatter);
		nrLogs.getValueFactory().valueProperty().bindBidirectional(nrLogsFormatter.valueProperty());		

		searchKeyword.disableProperty().bind(filterKeyword.selectedProperty().not());
	}
	
	private void selectGpxFile() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle(messageSource.getMessage("searchLocation.selectInputFile", null, null));
		fileChooser.setSelectedExtensionFilter(new ExtensionFilter("gpx files", ".gpx"));
		File inputFile = fileChooser.showOpenDialog(getView().getScene().getWindow());
		if (inputFile != null) {
			try {
				GPX file = new GPX();
				file.parse(inputFile);
				gpxContent = file;
				List<List<Location>> gpxLocations = LocationUtil.getGpxTrackLocations(gpxContent);
				int nrLocations = gpxLocations.stream().map(List::size).reduce(0, Integer::sum);
				double length = LocationUtil.getTotalDistanceInMForList(gpxLocations)/1000;
				locationFileLabel.setText(
					messageSource.getMessage("searchLocation.file.content", new Object[] {
							inputFile.getName(),
							gpxContent.getTracks().size(),
							gpxContent.getRoutes().size(),
							gpxContent.getWayPoints().size(),
							nrLocations,
							length
					}, null));
			} catch (IOException e) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle(messageSource.getMessage("searchLocation.errorFile.title", null,null));
				alert.setHeaderText(messageSource.getMessage("searchLocation.errorFile.message", new Object[] {inputFile.getName()}, null));
				alert.setContentText(messageSource.getMessage("searchLocation.errorFile.content", new Object[] {inputFile.getName(), e.getMessage()}, null));
				alert.show();
			}
		}
	}
	public void editFilterDifficultyTerrain() {
		filterDifficultyTerrainDialog.setOwner(this.getView().getScene().getWindow());
		filterDifficultyTerrainDialog.show(diffTerrFilter, (e)->diffTerrFilter = e); 
	}
	
	public void editFilterAttribute() {
		filterAttributeDialog.setOwner(this.getView().getScene().getWindow());
		filterAttributeDialog.show(attributeFilter, (e)->attributeFilter = e); 
	}
	
	@Override
	public ResultFilter getResultFilter() {
		LocationResultFilter filter = new LocationResultFilter();
		filter.setAcceptedDts(diffTerrFilter);
		filter.setAcceptedSizes(sizeCheckboxes.keySet().stream().filter(k->sizeCheckboxes.get(k).isSelected()).collect(Collectors.toSet()));
		filter.setAcceptedTypes(typeCheckboxes.keySet().stream().filter(k->typeCheckboxes.get(k).isSelected()).collect(Collectors.toSet()));
		filter.setAttributes(attributeFilter);
		filter.setCheckAttributes(filterAttributes.isSelected());
		filter.setCheckDT(filterDifficultyTerrain.isSelected());
		filter.setCheckFavouritePoints(filterNrFavourites.isSelected());
		filter.setCheckFoundDate(filterFoundInLast.isSelected());
		filter.setCheckKeyword(filterKeyword.isSelected());
		filter.setCheckPlacedOnOrAfter(filterPlacedOnOrAfter.isSelected());
		filter.setCheckPlacedOnOrBefore(filterPlacedOnOrBefore.isSelected());
		filter.setCheckSize(filterSize.isSelected());
		filter.setCheckType(filterType.isSelected());
		filter.setContainsTrackable(containsTrackable.isSelected());
		filter.setDistance(radiusSpinner.getValue());
		filter.setDistanceUnit(DistanceUnitEnum.values()[radiusUnit.getSelectionModel().getSelectedIndex()]);
		filter.setIgnoreDisabled(filterIgnoreDisabled.isSelected());
		filter.setIgnoreFinds(filterIgnoreFound.isSelected());
		filter.setIgnoreOwn(filterIgnoreOwn.isSelected());
		filter.setKeyword(searchKeyword.getText());
		filter.setLastFoundDate(LocalDate.now().minusDays(foundInLast.getValue()));
		if (singleLocationButton.isSelected()) {
			filter.setLocation(new Location(locationField.getText()));
		} else {
			List<List<Location>> gpxLocations = LocationUtil.getGpxTrackLocations(gpxContent); 
			double distanceInKm = radiusSpinner.getValue();
			if (DistanceUnitEnum.values()[radiusUnit.getSelectionModel().getSelectedIndex()]==DistanceUnitEnum.MILES) {
				distanceInKm = distanceInKm / 1.609344;
				LOGGER.debug("Converting {} miles to {} km", radiusSpinner.getValue(), distanceInKm);
			}
			double radiusAndStepSizeKm = distanceInKm * RADIUSFACTOR;
			filter.setDistance(radiusAndStepSizeKm);
			filter.setDistanceUnit(DistanceUnitEnum.KM);
			gpxLocations.replaceAll(l-> {
				if(l.size()<=1) {
					return l;
				} else {
					return LocationUtil.getStepLocations(l, radiusAndStepSizeKm*1000);
				}
			});
			filter.setLocations(gpxLocations);
		}
		filter.setLogin(identity.getLogin());
		filter.setMaxCaches(resultsSpinner.getValue());
		filter.setMinFavPoints(minNrFavourites.getValue());
		filter.setNotFound(hasNotBeenFound.isSelected());
		filter.setPlacedOnOrAfter(placedOnOrAfter.getValue());
		filter.setPlacedOnOrBefore(placedOnOrBefore.getValue());
		filter.setMinNumberOfLogs(includeFullLogs.isSelected()?Integer.MAX_VALUE:nrLogs.getValue());
		return filter;
	}
	
	@Override
	public SearchProgressListener createSearchProgressListener(ProgressListener pl, MessageSource ms, ResultFilter rf) {
		if (((LocationResultFilter)rf).getNumberOfLocations()!=1) {
			return new MultiCacheSearchProgressListener(pl, ms);
		} else {
			return SearchController.super.createSearchProgressListener(pl, ms, rf);
		}
	}
}
