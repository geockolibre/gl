package org.geockolibre.fx;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.inject.Named;

import org.geockolibre.scrape.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named
public class CredentialsStore {
	private static final Logger LOGGER = LoggerFactory.getLogger(CredentialsStore.class);
	private static final String SETTINGS_FILENAME = System.getProperty("user.home") + 
			File.separator + ".geockolibre"+File.separator+"settings";

	public void eraseCredentials() {
		LOGGER.debug("Clearing credentials from {}", SETTINGS_FILENAME);
		try {
			FileWriter fw = new FileWriter(SETTINGS_FILENAME, false);
			fw.write("C=");
			fw.close();
		} catch (IOException e) {
			// TODO: Pop up error message
		}
	}

	/**
	 *
	 * @param username
	 * @param password
	 */
	public void saveCredentials(Credentials credentials) {
		try {
			if (!credentials.isRememberMe()) {
				eraseCredentials();
			} else {
				LOGGER.debug("Saving credentials to {}", SETTINGS_FILENAME);
				FileWriter fw = new FileWriter(SETTINGS_FILENAME, false);
				fw.write("C=" + credentials.getScramble());
				fw.close();
			}
		} catch (IOException e) {
			// TODO: Pop up error message
		}
	}

	public Credentials loadCredentials() {
		LOGGER.debug("Loading credentials from {}", SETTINGS_FILENAME);
		File f = new File(SETTINGS_FILENAME);
		if (f.exists()) {
			try (BufferedReader br = new BufferedReader(new FileReader(f))) {
				String line;
				while ((line = br.readLine())!= null) {
					String[] tmp = StringUtil.getGroups(line, "^C=(.+)$");
					if (tmp!=null && tmp.length==1) {
						return Credentials.createFromScramble(tmp[0]);
					}
				}
			} catch (IOException e) {
				LOGGER.error("Failed to read credentials ", e);
			}
		} else {
			LOGGER.debug("Failed to read credentials . File {} not found", SETTINGS_FILENAME);
		}
		return new Credentials(null, null, false);
	}
}
