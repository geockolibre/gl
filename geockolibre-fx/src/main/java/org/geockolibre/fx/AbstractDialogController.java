package org.geockolibre.fx;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.geockolibre.fx.util.AbstractController;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.Window;

public abstract class AbstractDialogController extends AbstractController {
	private Stage dialogStage;

	public void setOwner(Window owner) {
		if (dialogStage == null) {
			dialogStage = new Stage();
			dialogStage.setScene(new Scene((Parent)getView()));
			dialogStage.initOwner(owner);
		}
	}
	
	public void show(boolean wait) {
		initDialogStage(dialogStage);
		if (wait) {
			dialogStage.showAndWait();
		} else {
			dialogStage.show();
		}
	}
	
	public void close() {
		dialogStage.hide();
	}
	
	protected Stage getDialogStage() {
		return dialogStage;
	}
	/**
	 * Initialize the dialog {@link Stage}
	 * @param stage the {@link Stage}
	 */
	protected void initDialogStage(Stage stage) {
	}
}
