package org.geockolibre.fx;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;

import org.geockolibre.scrape.Cache;
import org.geockolibre.scrape.Cacher;
import org.geockolibre.scrape.ListSearcher;
import org.geockolibre.scrape.Login;
import org.geockolibre.scrape.ResultFilter;
import org.geockolibre.scrape.Searcher;

public class HidesResultFilter implements ResultFilter {
	private Login login;
	private String targetUser;
	private LocalDate foundSince;
	private int maxCaches;
	private int maxLogs;
	
	@Override
	public int getMaxFind() {
		return maxCaches;
	}
	
	@Override
	public void startSearch(Searcher listSearcher) {
		try {
			((ListSearcher)listSearcher).findCachesOwnedBy(new Cacher(this.targetUser), maxCaches, false);
		} catch (IOException | ParseException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean checkPreDownload(Cache cache) {
		if (foundSince!=null && cache.getLastFoundDate()!=null) {
			LocalDate lastFound=
					Instant.ofEpochMilli(cache.getLastFoundDate()).atZone(ZoneId.systemDefault()).toLocalDate();
			if (lastFound.isBefore(foundSince)) {
				//cache hasn't been found since before lowestFindTime
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean checkPostDownload(Cache cache) {
		// make sure the logs contain the target user's log
		if (!cache.isUnavailableToUs()) {
			// check if "our" log was found
			try {
				while (cache.getLogs().size()<maxLogs
							&& cache.retrieveMoreLogs(login, maxLogs-cache.getLogs().size())) ;
			} catch (Exception e) {
				// TODO: Notify user
				e.printStackTrace();
			}
			cache.limitLogs(maxLogs);
			return true;
		} else {
			return false;
		}
	}

	public void setLogin(Login login) {
		this.login = login;
	}

	public void setFoundSince(LocalDate foundSince) {
		this.foundSince = foundSince;
	}
	
	public void setMaxCaches(int maxCaches) {
		this.maxCaches = maxCaches;
	}
	public void setTargetUser(String targetUser) {
		this.targetUser = targetUser;
	}

	public void setMaxLogs(int maxLogs) {
		this.maxLogs = maxLogs;
	}
}
