package org.geockolibre.fx;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.time.LocalDate;
import java.util.Collection;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Map;

import org.geockolibre.scrape.Attribute;
import org.geockolibre.scrape.CacheSize;
import org.geockolibre.scrape.CacheType;
import org.geockolibre.scrape.DifficultyTerrain;
import org.geockolibre.scrape.DistanceUnitEnum;
import org.geockolibre.scrape.InclusionEnum;
import org.geockolibre.scrape.Location;

public class LocationResultFilterFactory {
	private Location location;
	private Double distance;
	private DistanceUnitEnum distanceUnit;
	private boolean ignoreFoundAndOwn;
	private int maxCaches;
	private boolean checkFavouritePoints;
	private int minFavPoints;
	private boolean checkPlacedOnOrAfter;
	private LocalDate placedOnOrAfter;
	private boolean checkPlacedOnOrBefore;
	private LocalDate placedOnOrBefore;
	
	private boolean containsTrackable;
	
	private boolean checkFoundDate;
	private LocalDate lastFoundDate;
	private boolean notFound;
	
	private boolean ignoreFinds;
	private boolean ignoreOwn;
	private boolean ignoreDisabled;
	
	private boolean checkDT;
	private Collection<DifficultyTerrain> acceptedDts = new HashSet<>();
	private boolean checkSize;
	private Collection<CacheSize> acceptedSizes = EnumSet.noneOf(CacheSize.class);
	private boolean checkType;
	private Collection<CacheType> acceptedTypes = EnumSet.noneOf(CacheType.class);
	private boolean checkAttributes;
	private Map<Attribute, InclusionEnum> attributes = new EnumMap<>(Attribute.class);
	private boolean checkKeyword;
	private String keyword;
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	public LocationResultFilterFactory withLocation(Location location) {
		this.location = location;
		return this;
	}
	public Double getDistance() {
		return distance;
	}
	public void setDistance(Double distance) {
		this.distance = distance;
	}
	public DistanceUnitEnum getDistanceUnit() {
		return distanceUnit;
	}
	public void setDistanceUnit(DistanceUnitEnum distanceUnit) {
		this.distanceUnit = distanceUnit;
	}
	public boolean isIgnoreFoundAndOwn() {
		return ignoreFoundAndOwn;
	}
	public void setIgnoreFoundAndOwn(boolean ignoreFoundAndOwn) {
		this.ignoreFoundAndOwn = ignoreFoundAndOwn;
	}
	public int getMaxCaches() {
		return maxCaches;
	}
	public void setMaxCaches(int maxCaches) {
		this.maxCaches = maxCaches;
	}
	public boolean isCheckFavouritePoints() {
		return checkFavouritePoints;
	}
	public void setCheckFavouritePoints(boolean checkFavouritePoints) {
		this.checkFavouritePoints = checkFavouritePoints;
	}
	public int getMinFavPoints() {
		return minFavPoints;
	}
	public void setMinFavPoints(int minFavPoints) {
		this.minFavPoints = minFavPoints;
	}
	public boolean isCheckPlacedOnOrAfter() {
		return checkPlacedOnOrAfter;
	}
	public void setCheckPlacedOnOrAfter(boolean checkPlacedOnOrAfter) {
		this.checkPlacedOnOrAfter = checkPlacedOnOrAfter;
	}
	public LocalDate getPlacedOnOrAfter() {
		return placedOnOrAfter;
	}
	public void setPlacedOnOrAfter(LocalDate placedOnOrAfter) {
		this.placedOnOrAfter = placedOnOrAfter;
	}
	public boolean isCheckPlacedOnOrBefore() {
		return checkPlacedOnOrBefore;
	}
	public void setCheckPlacedOnOrBefore(boolean checkPlacedOnOrBefore) {
		this.checkPlacedOnOrBefore = checkPlacedOnOrBefore;
	}
	public LocalDate getPlacedOnOrBefore() {
		return placedOnOrBefore;
	}
	public void setPlacedOnOrBefore(LocalDate placedOnOrBefore) {
		this.placedOnOrBefore = placedOnOrBefore;
	}
	public boolean isContainsTrackable() {
		return containsTrackable;
	}
	public void setContainsTrackable(boolean containsTrackable) {
		this.containsTrackable = containsTrackable;
	}
	public boolean isCheckFoundDate() {
		return checkFoundDate;
	}
	public void setCheckFoundDate(boolean checkFoundDate) {
		this.checkFoundDate = checkFoundDate;
	}
	public LocalDate getLastFoundDate() {
		return lastFoundDate;
	}
	public void setLastFoundDate(LocalDate lastFoundDate) {
		this.lastFoundDate = lastFoundDate;
	}
	public boolean isNotFound() {
		return notFound;
	}
	public void setNotFound(boolean notFound) {
		this.notFound = notFound;
	}
	public boolean isIgnoreFinds() {
		return ignoreFinds;
	}
	public void setIgnoreFinds(boolean ignoreFinds) {
		this.ignoreFinds = ignoreFinds;
	}
	public boolean isIgnoreOwn() {
		return ignoreOwn;
	}
	public void setIgnoreOwn(boolean ignoreOwn) {
		this.ignoreOwn = ignoreOwn;
	}
	public boolean isIgnoreDisabled() {
		return ignoreDisabled;
	}
	public void setIgnoreDisabled(boolean ignoreDisabled) {
		this.ignoreDisabled = ignoreDisabled;
	}
	public boolean isCheckDT() {
		return checkDT;
	}
	public void setCheckDT(boolean checkDT) {
		this.checkDT = checkDT;
	}
	public Collection<DifficultyTerrain> getAcceptedDts() {
		return acceptedDts;
	}
	public void setAcceptedDts(Collection<DifficultyTerrain> acceptedDts) {
		this.acceptedDts = acceptedDts;
	}
	public boolean isCheckSize() {
		return checkSize;
	}
	public void setCheckSize(boolean checkSize) {
		this.checkSize = checkSize;
	}
	public Collection<CacheSize> getAcceptedSizes() {
		return acceptedSizes;
	}
	public void setAcceptedSizes(Collection<CacheSize> acceptedSizes) {
		this.acceptedSizes = acceptedSizes;
	}
	public boolean isCheckType() {
		return checkType;
	}
	public void setCheckType(boolean checkType) {
		this.checkType = checkType;
	}
	public Collection<CacheType> getAcceptedTypes() {
		return acceptedTypes;
	}
	public void setAcceptedTypes(Collection<CacheType> acceptedTypes) {
		this.acceptedTypes = acceptedTypes;
	}
	public boolean isCheckAttributes() {
		return checkAttributes;
	}
	public void setCheckAttributes(boolean checkAttributes) {
		this.checkAttributes = checkAttributes;
	}
	public Map<Attribute, InclusionEnum> getAttributes() {
		return attributes;
	}
	public void setAttributes(Map<Attribute, InclusionEnum> attributes) {
		this.attributes = attributes;
	}
	public boolean isCheckKeyword() {
		return checkKeyword;
	}
	public void setCheckKeyword(boolean checkKeyword) {
		this.checkKeyword = checkKeyword;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	
	

}
