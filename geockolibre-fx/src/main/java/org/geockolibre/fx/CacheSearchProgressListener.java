package org.geockolibre.fx;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.geockolibre.scrape.SearchProgressListener;
import org.geockolibre.scrape.util.concurrent.ProgressListener;
import org.springframework.context.MessageSource;

public class CacheSearchProgressListener implements SearchProgressListener {
	private final ProgressListener listener;
	private final MessageSource messageSource;
	
	public CacheSearchProgressListener(ProgressListener listener, MessageSource messageSource) {
		this.listener = listener;
		this.messageSource = messageSource;
	}

	@Override
	public void start() {
		listener.updateTitle(messageSource.getMessage("search.title", new Object[] {}, null));
		listener.updateMessage("");
		listener.updateAction("");
	}

	@Override
	public void updateProgress(double workDone, double max) {
		listener.updateProgress(workDone, max);
		listener.updateAction(messageSource.getMessage("search.progressCount", new Object[] {(int)workDone, (int)max}, null));
	}

	@Override
	public void setScraping(String cacheCode, String cacheName) {
		listener.updateMessage(messageSource.getMessage("search.downloading", new Object[] {cacheCode,cacheName}, null));
	}

	@Override
	public void setFound(int count, int maxFind) {
		listener.updateMessage(messageSource.getMessage(
				maxFind>0?"search.found.withMax":"search.found", new Object[] {count, maxFind}, null));
	}

	@Override
	public void setTotalNumber(int totalCaches) {
		listener.updateAction(messageSource.getMessage("search.populating", new Object[] {totalCaches}, null));
	}

	@Override
	public boolean isCanceled() {
		return listener.isCanceled();
	}
}
