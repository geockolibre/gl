package org.geockolibre.fx;

import org.geockolibre.fx.util.ProgressController;
import org.geockolibre.scrape.util.concurrent.Execution.ExecutionState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.stage.Stage;

public class ProgressDialog extends AbstractDialogController {
	private static final Logger LOGGER = LoggerFactory.getLogger(ProgressDialog.class);
	private ProgressController<?> task;
	
	@FXML ProgressBar progressBar;
	@FXML Label actionLabel;
	@FXML Label progressLabel;
	@FXML Button cancelButton;
	
	public void setProgressController(ProgressController<?> task) {
		this.task = task;
	}
	
	@Override
	protected void initDialogStage(Stage stage) {
		super.initDialogStage(stage);
		progressBar.setProgress(0);
		progressLabel.textProperty().bind(task.messageProperty());
		actionLabel.textProperty().bind(task.actionProperty());
		stage.titleProperty().bind(task.titleProperty());
		progressBar.progressProperty().bind(task.progressProperty());
		task.stateProperty().addListener(this::handleStateChange);
		cancelButton.setOnAction(e-> task.setCancelled());
	}
	
	private void handleStateChange(ObservableValue<? extends ExecutionState> observable, ExecutionState oldState, ExecutionState newState) {
		LOGGER.debug("Progress state changed from {} to {}",oldState,newState);
		switch(newState) {
		case CANCELLED:
		case FAILED:
		case FINISHED:
			progressBar.progressProperty().unbind();
			progressLabel.textProperty().unbind();
			actionLabel.textProperty().unbind();
			getDialogStage().titleProperty().unbind();
			task.stateProperty().removeListener(this::handleStateChange);
			cancelButton.setOnAction(null);
			progressBar.setProgress(1);
			close();
		case NONE:
		case RUNNING:
		case WAITING:
			// ignore
		}
	}
	
}
