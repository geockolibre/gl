package org.geockolibre.fx;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.inject.Inject;

import org.geockolibre.fx.util.AbstractController;
import org.geockolibre.scrape.ResultFilter;

import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory.IntegerSpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.ToggleGroup;

public class SearchFindsController extends AbstractController implements SearchController {
	@FXML ToggleGroup userToggleGroup;
	@FXML TextField otherUserField;
	@FXML RadioButton userMeButton;
	@FXML RadioButton userOtherButton;
	
	@FXML ToggleGroup nrOfCachesToggleGroup;
	@FXML Spinner<Integer> nrLastSpinner;
	@FXML RadioButton nrAllButton;
	@FXML RadioButton nrLastButton;
	@FXML RadioButton nrSinceButton;
	@FXML DatePicker nrSinceField;
	@FXML CheckBox includeFullLogs;
	@FXML Spinner<Integer> nrLogs;
	
	@Inject
	private Identity identity;
	
	public void initialize() {
		otherUserField.disableProperty().bind(userOtherButton.selectedProperty().not());
		nrLastSpinner.disableProperty().bind(nrLastButton.selectedProperty().not());
		nrSinceField.disableProperty().bind(nrSinceButton.selectedProperty().not());
		userMeButton.setSelected(true);
		nrAllButton.setSelected(true);
		nrLastSpinner.setEditable(true);
		nrLastSpinner.setValueFactory(new IntegerSpinnerValueFactory(0, Integer.MAX_VALUE, 5));
		TextFormatter<Integer> resultsFormatter = new TextFormatter<Integer>(nrLastSpinner.getValueFactory().getConverter(), nrLastSpinner.getValueFactory().getValue());
		nrLastSpinner.getEditor().setTextFormatter(resultsFormatter);
		nrLastSpinner.getValueFactory().valueProperty().bindBidirectional(resultsFormatter.valueProperty());		
		nrLogs.setEditable(true);
		nrLogs.disableProperty().bind(includeFullLogs.selectedProperty());
		nrLogs.setValueFactory(new IntegerSpinnerValueFactory(0, Integer.MAX_VALUE, 25, 25));
		TextFormatter<Integer> nrLogsFormatter = new TextFormatter<Integer>(nrLogs.getValueFactory().getConverter(), nrLogs.getValueFactory().getValue());
		nrLogs.getEditor().setTextFormatter(nrLogsFormatter);
		nrLogs.getValueFactory().valueProperty().bindBidirectional(nrLogsFormatter.valueProperty());		
	}

	@Override
	public ResultFilter getResultFilter() {
		FindsResultFilter filter = new FindsResultFilter();
		if (userMeButton.isSelected()) {
			filter.setTargetUser(identity.getLogin().getUserName());
		} else {
			filter.setTargetUser(otherUserField.getText());
		}
		if (nrLastButton.isSelected()) {
			filter.setMaxCaches(nrLastSpinner.getValue());
		} else {
			filter.setMaxCaches(0);
		}
		if (nrSinceButton.isSelected()) {
			filter.setFoundSince(nrSinceField.getValue());
		} else {
			filter.setFoundSince(null);
		}
		filter.setLogin(identity.getLogin());
		filter.setMaxLogs(includeFullLogs.isSelected()?Integer.MAX_VALUE:nrLogs.getValue());
		return filter;
	}

}
