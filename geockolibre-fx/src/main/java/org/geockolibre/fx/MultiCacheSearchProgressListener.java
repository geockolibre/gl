package org.geockolibre.fx;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;

import org.geockolibre.scrape.Location;
import org.geockolibre.scrape.MultiSearchProgressListener;
import org.geockolibre.scrape.util.LocationUtil;
import org.geockolibre.scrape.util.concurrent.ProgressListener;
import org.springframework.context.MessageSource;

public class MultiCacheSearchProgressListener implements MultiSearchProgressListener {
	private final ProgressListener listener;
	private final MessageSource messageSource;
	private double totalDistance;
	private int totalNrLocations;
	private double doneLocations;
	private double doneDistance;
	private int cachesForLocation;
	private int lastTotal;
	
	public MultiCacheSearchProgressListener(ProgressListener listener, MessageSource messageSource) {
		this.listener = listener;
		this.messageSource = messageSource;
	}

	@Override
	public void start() {
		listener.updateTitle(messageSource.getMessage("search.title", new Object[] {}, null));
		listener.updateMessage("");
		listener.updateAction("");
	}
	
	@Override
	public void updateStep(int currentListId, int currentLocationId, List<List<Location>> locations) {
		if (currentListId==0 && currentLocationId == 0) {
			totalDistance = LocationUtil.getTotalDistanceInMForList(locations);
			totalNrLocations = locations.stream().map(l->l.size()).reduce(0, Integer::sum);
		}
		doneDistance = LocationUtil.getTotalDistanceInMForList(locations.subList(0, currentListId))
				+ LocationUtil.getTotalDistanceInM(locations.get(currentListId).subList(0, currentLocationId));
		
		doneLocations = locations.subList(0, currentListId).stream().map(List::size).reduce(0, Integer::sum)+
				currentLocationId;

		listener.updateProgress(doneLocations*100, totalNrLocations*100);
		listener.updateAction(messageSource.getMessage("search.progressDistance", 
				new Object[] {doneDistance/1000, totalDistance/1000},null));
		
	}

	@Override
	public void updateProgress(double workDone, double max) {
		listener.updateProgress(doneLocations*100+(100.0*(workDone-max+cachesForLocation)/cachesForLocation), totalNrLocations*100);
	}

	@Override
	public void setScraping(String cacheCode, String cacheName) {
		listener.updateMessage(messageSource.getMessage("search.downloading", new Object[] {cacheCode,cacheName}, null));
	}

	@Override
	public void setFound(int count, int maxFind) {
		listener.updateMessage(messageSource.getMessage(
				maxFind>0?"search.found.withMax":"search.found", new Object[] {count, maxFind}, null));
	}

	@Override
	public void setTotalNumber(int totalCaches) {
		cachesForLocation=totalCaches - lastTotal;
		lastTotal = totalCaches;
	}

	@Override
	public boolean isCanceled() {
		return listener.isCanceled();
	}
}
