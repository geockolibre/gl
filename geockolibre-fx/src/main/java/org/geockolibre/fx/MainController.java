package org.geockolibre.fx;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.geockolibre.fx.util.AbstractController;
import org.geockolibre.fx.util.FxUtil;
import org.geockolibre.fx.util.ProgressController;
import org.geockolibre.fx.util.SpringFXMLLoader;
import org.geockolibre.scrape.Cache;
import org.geockolibre.scrape.Login;
import org.geockolibre.scrape.ResultFilter;
import org.geockolibre.scrape.SearchErrorHandler.SearchErrorHandlerEnum;
import org.geockolibre.scrape.SearchHandler;
import org.geockolibre.scrape.SearchProgressListener;
import org.geockolibre.scrape.util.concurrent.Execution;
import org.geockolibre.scrape.util.concurrent.MultiThreadUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;

public class MainController extends AbstractController {
	private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);
	@Inject
	private SpringFXMLLoader fxmlLoader;
	@Inject
	private Identity identity;
	@Inject
	private MessageSource messageSource;
	
	private SearchLocationController searchLocationController;
	private SearchHidesController searchHidesController;
	private SearchFindsController searchFindsController;
	private SearchResultDialog searchResultDialog;
	
	@FXML TabPane tabPane;
	@FXML Button exitButton;
	@FXML Button searchButton;

	@PostConstruct 
	public void loadTabs() {
		searchLocationController = fxmlLoader.load("/org/geockolibre/fx/searchLocationTab.fxml");
		searchHidesController = fxmlLoader.load("/org/geockolibre/fx/searchHidesTab.fxml");
		searchFindsController = fxmlLoader.load("/org/geockolibre/fx/searchFindsTab.fxml");
		searchResultDialog = fxmlLoader.load("/org/geockolibre/fx/searchResultDialog.fxml");
	}
	
	public void initialize() {
		tabPane.getTabs().get(0).setContent(searchLocationController.getView());
		tabPane.getTabs().get(1).setContent(searchFindsController.getView());
		tabPane.getTabs().get(2).setContent(searchHidesController.getView());
		tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
		exitButton.setOnAction(e->System.exit(0));
		searchButton.setOnAction(e->doSearch());
	}
	private void doSearch() {
		ResultFilter rf = null;
		ProgressController<Login> progress = new ProgressController<>();
		SearchProgressListener spl = null;
		switch (tabPane.getSelectionModel().getSelectedIndex()) {
		case 0: 
			rf = searchLocationController.getResultFilter();
			spl = searchLocationController.createSearchProgressListener(progress, messageSource, rf);
			break;
		case 1:
			rf = searchFindsController.getResultFilter();
			spl = searchLocationController.createSearchProgressListener(progress, messageSource, rf);
			break;
		case 2:
			rf = searchHidesController.getResultFilter();
			spl = searchLocationController.createSearchProgressListener(progress, messageSource, rf);
			break;
		}
		File tmpFile = null;
		try {
			rf.setProgressListener(progress);
			tmpFile = File.createTempFile("gpxsearchersearch", ".bin");
			tmpFile.deleteOnExit();
			final SearchHandler sh = new SearchHandler(identity.getLogin(), rf, tmpFile,spl, 
					this::handleCacheSearchError, identity.getUserIdManager());
			ProgressDialog progressDialog = fxmlLoader.load("/org/geockolibre/fx/progress.fxml");
			progressDialog.setOwner(getView().getScene().getWindow());
			progressDialog.setProgressController(progress);
			progressDialog.show(false);
			MultiThreadUtil.executeWithCallback((l)->sh.doSearch(), 
					e->FxUtil.runInJfx(()->searchDone(e, sh, progressDialog)), progress);
		} catch (IOException e) {
			LOGGER.error("Problem creating temp file", e);
		}
	}

	private SearchErrorHandlerEnum handleCacheSearchError(Throwable e, Cache cache) {
		final SearchErrorHandlerEnum[] result = new SearchErrorHandlerEnum[] {null};
		FxUtil.runInJfxAndWait(() -> {
			SearchErrorDialog searchErrorDialog = fxmlLoader.load("/org/geockolibre/fx/searchErrorDialog.fxml");
			searchErrorDialog.setOwner(getView().getScene().getWindow());
			searchErrorDialog.setMessage(
				messageSource.getMessage("searchErrorDialog.message", new Object[]{e.getMessage(), cache.getCacheCode()}, null),
				messageSource.getMessage("searchErrorDialog.title", new Object[]{cache.getCacheCode()}, null));
			searchErrorDialog.setOwner(getView().getScene().getWindow());
			result[0] = searchErrorDialog.show();
			fxmlLoader.releaseController(searchErrorDialog);
		});
		return result[0];
	}
	
	private void searchDone(Execution<Integer> execution, SearchHandler sh, ProgressDialog pd) {
		fxmlLoader.releaseController(pd);
		LOGGER.debug("Loaded {} caches of {}", sh.getFound(), sh.getChecked());
		LOGGER.debug("Execution state: {} ",execution.getState());
		LOGGER.debug("Wrote to file {} of size {} ",sh.getOutputFile(), sh.getOutputFile().length());
		if (execution.getError()!=null) {
			
			LOGGER.error("Problem during search", execution.getError());
		}
		
		searchResultDialog.setOwner(getView().getScene().getWindow());
		searchResultDialog.show(sh.getOutputFile(), sh.getFound());
	}
}
