package org.geockolibre.fx;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CredentialsTest {
	private static final Logger LOGGER = LoggerFactory.getLogger(CredentialsTest.class);
	@Test
	public void test() {
		Credentials cr = new Credentials("myname","top-S3c!!!èèt PassgworT", true);
		String credentials = cr.getScramble();
		Credentials unscrambled = Credentials.createFromScramble(credentials);
		LOGGER.debug(credentials);
		Assert.assertEquals(cr.getUsername(), unscrambled.getUsername());
		Assert.assertEquals(cr.getPassword(), unscrambled.getPassword());
	}

	@Test
	public void massiveTest() {
		for (int i=0; i<1000; i++) {
			Credentials cr = new Credentials(
					RandomStringUtils.random(RandomUtils.nextInt(1, 20)),
					RandomStringUtils.random(RandomUtils.nextInt(1, 20)),
					true);
			LOGGER.debug(cr.getScramble());
			Credentials unscrambled = Credentials.createFromScramble(cr.getScramble());
			Assert.assertEquals(cr.getUsername(), unscrambled.getUsername());
			Assert.assertEquals(cr.getPassword(), unscrambled.getPassword());
		}
	}
}
