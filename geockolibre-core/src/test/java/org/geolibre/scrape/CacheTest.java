package org.geolibre.scrape;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import static org.junit.Assert.*;

import org.geockolibre.scrape.Cache;
import org.geockolibre.scrape.LogType;
import org.geockolibre.scrape.Login;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test caches.
 */
public class CacheTest {

    private Login login;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        login = new Login(TestConstants.USERNAME, TestConstants.PASSWORD);
        login.authenticate();
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test method for
     * {@link org.geockolibre.scrape.Cache#populate(org.geockolibre.scrape.Login)}
     * .
     */
    @Test
    public void testPopulate() throws Exception {
        Cache c = new Cache(TestConstants.CACHE_NAME);
        c.populate(login, true);
        assertNotNull(c.getName());
        assertNotNull(c.getCacheCode());
        assertNotNull(c.getShortDescription());
        assertNotNull(c.getLongDescription());
        assertNotNull(c.getHint());
        assertNotNull(c.getLocation());
        assertNotNull(c.getCacheSize());
        assertNotNull(c.getCacheType());
        assertNotNull(c.getOwner());
        assertNotNull(c.getLocationDescription());
        assertTrue(c.getFavourited() >= 0);
        assertTrue(c.getPlacedAt() > 0);
        assertTrue(c.getDifficultyRating() > 0);
        assertTrue(c.getTerrainRating() > 0);
        assertTrue(c.getLogs().size() > 0);

        // make sure the first log is a "published" log
        assertEquals(LogType.PUBLISH_LISTING, c.getLogs().get(c.getLogs().size() - 1).getLogType());

    }

    /**
     * Test that caches with zero east/west coordinates poopulate properly.
     */
    @Test
    public void testPrimeMeridianCache() throws Exception {
        loadNamedCache("GC1R4N3");
    }

    /**
     * Test populating a tricky event cache.
     * 
     * @throws Exception
     */
    @Test
    public void testEventCache() throws Exception {
        loadNamedCache("GC3C68Y");
    }

    @Test
    public void testProblemCaches() throws Exception {
    	loadNamedCache("GC6E2FK");
    	/*
        loadNamedCache("GCGW89");
        loadNamedCache("GCPFHJ");
        loadNamedCache("GC2AYT3");
        loadNamedCache("GC1KBPB");
        loadNamedCache("GC33FA2");
        loadNamedCache("GC33F9H");
        loadNamedCache("GC33F9T");
        loadNamedCache("GC33F9C");
        loadNamedCache("GC33F9M");
        loadNamedCache("GC33F9N");
        loadNamedCache("GC33F9Q");
        loadNamedCache("GC33F94");
        loadNamedCache("GC33F90");
        loadNamedCache("GC2AYT3");
        */
    }

    /**
     * Helper method.
     * 
     * @param gcCode
     * @throws Exception
     */
    private void loadNamedCache(String gcCode) throws Exception {
        Cache c = new Cache(gcCode);
        c.populate(login, true);
        assertNotNull(c.getName());
        assertNotNull(c.getCacheCode());
        assertNotNull(c.getShortDescription());
        assertNotNull(c.getLongDescription());
        assertNotNull(c.getHint());
        assertNotNull(c.getLocation());
        assertNotNull(c.getCacheSize());
        assertNotNull(c.getCacheType());
        assertNotNull(c.getOwner());
        assertNotNull(c.getLocationDescription());
        assertTrue(c.getPlacedAt() > 0);
        assertTrue(c.getDifficultyRating() > 0);
        assertTrue(c.getTerrainRating() > 0);
        assertTrue(c.getLogs().size() > 0);

        System.out.println(c.getLocation());
    }

    /**
     * Test loading some premium-only caches
     * 
     * @throws Exception
     */
    @Test
    public void testPremiumCaches() throws Exception {
        loadNamedPremiumCache("GC2FEMY");
        loadNamedPremiumCache("GC2WR03");
        loadNamedPremiumCache("GC2XKG7");
        loadNamedPremiumCache("GC3CBF4");
        loadNamedPremiumCache("GC1EERQ");
        loadNamedPremiumCache("GC2FEJN");
        loadNamedPremiumCache("GC2FEK1");
        loadNamedPremiumCache("GC2FEKD");
        loadNamedPremiumCache("GC2FJKY");
        loadNamedPremiumCache("GC2GKWM");
        loadNamedPremiumCache("GC2K1R0");
        loadNamedPremiumCache("GC2Y4NG");
        loadNamedPremiumCache("GC32F4X");
    }

    /**
     * Helper method.
     * 
     * @param gcCode
     * @throws Exception
     */
    private void loadNamedPremiumCache(String gcCode) throws Exception {
        Cache c = new Cache(gcCode);
        c.populate(login, true);
        assertNotNull(c.getName());
        assertNotNull(c.getCacheCode());
        assertNotNull(c.getCacheSize());
        assertNotNull(c.getCacheType());
        assertNotNull(c.getOwner());
        assertTrue(c.getDifficultyRating() > 0);
        assertTrue(c.getTerrainRating() > 0);
        assertTrue(c.isPremiumonly());
    }
}
