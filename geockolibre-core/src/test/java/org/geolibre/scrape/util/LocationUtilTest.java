package org.geolibre.scrape.util;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.geockolibre.scrape.Location;
import org.geockolibre.scrape.util.LocationUtil;
import org.junit.Assert;
import org.junit.Test;

public class LocationUtilTest {
	@Test
	public void testDistance() {
		double distanceInM = LocationUtil.distanceInM(new Location("N 50° 03' 59\" W5° 42' 53\""), new Location("N 58° 38' 38\" W3° 04' 12\""));
		Assert.assertEquals(958831l, Math.round(distanceInM));
	}

	@Test
	public void testMidpoint() {
		Location l1 = new Location("N 50° 03' 59\" W5° 42' 53\"");
		Location l2 = new Location("N 58° 38' 38\" W3° 04' 12\"");
		Location l3 = LocationUtil.midPoint(l1, l2);
		// Close enough
		Assert.assertEquals(479414l, Math.round(LocationUtil.distanceInM(l1, l3)));
		Assert.assertEquals(479416l, Math.round(LocationUtil.distanceInM(l2, l3)));
	}

	@Test
	public void testIntermediary() {
		Location l1 = new Location("N50°03'59\" W5°42'53\"");
		Location l2 = new Location("N58°38'38\" W3°04'12\"");
		Location l3 = LocationUtil.midPoint(l1, l2);
		Location l4 = LocationUtil.intermediatePoint(l1, l2, 0.5);
		Assert.assertEquals(0l, Math.round(LocationUtil.distanceInM(l4, l3)));
		
		Location l1i = LocationUtil.intermediatePoint(l1, l2, 0);
		Location l2i = LocationUtil.intermediatePoint(l1, l2, 1);
		Assert.assertEquals(0l, Math.round(LocationUtil.distanceInM(l1, l1i)));
		Assert.assertEquals(0l, Math.round(LocationUtil.distanceInM(l2, l2i)));

		Location l5a = LocationUtil.intermediatePoint(l1, l2, 0.2);
		Location l5b = LocationUtil.intermediatePoint(l1, l2, 0.8);
		Assert.assertTrue(Math.abs(LocationUtil.distanceInM(l1, l5a) - LocationUtil.distanceInM(l2, l5b)) < 2);
		Assert.assertTrue(Math.abs(LocationUtil.distanceInM(l2, l5a) - LocationUtil.distanceInM(l1, l5b)) < 2);
		Assert.assertTrue(Math.abs(LocationUtil.distanceInM(l1, l5a) + LocationUtil.distanceInM(l2, l5a) - 
				LocationUtil.distanceInM(l1, l2)) < 2);
	}
}
