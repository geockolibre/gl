package org.geolibre.scrape;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import static org.junit.Assert.*;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.geockolibre.scrape.Cache;
import org.geockolibre.scrape.CacheType;
import org.geockolibre.scrape.Cacher;
import org.geockolibre.scrape.ListSearcher;
import org.geockolibre.scrape.Location;
import org.geockolibre.scrape.Login;
import org.geockolibre.scrape.MapSearcher;
import org.geockolibre.scrape.SearchCallback;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SearcherTest implements SearchCallback {

    private Login login;
    private List<Location> locs;

    @Before
    public void setUp() throws Exception {
        login = new Login(TestConstants.USERNAME, TestConstants.PASSWORD);
        login.authenticate();
        locs = new ArrayList<Location>();
        // get data from some major cities
        if (TestConstants.MAP_LOCATION != null) {
            locs.add(new Location(TestConstants.MAP_LOCATION));
        }
        // New York
        // locs.add(new Location(40.719, -74.056));
        // Buenos Aires
        locs.add(new Location(-34.603333, -58.381667));
        // Sydney
        locs.add(new Location(-33.859972, 151.211111));
        // London
        locs.add(new Location(51.507222, -0.1275));
        // Hamburg
        // locs.add(new Location(53.565278, 10.001389));
        // CITO cache
        locs.add(new Location(38.79912, -9.19411));
        // webcam cache
        locs.add(new Location(49.68114, 8.61966));
        // gps adventure
        locs.add(new Location(43.71731, -79.34289));
        // ape cache
        locs.add(new Location(-24.26899, -48.41851));

    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testFindCachesAtLocation() throws Exception {
        double totalDistance = 0;
        int foundCount = 0;
        double totalEstDistance = 0;
        int foundEstCount = 0;

        int totalFound = 0;

        double largeDistance = 0;
        double mediumDistance = 0;
        double smallDistance = 0;
        int largeCount = 0;
        int mediumCount = 0;
        int smallCount = 0;
        double largeEstDistance = 0;
        double mediumEstDistance = 0;
        double smallEstDistance = 0;
        int largeEstCount = 0;
        int mediumEstCount = 0;
        int smallEstCount = 0;

        int zoom = 14;
        for (Location loc : locs) {
            MapSearcher searcher = new MapSearcher(login);
            searcher.registerSearchCallback(this);
            System.out.println("Searching " + loc);
            int lat = loc.getLatitudeGsJson(zoom);
            int lon = loc.getLongitudeGsJson(zoom);
            List<Cache> found = searcher.findCachesAtGridSquare(lon, lat, 4, 4, zoom);
            if (found != null) {
                for (Cache c : found) {
                    try {
                        CacheType beforeType = c.getCacheType();
                        Location before = c.getLocation();
                        c.populateFromMap(login);
                        CacheType middleType = c.getCacheType();
                        c.populate(login, false);
                        CacheType afterType = c.getCacheType();
                        verifyType(c, beforeType, middleType, afterType);
                        totalFound++;
                        if (!c.isPremiumonly()) {
                            Location after = c.getLocation();
                            double err = before.distance(after);
                            // System.out.println(err);
                            if (beforeType.isEstimated()) {
                                totalEstDistance += err;
                                foundEstCount++;
                                largeEstDistance += err;
                                largeEstCount++;
                            } else {
                                totalDistance += err;
                                foundCount++;
                                largeDistance += err;
                                largeCount++;
                            }
                        }
                    } catch (Exception e) {
                        System.out.println("Problem with cache " + c.getCacheCode());
                        e.printStackTrace();
                    }
                }
            }
            Thread.sleep(12000);
        }
        zoom = 12;
        for (Location loc : locs) {
            MapSearcher searcher = new MapSearcher(login);
            System.out.println("Searching " + loc);
            int lat = loc.getLatitudeGsJson(zoom);
            int lon = loc.getLongitudeGsJson(zoom);
            List<Cache> found = searcher.findCachesAtGridSquare(lon, lat, 4, 4, zoom);
            if (found != null) {
                for (Cache c : found) {
                    try {
                        CacheType beforeType = c.getCacheType();
                        Location before = c.getLocation();
                        c.populateFromMap(login);
                        CacheType middleType = c.getCacheType();
                        c.populate(login, false);
                        CacheType afterType = c.getCacheType();
                        verifyType(c, beforeType, middleType, afterType);
                        Location after = c.getLocation();
                        totalFound++;
                        if (!c.isPremiumonly()) {
                            double err = before.distance(after);
                            // System.out.println(err);
                            if (beforeType.isEstimated()) {
                                totalEstDistance += err;
                                foundEstCount++;
                                mediumEstDistance += err;
                                mediumEstCount++;
                            } else {
                                totalDistance += err;
                                foundCount++;
                                mediumDistance += err;
                                mediumCount++;
                            }
                        }
                    } catch (Exception e) {
                        System.out.println("Problem with cache " + c.getCacheCode());
                        // System.out.println("Client return code: " +
                        // login.getClient().getResponseCode());
                        e.printStackTrace();
                    }
                }
            }
            Thread.sleep(12000);

        }
        zoom = 10;
        for (Location loc : locs) {
            MapSearcher searcher = new MapSearcher(login);
            System.out.println("Searching " + loc);
            int lat = loc.getLatitudeGsJson(zoom);
            int lon = loc.getLongitudeGsJson(zoom);
            List<Cache> found = searcher.findCachesAtGridSquare(lon, lat, 4, 4, zoom);
            if (found != null) {
                for (Cache c : found) {
                    CacheType beforeType = c.getCacheType();
                    Location before = c.getLocation();
                    c.populateFromMap(login);
                    CacheType middleType = c.getCacheType();
                    c.populate(login, false);
                    CacheType afterType = c.getCacheType();
                    verifyType(c, beforeType, middleType, afterType);
                    Location after = c.getLocation();
                    totalFound++;
                    if (!c.isPremiumonly()) {
                        double err = before.distance(after);
                        // System.out.println(err);
                        if (beforeType.isEstimated()) {
                            totalEstDistance += err;
                            foundEstCount++;
                            smallEstDistance += err;
                            smallEstCount++;
                        } else {
                            totalDistance += err;
                            foundCount++;
                            smallDistance += err;
                            smallCount++;
                        }
                    }
                }
            }
            if (locs.indexOf(loc) < (locs.size() - 1)) {
                Thread.sleep(12000);
            }
        }
        System.out.println("Mean error: " + totalDistance / foundCount);
        System.out.println("Mean est. error: " + totalEstDistance / foundEstCount);
        System.out.println("Mean error, large icons: " + (largeDistance / largeCount));
        System.out.println("Mean error, medium icons: " + (mediumDistance / mediumCount));
        System.out.println("Mean error, small icons: " + (smallDistance / smallCount));
        System.out.println("Mean est. error, large icons: " + (largeEstDistance / largeEstCount));
        System.out.println("Mean est. error, medium icons: " + (mediumEstDistance / mediumEstCount));
        System.out.println("Mean est. error, small icons: " + (smallEstDistance / smallEstCount));

        System.out.println("Found a total of " + totalFound + " caches.");
        System.out.println(foundCount + " non-estimated, " + foundEstCount + " estimated.");
        System.out.println("Large: " + largeCount + " non-estimated, " + largeEstCount + " estimated.");
        System.out.println("Medium: " + mediumCount + " non-estimated, " + mediumEstCount + " estimated.");
        System.out.println("Small: " + smallCount + " non-estimated, " + smallEstCount + " estimated.");
    }

    private void verifyType(Cache c, CacheType beforeType, CacheType middleType, CacheType afterType) {
        try {
            System.out.println("Cache c: beforeType " + beforeType + " Middle " + middleType + " After " + afterType);

            assertEquals(middleType, afterType);

            // check that there are not multiple caches at location
            // or that the cache type does not show up on the map for some other
            // reason
            if (!beforeType.equals(
                    CacheType.MULTIPLE_CACHES_AT_LOCATION) /* && !c.isFound() */
                    && !c.getOwner().getName().equals(TestConstants.USERNAME)) {
                if (beforeType.isEstimated()) {
                    if (beforeType.equals(CacheType.ESTIMATE_TRAD)) {
                        assertOneOf(beforeType, afterType, CacheType.TRADITIONAL, CacheType.PROJECTAPE1,
                                CacheType.PROJECTAPE2, CacheType.LETTERBOX);
                    } else if (beforeType.equals(CacheType.ESTIMATE_EVENT)) {
                        assertOneOf(beforeType, afterType, CacheType.EVENT, CacheType.MEGA_EVENT, CacheType.CITO,
                                CacheType.GPSADVENTURE);

                    } else if (beforeType.equals(CacheType.ESTIMATE_MYST)) {
                        assertOneOf(beforeType, afterType, CacheType.MYSTERY, CacheType.WHERIGO);

                    } else if (beforeType.equals(CacheType.ESTIMATE_VIRT)) {
                        assertOneOf(beforeType, afterType, CacheType.VIRTUAL, CacheType.WEBCAM, CacheType.EARTH_CACHE);

                    } else if (beforeType.equals(CacheType.MULTI)) {
                        assertEquals(beforeType, afterType);
                    }
                } else {
                    assertEquals(beforeType, afterType);
                }
            }
        } catch (AssertionError ae) {
            System.out.println("Cache " + c.getCacheCode());
            ae.printStackTrace();
        }
    }

    private void assertOneOf(CacheType beforeType, CacheType afterType, CacheType... afterTypes) {
        for (CacheType alt : afterTypes) {
            if (afterType.equals(alt)) {
                return;
            }
        }
        fail(afterType + " not expected for " + beforeType);
    }

    @Test
    public void testFindCachesCloseTo() throws Exception {
        Location loc = new Location(TestConstants.MAP_LOCATION);
        ListSearcher searcher = new ListSearcher(login);
        searcher.registerSearchCallback(this);
        List<Cache> found = searcher.findCachesCloseTo(loc, 16.0);
        assertNotNull(found);
        assertTrue(found.size() > 0);
        System.out.println("Found " + found.size() + " caches.");
    }

    @Test
    public void testFindCachesFoundBy() throws Exception {
        ListSearcher searcher = new ListSearcher(login);
        List<Cache> found = searcher.findCachesFoundBy(new Cacher(TestConstants.FINDERNAME));
        assertNotNull(found);
        assertTrue(found.size() > 0);
        System.out.println(TestConstants.FINDERNAME + " found " + found.size() + " caches.");
        for (Cache c : found) {
            System.out.println("Populating " + c.getCacheCode());
            c.populate(login, true);

        }
    }

    @Override
    public void totalNumber(int n) {
        System.out.println("Found " + n + " caches.");
    }

    @Override
    public void found(Cache cache) {
        try {
            System.out.println("Populating " + cache.getCacheCode());
            cache.populate(login, false);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
}
