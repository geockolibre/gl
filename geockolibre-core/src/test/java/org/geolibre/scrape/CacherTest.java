package org.geolibre.scrape;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import static org.junit.Assert.assertNotNull;

import org.geockolibre.scrape.Cacher;
import org.geockolibre.scrape.Login;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test caches.
 */
public class CacherTest {

    private Login login;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        login = new Login(TestConstants.USERNAME, TestConstants.PASSWORD);
        login.authenticate();
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test method for
     * {@link org.geockolibre.scrape.Cache#populate(org.geockolibre.scrape.Login)}
     * .
     */
    @Test
    public void testPopulateById() throws Exception {
        Cacher c = new Cacher(TestConstants.STRANGER);
        c.populate(login);

        Cacher c2 = new Cacher(c.getId());
        c2.populate(login);
        assertNotNull(c2.getName());
        assertNotNull(c2.getProfilePage());
        assertNotNull(c2.getFoundCaches());
    }

    /**
     * Test method for
     * {@link org.geockolibre.scrape.Cache#populate(org.geockolibre.scrape.Login)}
     * .
     */
    @Test
    public void testPopulateByFriendName() throws Exception {
        Cacher c = new Cacher(TestConstants.FRIEND);
        c.populate(login);
        assertNotNull(c.getName());
        assertNotNull(c.getProfilePage());
        assertNotNull(c.getFoundCaches());
    }

    /**
     * Test method for
     * {@link org.geockolibre.scrape.Cache#populate(org.geockolibre.scrape.Login)}
     * .
     */
    @Test
    public void testPopulateByOtherName() throws Exception {
        Cacher c = new Cacher(TestConstants.STRANGER);
        c.populate(login);
        assertNotNull(c.getName());
        assertNotNull(c.getProfilePage());
        assertNotNull(c.getFoundCaches());
    }

    /**
     * Test method for
     * {@link org.geockolibre.scrape.Cache#populate(org.geockolibre.scrape.Login)}
     * .
     */
    @Test
    public void testPopulateByMyself() throws Exception {
        Cacher c = new Cacher(TestConstants.USERNAME);
        c.populate(login);
        assertNotNull(c.getName());
        assertNotNull(c.getProfilePage());
        assertNotNull(c.getFoundCaches());
    }
}
