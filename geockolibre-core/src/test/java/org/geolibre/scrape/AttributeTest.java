package org.geolibre.scrape;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.geockolibre.scrape.Attribute;
import org.junit.Test;

/**
 *
 */
public class AttributeTest {

    /**
     * Test method for
     * {@link org.geockolibre.scrape.Attribute#parse(java.lang.String)}.
     */
    @Test
    public void testParse() {
        int maxlen = 0;
        Attribute[] attrs = Attribute.values();
        for (Attribute attr : attrs) {
            String desc = attr.toString();
            System.out.println("id: " + attr.getId() + ", inc: " + attr.getInc() + ", " + desc);
            maxlen = Math.max(maxlen, desc.length());

        }
        System.out.println(maxlen);
    }

}
