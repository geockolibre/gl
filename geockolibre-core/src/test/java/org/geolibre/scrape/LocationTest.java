package org.geolibre.scrape;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import static org.junit.Assert.*;

import java.util.Random;

import org.geockolibre.scrape.Coordinate;
import org.geockolibre.scrape.Location;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 *
 */
public class LocationTest {

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test method for
     * {@link org.geockolibre.scrape.Location#getLatitudeGsJson(int)}.
     */
    @Test
    public void testGetLatitudeGsJson() throws Exception {
        Location location = new Location();
        location.setLatitude(new Coordinate());
        for (int x = -85; x < 86; x++) {
            location.getLatitude().setDegree(x);
            assertNotNull(location.getLatitudeGsJson(13));
            System.out.println(x + " " + location.getLatitudeGsJson(13));
        }
    }

    /**
     * Test different formats.
     * 
     * @throws Exception
     */
    @Test
    public void testFormats() throws Exception {
        testStandardFormat();
        testDMSFormat();
        testDecimalFormat();
        testDecimalAndLetterFormat();
    }

    /**
     * 
     */
    private void testDecimalAndLetterFormat() {
        // paris
        Location loc = new Location("n 48.856683	e2.3508");
        assertEquals("N 48° 51.400' E 002° 21.048'", loc.toString());
        // moscow
        loc = new Location("N55.7543	E 37.618983");
        assertEquals("N 55° 45.258' E 037° 37.138'", loc.toString());
        // new york
        loc = new Location("N 40.719	W 74.055983");
        assertEquals("N 40° 43.140' W 074° 03.358'", loc.toString());
        // buenos aires
        loc = new Location("S 34.603317	W58.381667");
        assertEquals("S 34° 36.199' W 058° 22.900'", loc.toString());
        // sydney
        loc = new Location("S33.859967	E151.2111");
        assertEquals("S 33° 51.598' E 151° 12.665'", loc.toString());
        // London
        loc = new Location("N 51.507217	W 0.1275");
        assertEquals("N 51° 30.433' W 000° 07.650'", loc.toString());
        // somewhere close to the equator
        loc = new Location("S 0.1275	E 60.234");
        assertEquals("S 00° 07.650' E 060° 14.040'", loc.toString());
        // on prime meridian:
        loc = new Location("S 0.1275	E 0");
        assertEquals("S 00° 07.650' E 000° 00.000'", loc.toString());
        // on equator:
        loc = new Location("N 0	E 60.234");
        assertEquals("N 00° 00.000' E 060° 14.040'", loc.toString());
        // on equator and prime meridian:
        loc = new Location("0 0");
        assertEquals("N 00° 00.000' E 000° 00.000'", loc.toString());

        // some place right on a minute boundary:
        loc = new Location("N 51.5	W 0.116667");
        assertEquals("N 51° 30.000' W 000° 07.000'", loc.toString());
    }

    /**
     * 
     */
    private void testDecimalFormat() {
        // paris
        Location loc = new Location("48.856683	2.3508");
        assertEquals("N 48° 51.400' E 002° 21.048'", loc.toString());
        // moscow
        loc = new Location("55.7543	37.618983");
        assertEquals("N 55° 45.258' E 037° 37.138'", loc.toString());
        // new york
        loc = new Location("40.719	-74.055983");
        assertEquals("N 40° 43.140' W 074° 03.358'", loc.toString());
        // buenos aires
        loc = new Location("-34.603317	-58.381667");
        assertEquals("S 34° 36.199' W 058° 22.900'", loc.toString());
        // sydney
        loc = new Location("-33.859967	151.2111");
        assertEquals("S 33° 51.598' E 151° 12.665'", loc.toString());
        // London
        loc = new Location("51.507217	-0.1275");
        assertEquals("N 51° 30.433' W 000° 07.650'", loc.toString());
        // somewhere close to the equator
        loc = new Location("-0.1275	60.234");
        assertEquals("S 00° 07.650' E 060° 14.040'", loc.toString());
        // on prime meridian:
        loc = new Location("-0.1275	0");
        assertEquals("S 00° 07.650' E 000° 00.000'", loc.toString());
        // on equator:
        loc = new Location("0	60.234");
        assertEquals("N 00° 00.000' E 060° 14.040'", loc.toString());
        // on equator and prime meridian:
        loc = new Location("0 0");
        assertEquals("N 00° 00.000' E 000° 00.000'", loc.toString());

        // some place right on a minute boundary:
        loc = new Location("51.5	-0.116667");
        assertEquals("N 51° 30.000' W 000° 07.000'", loc.toString());

    }

    /**
     * 
     */
    private void testDMSFormat() {
        // paris
        Location loc = new Location("N 48 51' 24.080\" E 2 21' 02.88\"");
        assertEquals("N 48° 51.401' E 002° 21.048'", loc.toString());
        // moscow
        loc = new Location("N 55° 45' 15.480\" E 37° 37' 08.398\"");
        assertEquals("N 55° 45.258' E 037° 37.139'", loc.toString());
        // new york
        loc = new Location("N40 43 08.400 W74 3 21.588");
        assertEquals("N 40° 43.140' W 074° 03.359'", loc.toString());
        // buenos aires
        loc = new Location("S 34 36 11	W 58° 22 54");
        assertEquals("S 34° 36.183' W 058° 22.900'", loc.toString());
        // sydney
        loc = new Location("S 33° 51' 35.892 E 151° 12' 39.960");
        assertEquals("S 33° 51.598' E 151° 12.666'", loc.toString());
        // London
        loc = new Location("N 51° 30 25.992	W 0 07 39");
        assertEquals("N 51° 30.433' W 000° 07.650'", loc.toString());
        // somewhere close to the equator
        loc = new Location("S0° 07 39	E60° 14 02.4");
        assertEquals("S 00° 07.650' E 060° 14.040'", loc.toString());
        // on prime meridian:
        loc = new Location("S0 07 39	E0 00 00");
        assertEquals("S 00° 07.650' E 000° 00.000'", loc.toString());
        // on equator:
        loc = new Location("0 00 00	E60 14 02.4");
        assertEquals("N 00° 00.000' E 060° 14.040'", loc.toString());
        // on equator and prime meridian:
        loc = new Location("N0 00 00	E0 0 0.0");
        assertEquals("N 00° 00.000' E 000° 00.000'", loc.toString());

        // some place right on a minute boundary:
        loc = new Location("N51 30 00.000 W0 7' 00.012");
        assertEquals("N 51° 30.000' W 000° 07.000'", loc.toString());
    }

    /**
     * 
     */
    private void testStandardFormat() {
        // paris
        Location loc = new Location("N 48° 51.401' E 002° 21.048'");
        assertEquals("N 48° 51.401' E 002° 21.048'", loc.toString());
        // moscow
        loc = new Location("N 55° 45.258' E 037° 37.139'");
        assertEquals("N 55° 45.258' E 037° 37.139'", loc.toString());
        // new york
        loc = new Location("N 40° 43.140' W 074° 03.359'");
        assertEquals("N 40° 43.140' W 074° 03.359'", loc.toString());
        // buenos aires
        loc = new Location("S 34° 36.199' W 058° 22.900'");
        assertEquals("S 34° 36.199' W 058° 22.900'", loc.toString());
        // sydney
        loc = new Location("S 33° 51.598' E 151° 12.666'");
        assertEquals("S 33° 51.598' E 151° 12.666'", loc.toString());
        // London
        loc = new Location("N 51° 30.433' W 000° 07.650'");
        assertEquals("N 51° 30.433' W 000° 07.650'", loc.toString());
        // somewhere close to the equator
        loc = new Location("S 00° 07.650' E 060° 14.040'");
        assertEquals("S 00° 07.650' E 060° 14.040'", loc.toString());
        // on prime meridian:
        loc = new Location("S 00° 07.650'  00° 0.00'");
        assertEquals("S 00° 07.650' E 000° 00.000'", loc.toString());
        // on equator:
        loc = new Location(" 00° 0.0' E 060° 14.040'");
        assertEquals("N 00° 00.000' E 060° 14.040'", loc.toString());
        // on equator and prime meridian:
        loc = new Location(" 00° 0.0' 0° 0.00'");
        assertEquals("N 00° 00.000' E 000° 00.000'", loc.toString());

        // some place right on a minute boundary:
        loc = new Location("N 51° 30' W 000° 07'");
        assertEquals("N 51° 30.000' W 000° 07.000'", loc.toString());

    }

    /**
     * Generate a bunch of random coords, convert to string then back again.
     * Check for consistency.
     * 
     * @throws Exception
     */
    @Test
    public void testToString() throws Exception {
        Random rand = new Random();
        for (int x = 0; x < 100000; x++) {
            double north = rand.nextDouble() * 180 - 90;
            double east = rand.nextDouble() * 360 - 180;
            Location loc = new Location(north, east);
            Location nuLoc = new Location(loc.toString());
            assertEquals(loc.toString(), nuLoc.toString());
        }
    }

    @Test
    public void testRationalCoords() throws Exception {
        // test one city in each quadrant

        // paris
        Location loc = new Location(48.8567, 2.3508);
        assertEquals("N 48° 51.401' E 002° 21.048'", loc.toString());
        // moscow
        loc = new Location(55.7543, 37.619);
        assertEquals("N 55° 45.258' E 037° 37.139'", loc.toString());
        // new york
        loc = new Location(40.719, -74.056);
        assertEquals("N 40° 43.140' W 074° 03.359'", loc.toString());
        // buenos aires
        loc = new Location(-34.603333, -58.381667);
        assertEquals("S 34° 36.199' W 058° 22.900'", loc.toString());
        // sydney
        loc = new Location(-33.859972, 151.211111);
        assertEquals("S 33° 51.598' E 151° 12.666'", loc.toString());
        // London
        loc = new Location(51.507222, -0.1275);
        assertEquals("N 51° 30.433' W 000° 07.650'", loc.toString());
        // somewhere close to the equator
        loc = new Location(-0.1275, 60.234);
        assertEquals("S 00° 07.650' E 060° 14.040'", loc.toString());
    }

}
