package org.geolibre.scrape;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * 
 * Some useful constants for testing.
 * 
 */
public class TestConstants {
    /**
     * You must fill in these values before running the JUnit test.
     * 
     * Either change the values here, or set the appropriate environment
     * variables
     */
    public static final String USERNAME = System.getProperty("GEOCKOLIBRE_USERNAME");
    public static final String PASSWORD = System.getProperty("GEOCKOLIBRE_PASSWORD");

    public static final String MAP_LOCATION = "N 50° 48.295 E 003° 14.690";
    public static final String CACHE_NAME = "GC4BPW8";
    public static final String FINDERNAME = USERNAME;
    public static final String STRANGER = "narmandtcap";
    public static final String FRIEND = USERNAME;
}
