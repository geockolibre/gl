package org.geockolibre.gpx;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.geockolibre.scrape.Cache;
import org.geockolibre.scrape.util.UserIdManager;
import org.geockolibre.scrape.util.concurrent.ProgressListener;
import org.xml.sax.SAXException;

/**
 * Generates .gpx formatted output from a list of caches.
 * 
 */
public class GpxFolderWriter extends AbstractGpxWriter {
	private String folderName;
	private boolean overwrite;

	public GpxFolderWriter(int cacheCount, File cacheFile, UserIdManager man, String folderName) {
		super(cacheCount, cacheFile, man);
		this.folderName = folderName;
	}

	public int checkOverwrite() {
		File outputFolder = new File(folderName);
		int nrExistingFiles = 0;
		try {
			ObjectInputStream input = new ObjectInputStream(new FileInputStream(getCacheFile()));
			for (int x = 0; x < getCacheCount(); x++) {
				Cache c = (Cache) input.readObject();
				File outputFile = new File(outputFolder, createFileName(c));
				if (outputFile.exists()) {
					nrExistingFiles++;
				}
			}
			input.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return nrExistingFiles;
	}

	private String createFileName(Cache c) {
		String name = c.getName().replaceAll("[^\\w\\s_-]", "_").trim();
		return c.getCacheCode() + "-" + name + ".gpx";
	}

	public void setOverwrite(boolean overwrite) {
		this.overwrite = overwrite;
	}

	/**
	 * @param os
	 * @throws IOException
	 * @throws SAXException
	 * @throws ClassNotFoundException
	 */
	public void write(File outputFolder, ProgressListener monitor) throws IOException {
		try (ObjectInputStream input = new ObjectInputStream(new FileInputStream(getCacheFile()))) {
			for (int x = 0; x < getCacheCount(); x++) {
				Cache c;
				try {
					c = (Cache) input.readObject();
				} catch (ClassNotFoundException e) {
					throw new IOException(e);
				}
				// add all the logs
				if (monitor.isCanceled()) {
					break;
				}
				monitor.updateProgress(x+1, getCacheCount());
				if (c.isUnavailableToUs()) {
					// skip premium caches if we are not a premium user
					continue;
				}
				monitor.updateMessage("Saving cache " + (x + 1) + "/" + getCacheCount());
	
				File outputFile = new File(outputFolder, createFileName(c));
				if (outputFile.exists() && !overwrite) {
					continue;
				}
		
				try (FileOutputStream fs = new FileOutputStream(outputFile);
						Writer fw = new OutputStreamWriter(fs, "UTF-8")) {
					XMLStreamWriter hd = createStreamWriter(fw);
					hd.writeStartDocument("UTF-8", "1.0");
					writeHeader(hd, CoordinateRange.fromCache(c), c.getCacheCode());
					writeCache(c, hd);
					hd.writeEndElement();
					hd.writeEndDocument();
				} catch (XMLStreamException|ClassNotFoundException e) {
					throw new IOException(e);
				}
			}
		}
	}

	/**
	 * @see org.eclipse.jface.operation.IRunnableWithProgress#run(org.eclipse.core.runtime.IProgressMonitor)
	 */
	public void run(ProgressListener monitor) {
		monitor.updateTitle("Writing output...");
		monitor.updateAction("Writing output...");
		try {
			write(new File(folderName), monitor);
		} catch (IOException e) {
			e.printStackTrace();
		}
		monitor.updateProgress(getCacheCount(), getCacheCount());
	}
}
