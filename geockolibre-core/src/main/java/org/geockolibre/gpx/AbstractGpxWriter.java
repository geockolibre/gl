package org.geockolibre.gpx;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TimeZone;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.lang3.StringUtils;
import org.geockolibre.gpx.sun.internal.IndentingXMLStreamWriter;
import org.geockolibre.scrape.Attribute;
import org.geockolibre.scrape.Cache;
import org.geockolibre.scrape.CacheLog;
import org.geockolibre.scrape.WayPoint;
import org.geockolibre.scrape.util.StringUtil;
import org.geockolibre.scrape.util.UserIdManager;

/**
 * Generates .gpx formatted output from a list of caches.
 * 
 */
public abstract class AbstractGpxWriter {
	private UserIdManager idManager;
	private int cacheCount;
	private File cacheFile;

	public AbstractGpxWriter(int cacheCount, File cacheFile, UserIdManager man) {
		this.cacheCount = cacheCount;
		this.cacheFile = cacheFile;
		this.idManager = man;
	}

	public int getCacheCount() {
		return cacheCount;
	}

	public File getCacheFile() {
		return cacheFile;
	}

	protected XMLStreamWriter createStreamWriter(Writer writer) throws IOException {
		 XMLOutputFactory factory = XMLOutputFactory.newInstance();
		 try {
			return new IndentingXMLStreamWriter(factory.createXMLStreamWriter(writer));
		} catch (XMLStreamException e) {
			throw new IOException(e);
		}
	}

	protected void writeCache(Cache c, XMLStreamWriter hd) throws XMLStreamException, IOException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		dateFormat.setTimeZone(TimeZone.getTimeZone("Z"));

		// add coordinates
		hd.writeStartElement("wpt");
		hd.writeAttribute("lat", c.getLocation().getLatitude().toDecimalString());
		hd.writeAttribute("lon",c.getLocation().getLongitude().toDecimalString());

		// add the cache details
		String cacheType = c.getCacheType().getDescription();
		putElement(hd, "time", dateFormat.format(new Date(c.getPlacedAt())));
		putElement(hd, "name", c.getCacheCode());

		String desc = c.getName() + " by " + c.getHider().getName();
		desc += ", " + cacheType + " (";
		desc += Double.toString(c.getDifficultyRating()) + "/" + Double.toString(c.getTerrainRating()) + ")";
		putElement(hd, "desc", desc);
		putElement(hd, "url", "http://www.geocaching.com/seek/cache_details.aspx?guid=" + c.getGuid());
		putElement(hd, "urlname", c.getName());
		if (c.isFoundByUser()) {
			putElement(hd, "sym", "Geocache Found");
		} else {
			putElement(hd, "sym", "Geocache");
		}
		putElement(hd, "type", "Geocache|" + cacheType);

		hd.writeStartElement("groundspeak:cache");
		hd.writeAttribute("id", Long.toString(c.getId()));
		hd.writeAttribute("available", c.isDisabled()?"False":"True");
		hd.writeAttribute("archived", c.isArchived()?"True":"False");
		hd.writeAttribute("xmlns:groundspeak", "http://www.groundspeak.com/cache/1/0/1");
		putElement(hd, "groundspeak:name", c.getName());
		putElement(hd, "groundspeak:placed_by", c.getHider().getName());

		// put all the log ids in the idmanager database before checking

		for (CacheLog log : c.getLogs()) {
			if (log.getLoggedBy().getId() != null) {
				idManager.setId(log.getLoggedBy(), log.getLoggedBy().getId());
			}
		}

		putElement(hd, "groundspeak:owner", c.getHider().getName(),
				Collections.singletonMap("id", Long.toString(idManager.getId(c.getHider()))));
		putElement(hd, "groundspeak:type", cacheType);
		putElement(hd, "groundspeak:container", c.getCacheSize().toString());
		putElement(hd, "groundspeak:favorite_points", Integer.toString(c.getFavourited()));

		hd.writeStartElement("groundspeak:attributes");
		Map<String, String> atts = new LinkedHashMap<>();
		for (Attribute a : c.getAttributes()) {
			atts.clear();
			atts.put("id", Integer.toString(a.getId()));
			atts.put("inc", Integer.toString(a.getInc()));
			putElement(hd, "groundspeak:attribute", a.toString(), atts);
		}
		hd.writeEndElement();

		putElement(hd, "groundspeak:difficulty", Double.toString(c.getDifficultyRating()));
		putElement(hd, "groundspeak:terrain", Double.toString(c.getTerrainRating()));

		String country = "";
		String state = "";
		String locDesc = c.getLocationDescription();
		if (locDesc.contains(",")) {
			String[] parts = locDesc.split(",");
			state = parts[0].trim();
			country = parts[1].trim();
		} else {
			country = locDesc;
		}
		putElement(hd, "groundspeak:country", country);
		putElement(hd, "groundspeak:state", state);

		atts.clear();
		atts.put("html", "True");
		putElement(hd, "groundspeak:short_description", c.getShortDescription(), atts);
		String longDescription;
		if (c.getWaypoints()!=null && !c.getWaypoints().isEmpty()) {
			longDescription = c.getLongDescription()+'\n'+WayPoint.convertToString(c.getWaypoints(), "Additional waypoints:", true);
		} else {
			longDescription = c.getLongDescription();
		}
		putElement(hd, "groundspeak:long_description", 
				longDescription, 
				atts);
		putElement(hd, "groundspeak:encoded_hints", c.getHint());

		// add the log details
		hd.writeStartElement("groundspeak:logs");
		for (CacheLog log : c.getLogs()) {
			hd.writeStartElement("groundspeak:log");
			hd.writeAttribute("id", Long.toString(log.getId()));
			putElement(hd, "groundspeak:date", dateFormat.format(new Date(log.getLogTime())));
			putElement(hd, "groundspeak:type", log.getLogType().getText());

			atts.clear();

			putElement(hd, "groundspeak:finder", log.getLoggedBy().getName(),
					Collections.singletonMap("id", log.getLoggedBy().getId() != null?Long.toString(log.getLoggedBy().getId()):""));
			atts.clear();

			putElement(hd, "groundspeak:text", log.getText(), Collections.singletonMap("encoded","False"));

			hd.writeEndElement();
		}

		hd.writeEndElement(); // groundspeak:logs
		hd.writeEndElement(); // "groundspeak:cache"
		hd.writeEndElement(); // wpt

		if (c.getWaypoints()!=null && !c.getWaypoints().isEmpty()) {
			/*
			for (WayPoint wp : c.getWaypoints()) {
				// add the waypoint details
				hd.writeStartElement("wpt");
				if (wp.getLocation() != null) {
					hd.writeAttribute("lat", wp.getLocation().getLatitude().toDecimalString());
					hd.writeAttribute("lon", wp.getLocation().getLongitude().toDecimalString());
				}
				putElement(hd, "name", wp.getWaypointCode());
				putElement(hd, "sym", wp.getType());
				putElement(hd, "type", "Waypoint|" + wp.getType());
				hd.writeEndElement();
			}*/
			// Save waypoints as route
			hd.writeStartElement("rte");
			putElement(hd, "name", c.getCacheCode()+" - "+c.getName());
			putElement(hd, "desc", WayPoint.convertToString(c.getWaypoints(), "", false));
			for (WayPoint wp : c.getWaypoints()) {
				// add the waypoint details
				if (wp.getLocation() == null) {
					continue;
				}
				hd.writeStartElement("rtept");
				hd.writeAttribute("lat", wp.getLocation().getLatitude().toDecimalString());
				hd.writeAttribute("lon", wp.getLocation().getLongitude().toDecimalString());
				putElement(hd, "name", wp.getWaypointCode()+"-"+wp.getName());
				if (StringUtils.isBlank(wp.getNote())) {
					putElement(hd, "desc", wp.getName());
				} else {
					putElement(hd, "desc", wp.getName()+" : "+wp.getNote());
					putElement(hd, "cmt", wp.getNote());
				}
				putElement(hd, "sym", wp.getType());
				putElement(hd, "type", "Waypoint|" + wp.getType());
				hd.writeEndElement();
			}
			hd.writeEndElement();
		}

	}

	protected void writeHeader(XMLStreamWriter hd, CoordinateRange range, String name)
			throws XMLStreamException, IOException, FileNotFoundException, ClassNotFoundException {
		// gpx tag.
		hd.writeStartElement("gpx");

		hd.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		hd.writeAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
		hd.writeAttribute("version", "1.0");
		hd.writeAttribute("creator", "Groundspeak Pocket Query");
		hd.writeAttribute("xsi:schemaLocation", 
				"http://www.topografix.com/GPX/1/0 http://www.topografix.com/GPX/1/0/gpx.xsd http://www.groundspeak.com/cache/1/0/1 http://www.groundspeak.com/cache/1/0/1/cache.xsd");
		hd.writeAttribute("xmlns", "http://www.topografix.com/GPX/1/0");
		
		// preamble
		putElement(hd, "name", name);
		// the "(HasChildren)" part is necessary for GSAK to parse child
		// waypoints properly.
		putElement(hd, "desc", "Geocache file generated by GpxWriter (HasChildren)");
		putElement(hd, "author", "GpxWriter");
		putElement(hd, "email", "smobailey@gmail.com");
		Date d = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.S'Z'");
		dateFormat.setTimeZone(TimeZone.getTimeZone("Z"));
		putElement(hd, "time", dateFormat.format(d));
		putElement(hd, "keywords", "cache, geocache, groundspeak");

		hd.writeStartElement("bounds");
		hd.writeAttribute("minlat", Double.toString(range.getMinLat()));
		hd.writeAttribute("minlon", Double.toString(range.getMinLon()));
		hd.writeAttribute("maxlat", Double.toString(range.getMaxLat()));
		hd.writeAttribute("maxlon", Double.toString(range.getMaxLon()));
		hd.writeEndElement();
	}

	protected void putElement(XMLStreamWriter hd, String name, String content) throws XMLStreamException {
		putElement(hd, name, content, null);
	}

	protected void putElement(XMLStreamWriter hd, String name, String content, Map<String,String> atts) throws XMLStreamException {
		hd.writeStartElement(name);
		if (atts!=null) {
			for (Map.Entry<String, String> att: atts.entrySet()) {
				hd.writeAttribute(att.getKey(), att.getValue());
			}
		}
		if (content != null) {
			hd.writeCharacters(replaceNonPrintable(content));
		}
		hd.writeEndElement();
	}

	/**
	 * Replace all non-printable characters with blanks or printable eqivalent.
	 * 
	 * @param input
	 * @return
	 */
	protected String replaceNonPrintable(String input) {
		input = input.replaceAll("\u0007", "\u2407");// bell
		input = input.replaceAll("\u0008", "");// backspace
		input = input.replaceAll("\u001B", "\u241B");// esc
		input = input.replaceAll("\u007F", "");// del
		input = input.replaceAll("\\&#[^;]*;", "");
		return StringUtil.removeNonPrintables2(input);
	}
}
