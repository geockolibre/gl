package org.geockolibre.gpx;

import java.io.EOFException;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.geockolibre.scrape.Cache;
import org.geockolibre.scrape.util.UserIdManager;
import org.geockolibre.scrape.util.concurrent.ProgressListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 * Generates .gpx formatted output from a list of caches.
 * 
 */
public class GpxWriter extends AbstractGpxWriter {
	private static final Logger LOGGER = LoggerFactory.getLogger(GpxWriter.class);
	private String fileName;

	public GpxWriter(int cacheCount, File cacheFile, UserIdManager man, String fileName) {
		super(cacheCount, cacheFile, man);
		this.fileName = fileName;

	}

	/**
	 * @param file
	 * @param monitor
	 * @throws IOException
	 */
	public void write(File file, ProgressListener listener) throws IOException {
		
		try (FileOutputStream fs = new FileOutputStream(file);
				Writer os = new OutputStreamWriter(fs, "UTF-8")) {
			write(os, listener);
		} catch (XMLStreamException e) {
			throw new IOException(e);
		} catch (ClassNotFoundException e) {
			throw new IOException(e);
		}

	}

	/**
	 * @param os
	 * @throws IOException
	 * @throws SAXException
	 * @throws ClassNotFoundException
	 */
	public void write(Writer os, ProgressListener monitor)
			throws IOException, XMLStreamException, ClassNotFoundException {
		XMLStreamWriter hd = createStreamWriter(os);
		hd.writeStartDocument("UTF-8", "1.0");
		writeHeader(hd, CoordinateRange.fromCacheFile(getCacheFile(), getCacheCount()), "Pocket Query");

		// add all the logs
		int cachesRead = 0;
		try (ObjectInputStream input = new ObjectInputStream(new FileInputStream(getCacheFile()))) {
			for (int x = 0; x < getCacheCount(); x++) {
				Cache c = (Cache) input.readObject();
				if (monitor.isCanceled()) {
					break;
				}
				monitor.updateProgress(x+1, getCacheCount());
				if (c.isUnavailableToUs()) {
					// skip premium caches if we are not a premium user
					continue;
				}
				monitor.updateMessage("Saving cache " + (x + 1) + "/" + getCacheCount());
				
				writeCache(c, hd);
				cachesRead++;
			}
			LOGGER.debug("Wrote {} caches to {}",cachesRead, fileName);
		} catch (EOFException e) {
			LOGGER.error("Expected "+getCacheCount()+" caches but only found "+cachesRead, e);
		}
		hd.writeEndElement();
		hd.writeEndDocument();
	}

	/**
	 * @see org.eclipse.jface.operation.IRunnableWithProgress#run(org.eclipse.core.runtime.IProgressMonitor)
	 */
	public void run(ProgressListener monitor) {
		monitor.updateTitle("Writing output...");
		monitor.updateAction("Writing output...");
		try {
			write(new File(fileName), monitor);
		} catch (IOException e) {
			LOGGER.error("Error writing "+fileName, e);
		}
		monitor.updateProgress(getCacheCount(), getCacheCount());

	}

}
