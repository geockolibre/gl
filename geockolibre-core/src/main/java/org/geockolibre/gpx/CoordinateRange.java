package org.geockolibre.gpx;

import java.io.EOFException;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import org.geockolibre.scrape.Cache;
import org.geockolibre.scrape.Location;
import org.geockolibre.scrape.WayPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Range of coordinates.
 */
public class CoordinateRange {
	private static final Logger LOGGER = LoggerFactory.getLogger(CoordinateRange.class);
	private final double minLat;
	private final double minLon;
	private final double maxLat;
	private final double maxLon;

	/**
	 * Create a coordinate range from min and max lat & long
	 */
	public CoordinateRange(double minLat, double minLon, double maxLat, double maxLon) {
		this.minLat = minLat;
		this.minLon = minLon;
		this.maxLat = maxLat;
		this.maxLon = maxLon;
	}

	/**
	 * Create a coordinate range from min and max lat & long
	 */
	private CoordinateRange(Range range) {
		this.minLat = range.minLat;
		this.minLon = range.minLon;
		this.maxLat = range.maxLat;
		this.maxLon = range.maxLon;
	}

	/**
	 * Create a Coordinate range for a single cache
	 */
	public static CoordinateRange fromCache(Cache c) {
		if (c.isUnavailableToUs()) {
			throw new IllegalArgumentException("Cache is unavailable");
		}
		Range r=new Range();
		r.add(c);
		return new CoordinateRange(r);
	}

	/**
	 * Create a coordinate range for all caches in an Object cacheFile
	 * 
	 * @param cacheFile
	 *            the cachefile
	 * @param cacheCount
	 *            the number of caches
	 * @return the coordinates
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static CoordinateRange fromCacheFile(File cacheFile, int cacheCount)
			throws IOException, ClassNotFoundException {
		Range r = new Range();
		int cachesRead = 0;
		try (ObjectInputStream input = new ObjectInputStream(new FileInputStream(cacheFile))) {
			for (int x = 0; x < cacheCount; x++) {
				Cache c = (Cache) input.readObject();
				if (!c.isUnavailableToUs()) {
					r.add(c);
				}
				cachesRead++;
			}
		} catch (EOFException e) {
			LOGGER.error("Expected "+cacheCount+" caches but only found "+cachesRead, e);
		}
		return new CoordinateRange(r);
	}

	public double getMinLat() {
		return minLat;
	}

	public double getMinLon() {
		return minLon;
	}

	public double getMaxLat() {
		return maxLat;
	}

	public double getMaxLon() {
		return maxLon;
	}

	private static class Range {
		private double minLat = Double.POSITIVE_INFINITY;
		private double minLon = Double.POSITIVE_INFINITY;
		private double maxLat = Double.NEGATIVE_INFINITY;
		private double maxLon = Double.NEGATIVE_INFINITY;
		private void add(Location loc) {
			if (loc!=null) {
				double lat = loc.getLatitude().getDegreeWithFraction();
				double lon = loc.getLongitude().getDegreeWithFraction();
				minLat = Math.min(minLat, lat);
				minLon = Math.min(minLon, lon);
				maxLat = Math.max(maxLat, lat);
				maxLon = Math.max(maxLon, lon);
			}
		}
		private void add(Cache c) {
			add(c.getLocation());
			if (c.getWaypoints()!=null) {
				for (WayPoint wp : c.getWaypoints()) {
					add(wp.getLocation());
				}
			}
		}
	}
}
