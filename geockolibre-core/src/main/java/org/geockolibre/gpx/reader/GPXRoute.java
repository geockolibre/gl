package org.geockolibre.gpx.reader;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import java.util.ArrayList;

public class GPXRoute extends ArrayList<GPXRoutePoint> {
	private static final long serialVersionUID = 2215326055872250470L;

	public GPXRoute(XML trkseg) {
		for (XML child : trkseg.getChildren("rtept")) {
			add(new GPXRoutePoint(child));
		}
	}
}
