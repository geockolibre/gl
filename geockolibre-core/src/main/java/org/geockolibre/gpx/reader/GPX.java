package org.geockolibre.gpx.reader;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

/** contains a collection of GPXTrack , GPXRoute objects and GPXWayPoint objects */
public class GPX {

	public static boolean debug = false;


	private List<GPXWayPoint> wayPoints = new ArrayList<>();
	private List<GPXTrack> tracks = new ArrayList<>();
	private List<GPXRoute> routes = new ArrayList<>();

	public GPX() {
	}

	public void parse(File file) throws IOException {
		try {
			XML xmldata = new XML(file, null);
			for (XML xmlthing : xmldata.getChildren()) {
				if (xmlthing.getName().equals("trk")) {
					tracks.add(new GPXTrack(xmlthing));
				} else if (xmlthing.getName().equals("rte")) {
					routes.add(new GPXRoute(xmlthing));
				} else if (xmlthing.getName().equals("wpt")) {
					wayPoints.add(new GPXWayPoint(xmlthing));
				}
			}
		} catch (SAXException | ParserConfigurationException e) {
			throw new IOException (e.getMessage(), e);
		}
	}

	public List<GPXTrack> getTracks() {
		return tracks;
	}
	public List<GPXWayPoint> getWayPoints() {
		return wayPoints;
	}
	public List<GPXRoute> getRoutes() {
		return routes;
	}
}
