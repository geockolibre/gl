package org.geockolibre.gpx.reader;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * This is the base class used for the Processing XML library, representing a
 * single node of an XML tree.
 */
public class XML {

	/** The internal representation, a DOM node. */
	private Node node;

	/** Child elements, once loaded. */
	private List<XML> children;

	/**
	 * Advanced users only; use loadXML() in PApplet.
	 *
	 * @nowebref
	 */
	public XML(File file, String options) throws IOException, ParserConfigurationException, SAXException {
		this(new FileInputStream(file), options);
	}

	/**
	 * Unlike the loadXML() method in PApplet, this version works with files
	 * that are not in UTF-8 format.
	 *
	 * @nowebref
	 */
	private XML(InputStream input, String options) throws IOException, ParserConfigurationException, SAXException {
		// this(PApplet.createReader(input), options); // won't handle non-UTF8
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		try {
			// Prevent 503 errors from www.w3.org
			factory.setAttribute("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
		} catch (IllegalArgumentException e) {
			// ignore this; Android doesn't like it
		}

		factory.setExpandEntityReferences(false);
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(new InputSource(input));
		node = document.getDocumentElement();
	}
	
	/**
	 * @nowebref
	 */
	private XML(Node node) {
		this.node = node;
		// this.name = node.getNodeName();
	}

	/**
	 * Returns the full name (i.e. the name including an eventual namespace
	 * prefix) of the element.
	 *
	 * @webref xml:method
	 * @brief Gets the element's full name
	 * @return the name, or null if the element only contains #PCDATA.
	 */
	public String getName() {
		return node.getNodeName();
	}

	/**
	 * Honey, can you just check on the kids? Thanks.
	 *
	 * Internal function; not included in reference.
	 */
	private void checkChildren() {
		if (children == null) {
			NodeList kids = node.getChildNodes();
			int childCount = kids.getLength();
			children = new ArrayList<XML>(childCount);
			for (int i = 0; i < childCount; i++) {
				children.add(new XML(kids.item(i)));
			}
		}
	}

	/**
	 * Returns an array containing all the child elements.
	 *
	 * @webref xml:method
	 * @brief Returns an array containing all child elements
	 */
	public List<XML> getChildren() {
		checkChildren();
		return children;
	}

	/**
	 * Get any children that match this name or path. Similar to getChild(), but
	 * will grab multiple matches rather than only the first.
	 *
	 * @param name
	 *            element name or path/to/element
	 * @return array of child elements that match
	 * @author processing.org
	 */
	public List<XML> getChildren(String name) {
		checkChildren();
		return children.stream().filter(x->name.equals(x.getName())).collect(Collectors.toList());
	}

	/**
	 * @webref xml:method
	 * @brief Gets the content of an attribute as a String
	 */
	public String getString(String name) {
		NamedNodeMap attrs = node.getAttributes();
		if (attrs != null) {
			Node attr = attrs.getNamedItem(name);
			if (attr != null) {
				return attr.getNodeValue();
			}
		}
		return null;
	}

	public double getDouble(String name) {
		String value = getString(name);
		return (value == null) ? 0 : Double.parseDouble(value);
	}
}