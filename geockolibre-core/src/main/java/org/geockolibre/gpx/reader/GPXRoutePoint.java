package org.geockolibre.gpx.reader;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.geockolibre.scrape.Location;

public class GPXRoutePoint {
	public final Location location;

	public GPXRoutePoint() {
		location = new Location();
	}

	public GPXRoutePoint(double lat, double lon) {
		location = new Location(lat, lon);
	}

	/**
	 * parses a GPXRoutePoint from the given XML
	 * with and without milliseconds
	 */
	public GPXRoutePoint(XML trkpt) {

		// NB:- this is a bit more complex than it should be
		// but it should handle a wider variety of broken
		// GPX files, and let's face it that's why you're using
		// a library instead of parsing this file yourself
		double lat;
		try {
			String sLat = trkpt.getString("lat");
			lat = Double.parseDouble(sLat);
		} catch (Exception e) {
			if (GPX.debug) {
				e.printStackTrace();
				System.err.println("error parsing lat in: " + trkpt);
			}
			lat = 0.0;
		}
		double lon;
		try {
			String sLon = trkpt.getString("lon");
			lon = Double.parseDouble(sLon);
		} catch (Exception e) {
			if (GPX.debug) {
				e.printStackTrace();
				System.err.println("error parsing lon in: " + trkpt);
			}
			lon = 0.0;
		}

		this.location = new Location(lat,lon);
	}

	public Location getLocation() {
		return location;
	}
}
