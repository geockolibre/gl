package org.geockolibre.gpx.reader;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import org.geockolibre.scrape.Location;

/**
 * simple GPX waypoint, only understands latitude, longitude
 */
public class GPXWayPoint {

	public final Location location;

	public GPXWayPoint() {
		this.location = new Location();
	}

	public GPXWayPoint(double lat, double lon) {
		this.location = new Location(lat,lon);
	}

	/** parses a GPXWayPoint from the given XML */
	public GPXWayPoint(XML trkpt) {
		double lat = trkpt.getDouble("lat");
		double lon = trkpt.getDouble("lon");
		this.location = new Location(lat,lon);
	}

	public Location getLocation() {
		return location;
	}
}
