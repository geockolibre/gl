package org.geockolibre.scrape;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

public enum LogType {
    ANNOUNCEMENT, ATTENDED, WILL_ATTEND, RETRACT_LISTING, FOUND_IT, DID_NOT_FIND, WRITE_NOTE, NEEDS_ARCHIVED, NEEDS_MAINTENANCE, UNARCHIVED, PUBLISH_LISTING, ENABLE_LISTING, DISABLED, OWNER_MAINTENANCE, WEBCAM_PHOTO_TAKEN, UPDATE_COORDINATES, POST_REVIEWER_NOTE, UNARCHIVE, ARCHIVE;
	private static final Logger LOGGER = LoggerFactory.getLogger(LogType.class);

    public static LogType parse(String type) {
        try {
            String convertedType = type.toUpperCase();
            convertedType = convertedType.replaceAll(" ", "_");
            convertedType = convertedType.replaceAll("\\!", "");
            convertedType = convertedType.replaceAll("-", "");
            convertedType = convertedType.replaceAll("/", "");
            convertedType = convertedType.replaceAll("\\\\", "_");
            LogType res = LogType.valueOf(convertedType);
            return res;
        } catch (IllegalArgumentException e) {
            // not found
        }
        if (type.equals("Didn't find it")) {
            return DID_NOT_FIND;
        } else if (type.equals("Temporarily Disable Listing")) {
            return DISABLED;
        } else {
        	LOGGER.error("Don't know log type \"" + type + "\"");
            return null;
            // throw new IllegalArgumentException("Don't know log type \"" +
            // type +"\"");
        }
    }

    public String getText() {
        String res = "";
        if (this.equals(DID_NOT_FIND)) {
            res = "Didn't find it";
        } else if (this.equals(DISABLED)) {
            res = "Temporarily Disable Listing";
        } else {
            res = this.name();
            res = res.replaceAll("_", " ");
            res = res.toLowerCase();
            res = res.substring(0, 1).toUpperCase() + res.substring(1);
        }
        return res;
    }
}
