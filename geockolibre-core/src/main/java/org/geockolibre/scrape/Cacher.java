package org.geockolibre.scrape;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;

import org.geockolibre.scrape.util.HtmlParser;
import org.geockolibre.scrape.util.StringUtil;
import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Data object for a Cacher
 * TODO Move the Cache page parsing to a separate object
 */
public class Cacher implements Serializable {
    private static final long serialVersionUID = -8770402232284898399L;
    private String profilePage;
    private String name;
    private Long id;
    private Long foundCaches;
    private Boolean premiumMember;

    /**
     * Constructor
     * @param name the cacher name
     * @param url the url to the profile page
     */
    public Cacher(String name, String url) {
        this.name = name;
        this.profilePage = url;
    }

    /**
     * Constructor
     * @param name the cacher name
     */
    public Cacher(String name) {
        this.name = name;
    }

    /**
     * Constructor
     * @param id the cacher id
     */
    public Cacher(Long id) {
        this.setId(id);
    }
    @Override
    public int hashCode() {
        return System.identityHashCode(this);
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Cacher other = (Cacher) obj;
        if (other.getId() != null && getId() != null) {
            return other.getId() == getId();
        } else if (other.getName() != null && getName() != null) {
            return other.getName().equals(getName());
        }
        return false;
    }
    

    public String getProfilePage() {
        return profilePage;
    }

    public void setProfilePage(String profilePage) {
        this.profilePage = profilePage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getFoundCaches() {
        return foundCaches;
    }

    public void setFoundCaches(Long foundCaches) {
        this.foundCaches = foundCaches;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isPremiumMember() {
        return premiumMember;
    }

    public void setPremiumMember(Boolean isPremiumMember) {
        this.premiumMember = isPremiumMember;
    }

    /**
     * Retrieve the profile page for this user
     * @param login the login to connect to geocaching.com
     * @return the profile page
     * @throws IOException in case we cannot connect to geocaching.com
     */
    private String retrieveProfilePage(Login login) throws IOException {
        String res = null;
        final Connection detailConnection = login.getConnection("https://www.geocaching.com/my/myfriends.aspx");
        Document myFriends = detailConnection.get();
        for (Element myFriend : myFriends.select("a[href^=https://www.geocaching.com/profile/?guid")) {
            if (myFriend.text().equals(getName())) {
                return myFriend.attr("href");
            }
        }

        HashMap<String, String> values = HtmlParser.getViewState(myFriends);

        values.put("ctl00$ContentBody$FindUserPanel1$txtUsername", getName());
        values.put("ctl00$ContentBody$FindUserPanel1$GetUsers", "Go");
        values.put("__EVENTTARGET", "");
        values.put("__EVENTARGUMENT", "");

        Connection requestConnection = login.getConnection("https://www.geocaching.com/my/myfriends.aspx");
        Connection.Response response = requestConnection.data(values).method(Method.POST).execute();
        Document userDetail = response.parse();

        Elements profileLink = userDetail.select("#ctl00_ContentBody_SendMessagePanel1_lblEmailInfo a");
        if (!profileLink.isEmpty()) {
            res = profileLink.attr("href");
        } else {
            Elements selectUserSelect = userDetail.select("#ctl00_ContentBody_FindUserPanel1_ddListUsers option");
            for (Element option : selectUserSelect) {
                if (getName().equals(option.text())) {
                    res = "https://www.geocaching.com/profile/?guid=" + option.attr("value");
                    break;
                }
            }
        }
        return res;
    }

    /**
     * Get the Cacher profile page by url
     * @param login the login to connect to geocaching.com
     * @return the profile page Document
     * @throws IOException ion case we cannot read geocaching.com
     */
    private Document retrieveByProfilePage(Login login) throws IOException {
        return login.getDocument(getProfilePage());
    }

    /**
     * Get the Cacher profile page by cacher id
     * @param login the login to connect to geocaching.com
     * @return the profile page Document
     * @throws IOException ion case we cannot read geocaching.com
     */
    private Document retrieveById(Login login) throws IOException {
        return login.getDocument("https://www.geocaching.com/profile/?id=" + Long.toString(getId()));
    }

    /**
     * Extract the cacher id from his profile page
     * @param content the cacher profile page
     * @return the cacher id
     */
    private Long extractId(Document content) {
        Element midLink = content.getElementById("ctl00_ContentBody_ProfilePanel1_lnkSeePosts");
        if (midLink != null) {
            String idString = StringUtil.getFirstGroup(midLink.attr("href"), "mid=([0-9]+)");
            id = Long.parseLong(idString);
            return id;
        }
        return null;
    }

    /**
     * Extract the cacher name from his profile page
     * @param contents the cacher profile page
     * @return the cacher name
     */
    private String extractName(Document contents) {
        Element nameEl = contents.getElementById("ctl00_ContentBody_ProfilePanel1_lblMemberName");
        if (nameEl != null) {
            name = nameEl.text();
            return name;
        }
        return null;
    }

    /**
     * Extract the cacher profile page from his profile page
     * @param contents the cacher profile page
     * @return the cacher profile page url
     */
    private String extractProfilePage(Document contents) {
        Element nameEl = contents.getElementById("ctl00_ContentBody_ProfilePanel1_lnkGiftSubscription");
        if (nameEl != null) {
            String guid = StringUtil.getFirstGroup(nameEl.attr("href"), "guid=([^&]+)");
            return "https://www.geocaching.com/profile/?guid=" + guid;
        }
        return null;
    }

    /**
     * Extract if the cacher is a premium member from his profile page
     * @param contents the cacher profile page
     * @return if the cacher is  a premium member
     */
    private Boolean extractPremiumMember(Document contents) {
        Element membership = contents.getElementById("ctl00_ContentBody_ProfilePanel1_lblStatusText");
        if (membership != null) {
            return membership.text().contains("Premium");
        }
        return null;
    }

    /**
     * Populate the missing data in this object from the internet.
     * 
     * @param login the login to connect to geocaching.com
     * @throws IOException ion case we cannot read geocaching.com
     */
    // CHECKSTYLE:OFF:JavaNCSS FOR 1 LINE
    public void populate(Login login) throws IOException {
        // don't populate if we don't have to
        if (getName() != null && getId() != null && isPremiumMember() != null && getProfilePage() != null) {
            return;
        }
        Document profileContents = null;
        if (this.getProfilePage() != null) {
            // we have the guid, just grab the page
            profileContents = retrieveByProfilePage(login);
        } else if (this.getId() != null) {
            // no guid, but id number
            profileContents = retrieveById(login);
        } else if (this.getName() != null) {
            // neither guid nor id, so look up the name
            this.setProfilePage(retrieveProfilePage(login));
            profileContents = retrieveByProfilePage(login);
        } else {
            // not enough information to identify cacher.
            return;
        }

        if (getName() == null) {
            setName(extractName(profileContents));
        }
        if (getId() == null) {
            setId(extractId(profileContents));
        }
        if (getProfilePage() == null) {
            this.setProfilePage(extractProfilePage(profileContents));
        }
        if (isPremiumMember() == null) {
            setPremiumMember(extractPremiumMember(profileContents));
        }
        Element foundEl = profileContents.getElementById("ctl00_ContentBody_ProfilePanel1_Panel_CachesFound");
        if (foundEl != null) {
            setFoundCaches(Long.parseLong(foundEl.ownText().replaceAll("[^\\d]", "")));
        }
    }

}
