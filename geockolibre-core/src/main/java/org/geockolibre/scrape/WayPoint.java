package org.geockolibre.scrape;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;
import java.util.Collection;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * A waypoint that forms part of a cache.
 * 
 * This is usually a parking area, a step in a multicache, a trailhead, or
 * similar.
 * 
 */
public class WayPoint extends BasePlace implements Serializable {
    private static final long serialVersionUID = 1476745457841494363L;
    private Cache partOf;
    private String viewable;
    private String lookup;
    private String prefix;
    private String type;
    private String note;

    /**
     * Set the cache this waypoint is part of
     * 
     * @param partOf
     */
    public void setPartOf(Cache partOf) {
        this.partOf = partOf;
    }

    public String getViewable() {
        return viewable;
    }

    public void setViewable(String viewable) {
        this.viewable = viewable;
    }

    /**
     * Get the cache this waypoint is part of.
     * 
     * @return
     */
    public Cache getPartOf() {
        return partOf;
    }

    /**
     * @return the lookup
     */
    public String getLookup() {
        return lookup;
    }

    /**
     * @param lookup
     *            the lookup to set
     */
    public void setLookup(String lookup) {
        this.lookup = lookup;
    }

    /**
     * @return the prefix
     */
    public String getPrefix() {
        return prefix;
    }

    /**
     * @param prefix
     *            the prefix to set
     */
    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    /**
     * This is the name used in .gpx files for waypoints
     */
    public String getWaypointCode() {
        String cacheCode = this.partOf.getCacheCode();
        // strip leading chars
        cacheCode = cacheCode.substring(2);
        return getPrefix() + cacheCode;
    }

    /**
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return this.type;
    }
    
    public String getNote() {
		return note;
	}
    public void setNote(String note) {
		this.note = note;
	}
    
    public static String convertToString(Collection<WayPoint> waypoints, String label, boolean html) {
    	StringBuilder sb = new StringBuilder();
    	if (html) {
    		sb.append("<p>");
    	}
    	if (StringUtils.isNotBlank(label)) {
    		sb.append(label);
			sb.append('\n');
    	}
    	for (WayPoint wp : waypoints) {
    		if (html) {
    			sb.append("<br />");
    		}
			sb.append(wp.getName());
			sb.append(" (");
			sb.append(wp.getPrefix());
			sb.append("):");
			sb.append(wp.getNote());
			sb.append('\n');
    	}
    	if (html) {
    		sb.append("</p>");
    	}
    	return sb.toString();
    }
}
