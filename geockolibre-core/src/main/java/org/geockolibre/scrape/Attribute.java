package org.geockolibre.scrape;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Cache Attributes.
 */
public enum Attribute {
    // Enum values are self describing, no javadoc needed
    // CHECKSTYLE:OFF:JavadocVariable
    DOGS_YES(1,1,"Dogs allowed"),
    DOGS_NO(1,0,"Dogs not allowed"),
    FEE_YES(2,1,"Access or parking fee"),
    RAPPELLING_YES(3,1,"Climbing gear required "),
    BOAT_YES(4,1,"Boat required"),
    SCUBA_YES(5,1,"Scuba gear required"),
    KIDS_YES(6,1,"Recommended for kids"),
    KIDS_NO(6,0,"Not recommended for kids"),
    ONEHOUR_YES(7,1,"Takes less than an hour"),
    ONEHOUR_NO(7,0,"Does not take less than an hour"),
    SCENIC_YES(8,1,"Scenic view"),
    SCENIC_NO(8,0,"No scenic view"),
    HIKING_YES(9,1,"Significant hike"),
    HIKING_NO(9,0,"No significant hike"),
    CLIMBING_YES(10,1,"Difficult climbing"),
    CLIMBING_NO(10,0,"No difficult climbing"),
    WADING_YES(11,1,"May require wading"),
    SWIMMING_YES(12,1,"May require swimming"),
    AVAILABLE_YES(13,1,"Available at all times"),
    AVAILABLE_NO(13,0,"Not available at all times"),
    NIGHT_YES(14,1,"Recommended at night"),
    NIGHT_NO(14,0,"Not recommended at night"),
    WINTER_YES(15,1,"Available during winter"),
    WINTER_NO(15,0,"Not available during winter"),
    POISONOAK_YES(17,1,"Poison plants"),
    POISONOAK_NO(17,0,"No poison plants"),
    DANGEROUSANIMALS_YES(18,1,"Dangerous Animals"),
    TICKS_YES(19,1,"Ticks"),
    MINE_YES(20,1,"Abandoned mines"),
    CLIFF_YES(21,1,"Cliff / falling rocks"),
    HUNTING_YES(22,1,"Hunting"),
    DANGER_YES(23,1,"Dangerous area"),
    WHEELCHAIR_YES(24,1,"Wheelchair accessible"),
    WHEELCHAIR_NO(24,0,"Not wheelchair accessible"),
    PARKING_YES(25,1,"Parking available"),
    PARKING_NO(25,0,"No parking available"),
    PUBLIC_YES(26,1,"Public transportation"),
    WATER_YES(27,1,"Drinking water nearby"),
    WATER_NO(27,0,"No drinking water nearby"),
    RESTROOMS_YES(28,1,"Public restrooms nearby"),
    RESTROOMS_NO(28,0,"No public restrooms nearby"),
    PHONE_YES(29,1,"Telephone nearby"),
    PHONE_NO(29,0,"No telephone nearby"),
    PICNIC_YES(30,1,"Picnic tables nearby"),
    PICNIC_NO(30,0,"No picnic tables nearby"),
    CAMPING_YES(31,1,"Camping available"),
    CAMPING_NO(31,0,"Camping not available"),
    BICYCLES_YES(32,1,"Bicycles allowed"),
    BICYCLES_NO(32,0,"Bicycles not allowed"),
    MOTORCYCLES_YES(33,1,"Motorcycles allowed"),
    MOTORCYCLES_NO(33,0,"Motorcycles now allowed"),
    QUADS_YES(34,1,"Quads allowed"),
    QUADS_NO(34,0,"Quads not allowed"),
    JEEPS_YES(35,1,"Off-road vehicles allowed"),
    JEEPS_NO(35,0,"Off-road vehicles not allowed"),
    SNOWMOBILES_YES(36,1,"Snowmobiles allowed"),
    SNOWMOBILES_NO(36,0,"Snowmobiles not allowed"),
    HORSES_YES(37,1,"Horses allowed"),
    HORSES_NO(37,0,"Horses not allowed"),
    CAMPFIRES_YES(38,1,"Campfires allowed"),
    CAMPFIRES_NO(38,0,"Campfires not allowed"),
    THORN_YES(39,1,"Thorns"),
    STEALTH_YES(40,1,"Stealth required"),
    STEALTH_NO(40,0,"Stealth not required"),
    STROLLER_YES(41,1,"Stroller accessible"),
    STROLLER_NO(41,0,"Not stroller accessible"),
    FIRSTAID_YES(42,1,"Needs maintenance"),
    COW_YES(43,1,"Watch for livestock"),
    FLASHLIGHT_YES(44,1,"Flashlight required"),
    LANDF_YES(45,1,"Lost and Found Tour"),
    LANDF_NO(45,0,"Not Lost and Found Tour"),
    RV_YES(46,1,"Truck Driver/RV"),
    RV_NO(46,0,"No Truck Driver/RV"),
    FIELD_PUZZLE_YES(47,1,"Field Puzzle"),
    FIELD_PUZZLE_NO(47,0,"Not Field Puzzle"),
    UV_YES(48,1,"UV Light required"),
    SNOWSHOES_YES(49,1,"Snowshoes required"),
    SKIIS_YES(50,1,"Cross Country Skis required"),
    S_TOOL_YES(51,1,"Special Tool required"),
    NIGHTCACHE_YES(52,1,"Night Cache"),
    NIGHTCACHE_NO(52,0,"Not Night Cache"),
    PARKNGRAB_YES(53,1,"Park and grab"),
    PARKNGRAB_NO(53,0,"Not Park and grab"),
    ABANDONEDBUILDING_YES(54,1,"Abandoned structure"),
    ABANDONEDBUILDING_NO(54,0,"Not Abandoned structure"),
    HIKE_SHORT_YES(55,1,"Short hike"),
    HIKE_SHORT_NO(55,0,"Not Short hike"),
    HIKE_MED_YES(56,1,"Medium Hike"),
    HIKE_MED_NO(56,0,"Not Medium Hike"),
    HIKE_LONG_YES(57,1,"Long Hike"),
    HIKE_LONG_NO(57,0,"Not Long Hike"),
    FUEL_YES(58,1,"Fuel nearby"),
    FUEL_NO(58,0,"No fuel nearby"),
    FOOD_YES(59,1,"Food nearby"),
    FOOD_NO(59,0,"No food nearby"),
    WIRELESSBEACON_YES(60,1,"Wireless Beacon"),
    PARTNERSHIP_YES(61,1,"Partnership cache"),
    PARTNERSHIP_NO(61,0,"Not Partnership cache"),
    SEASONAL_YES(62,1,"Seasonal Access"),
    SEASONAL_NO(62,0,"Not Seasonal Access"),
    TOURISTOK_YES(63,1,"Tourist Friendly"),
    TOURISTOK_NO(63,0,"Not Tourist Friendly"),
    TREECLIMBING_YES(64,1,"Tree Climbing required"),
    TREECLIMBING_NO(64,0,"Tree Climbing not required"),
    FRONTYARD_YES(65,1,"Front Yard (Private Residence)"),
    FRONTYARD_NO(65,0,"Not In Front Yard (Private Residence)"),
    TEAMWORK_YES(66,1,"Teamwork Required"),
    TEAMWORK_NO(66,0,"No Teamwork Required");
    // CHECKSTYLE:ON:JavaDoc 
	private static final Logger LOGGER = LoggerFactory.getLogger(Attribute.class);
    

    private int inc;
    private int id;
    private String description;

	/**
	 * Constructor
	 * 
	 * @param id
	 *            the id
	 * @param inc
	 *            property (1 if YES attribute, 0 if no attribute)
	 * @param description
	 *            description
	 */
	Attribute(int id, int inc, String description) {
		this.id = id;
		this.inc = inc;
		this.description = description;
	}

	/**
	 * Parse the attribute from a image file name as used on geocaching.com
	 * 
	 * @param type
	 *            the image filename
	 * @return the Attribute or null if not found
	 */
	public static Attribute parse(String type) {
		try {
			String convertedType = type.toUpperCase();
			int start = convertedType.lastIndexOf("/");
			int end = convertedType.lastIndexOf(".");
			if (start < 0) {
				start = 0;
			} else {
				start++;
			}
			if (end < 0) {
				end = convertedType.length();
			}
			convertedType = convertedType.substring(start, end);
			convertedType = convertedType.replaceAll("-", "_");
			Attribute res = Attribute.valueOf(convertedType);
			return res;
		} catch (IllegalArgumentException e) {
			// not found
		}
		LOGGER.error("Don't know attribute \"{}\"", type);
		return null;
	}

	@Override
	public String toString() {
		if (this.description != null) {
			return this.description;
		} else {
			String res = super.toString();
			res = res.replaceAll("_", " ");
			String first = res.substring(0, 1);
			res = res.substring(1);

			return first.toUpperCase() + res.toLowerCase();
		}
	}

	public int getId() {
		return id;
	}

	public int getInc() {
		return inc;
	}

	public void setInc(int inc) {
		this.inc = inc;
	}

	public String getDescription() {
		return this.description;
	}
}
