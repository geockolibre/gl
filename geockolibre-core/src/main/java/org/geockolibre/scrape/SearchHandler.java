package org.geockolibre.scrape;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import org.geockolibre.scrape.SearchErrorHandler.SearchErrorHandlerEnum;
import org.geockolibre.scrape.util.UserIdManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SearchHandler {
	private static final Logger LOGGER = LoggerFactory.getLogger(SearchHandler.class);
	private final Searcher searcher;
	private final Login login;
	private final SearchProgressListener progressListener;
	private final ResultFilter resultFilter;
	private final SearchErrorHandler errorHandler;
	private final UserIdManager idManager;
	private final File outputFile;

	private ObjectOutputStream outputStream;
	private boolean ignoreErrors = false;
	private int found = 0;
	private Integer totalCaches = null;
	private int checked = 0;
	private int maxFind = 0;
	
	
	
	public SearchHandler(Login login, ResultFilter filter, File outputFile, SearchProgressListener listener, 
			SearchErrorHandler errorHandler, UserIdManager userIdManager) {
		this.login = login;
		this.progressListener = listener;
		this.resultFilter = filter;
		this.errorHandler = errorHandler;
		this.outputFile = outputFile;
		this.idManager = userIdManager;
		
		setMaxFind(resultFilter.getMaxFind());
		
		progressListener.start();
		searcher = filter.createSearcher(login, listener);
		searcher.registerSearchCallback(new SearchCallback() {
			@Override
			public void totalNumber(int n) {
				announceTotalNumber(n);
			}
			
			@Override
			public void found(Cache cache) {
				handleCache(cache);
			}
		});
		
		
	}
	
	public void setMaxFind(int maxFind) {
		this.maxFind = maxFind;
	}
	
	public int doSearch() {
		try {
			outputStream = new ObjectOutputStream(new FileOutputStream(outputFile));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		resultFilter.startSearch(searcher);
		
		try {
			this.outputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return found;
		
	}

	protected void handleCache(Cache cache) {
		if (this.progressListener.isCanceled()) {
			searcher.abort();
		} else {
			checked++;
			// calculate percentage, if we can
			if (totalCaches != null) {
				progressListener.updateProgress(checked, totalCaches);
			}
			try {
				// check if it fits pre-download criteria
				if (resultFilter.checkPreDownload(cache)) {
					// if so, download cache details
					progressListener.setScraping(cache.getCacheCode(),cache.getName());
					cache.populate(login, false);
					// check if we fulfil post-download criteria
					if (resultFilter.checkPostDownload(cache)) {
						// if so, save cache to list
						outputStream.writeObject(cache);
						// stop ObjectOutputStream from saving a strong
						// reference to every object it stores
						outputStream.reset();
						// increment counter
						found++;
						// put all id logs in cache
						for (CacheLog log : cache.getLogs()) {
							if (log.getLoggedBy().getId() != null) {
								idManager.setId(log.getLoggedBy(), log.getLoggedBy().getId());
							}
						}
						LOGGER.debug("Found cache {} - {} with {} logs.",cache.getCacheCode(),cache.getName(),cache.getLogs().size());
						if (maxFind > 0 && found >= maxFind) {
							searcher.abort();
						}
					}
					progressListener.setFound(found, maxFind);
				}
			} catch (Exception e) {
				LOGGER.error("Problem with cache " + cache.getCacheCode()+" Ignoring "+ignoreErrors+" errorHandler "+errorHandler, e);
				if (!ignoreErrors && errorHandler !=null) {
					SearchErrorHandlerEnum result = errorHandler.handleCacheSearchError(e, cache);
					switch(result) {
					case STOP:
						searcher.abort();
						break;
					case IGNORE_ALL:
						ignoreErrors = true;
						break;
					case IGNORE:
						// ignore
					}
				}
			}
		}
	}

	protected void announceTotalNumber(int n) {
		totalCaches = n;
		progressListener.setTotalNumber(totalCaches);
	}
	
	public int getChecked() {
		return checked;
	}
	
	public int getFound() {
		return found;
	}
	
	public File getOutputFile() {
		return outputFile;
	}
}
