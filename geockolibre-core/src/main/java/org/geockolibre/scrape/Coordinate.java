package org.geockolibre.scrape;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;

/**
 * Implements a latitude or longitude in the WGS84 datum. Sign indicates
 * east/west and north south, with positive being North-east.
 * 
 */
public class Coordinate implements Serializable {
    private static final long serialVersionUID = -3593731490210324085L;
    private int degree;
    private int minutes;
    private int fractionalMinutes;
    private boolean negative;

    public Coordinate() {
    }

    /**
     * Set the coordinate from the degree min.fracMin format, e.g. N 14 14.345 E
     * 009 10.345
     * 
     * Positive indicates North and East, negative South and West.
     * 
     * @param deg
     *            the degrees.
     * @param min
     *            the whole minutes.
     * @param fracMin
     *            the fractional minutes, in thousands of a minute.
     * 
     */
    public Coordinate(int deg, int min, int fracMin) {
        setDegree(deg);
        setMinutes(min);
        setFractionalMinutes(fracMin);
    }

    public Coordinate(Coordinate other) {
        setDegree(other.getDegree());
        setMinutes(other.getMinutes());
        setFractionalMinutes(other.getFractionalMinutes());
        setNegative(other.isNegative());
    }

    public Coordinate(double degree) {
        if (degree >= 0) {
            int deg = (int) Math.floor(degree);
            double remainder = degree - deg;
            double minutes = remainder * 60;
            int min = (int) Math.floor(minutes);
            int fracMin = (int) Math.floor((minutes - min) * 1000);
            setDegree(deg);
            setMinutes(min);
            setFractionalMinutes(fracMin);
        } else {
            negative = true;
            int deg = (int) Math.ceil(degree);
            double remainder = degree - deg;
            double minutes = -remainder * 60;
            int min = (int) Math.floor(minutes);
            int fracMin = (int) Math.floor((minutes - min) * 1000);
            setDegree(deg);
            setMinutes(min);
            setFractionalMinutes(fracMin);
        }
    }

    public boolean isNegative() {
        return negative;
    }

    public void setNegative(boolean negative) {
        this.negative = negative;
    }

    public String toString() {
        return Math.abs(degree) + "\u00B0 " + getMinuesWithFractions() + "\'";
    }

    private String pad(int number, int desiredSize) {
        String res = Integer.toString(number);
        while (res.length() < desiredSize) {
            res = "0" + res;
        }
        return res;
    }

    public String getMinuesWithFractions() {
        return pad(minutes, 2) + "." + pad(fractionalMinutes, 3);
    }

    /**
     * @return the degree
     */
    public int getDegree() {
        return degree;
    }

    /**
     * @param degree
     *            the degree to set
     */
    public void setDegree(int degree) {
        if (Math.abs(degree) > 180) {
            throw new IllegalArgumentException("Degrees must be in range [-180 to 180]");
        }
        this.degree = degree;
    }

    /**
     * @return the minutes
     */
    public int getMinutes() {
        return minutes;
    }

    /**
     * @param minutes
     *            the minutes to set
     */
    public void setMinutes(int minutes) {
        if (minutes > 59 || minutes < 0) {
            throw new IllegalArgumentException("Minutes must be in range [0 to 59]");
        }
        this.minutes = minutes;
    }

    /**
     * @return the fractionalMinutes
     */
    public int getFractionalMinutes() {
        return fractionalMinutes;
    }

    /**
     * @param fractionalMinutes
     *            the fractionalMinutes to set
     */
    public void setFractionalMinutes(int fractionalMinutes) {
        if (fractionalMinutes > 999 || fractionalMinutes < 0) {
            throw new IllegalArgumentException("Minutes must be in range [0 to 999]");
        }
        this.fractionalMinutes = fractionalMinutes;
    }

    /**
     * Get the coordinate formatted as a positive/negative fractional degree
     * value
     * 
     * @return
     */
    public String toDecimalString() {
        return Double.toString(getDegreeWithFraction());
    }

    public double getDegreeWithFraction() {
        double angle = getDegree();
        if (isNegative()) {
            angle -= getMinutes() / 60.0;
            angle -= getFractionalMinutes() / 60000.0;
        } else {
            angle += getMinutes() / 60.0;
            angle += getFractionalMinutes() / 60000.0;

        }
        return angle;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + degree;
		result = prime * result + fractionalMinutes;
		result = prime * result + minutes;
		result = prime * result + (negative ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Coordinate other = (Coordinate) obj;
		if (degree != other.degree)
			return false;
		if (fractionalMinutes != other.fractionalMinutes)
			return false;
		if (minutes != other.minutes)
			return false;
		if (negative != other.negative)
			return false;
		return true;
	}

    
}
