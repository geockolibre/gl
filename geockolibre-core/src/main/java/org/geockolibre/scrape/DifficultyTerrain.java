package org.geockolibre.scrape;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Tuple holding difficulty and terrain
 * @author sst
 */
public class DifficultyTerrain {
	public enum Grade {
		G_1 (1),
		G_1_5 (1.5),
		G_2 (2),
		G_2_5 (2.5),
		G_3 (3),
		G_3_5 (3.5),
		G_4 (4),
		G_4_5 (4.5),
		G_5 (5),
		;
		final double value;
		final String asString;
		Grade(double value) {
			this.value = value;
			this.asString = String.format("%.1f",value).replace(',', '.');
		}
		Grade(int value) {
			this.value = value;
			this.asString = Integer.toString(value);
		}
		@Override
		public String toString() {
			return asString;
		}
		
		public static Grade parse(String s) {
			for (Grade g:values()) {
				if (g.asString.equals(s)) {
					return g;
				}
			}
			return null;
		}
	}
	
	private final Grade difficulty;
	private final Grade terrain;
	
	public DifficultyTerrain(Grade difficulty, Grade terrain) {
		this.difficulty = difficulty;
		this.terrain = terrain;
	}

	public Grade getDifficulty() {
		return difficulty;
	}
	public Grade getTerrain() {
		return terrain;
	}
	@Override
	public String toString() {
		return difficulty+"/"+terrain; 
	}
	
	public static DifficultyTerrain parse(String diffTerr) {
		if (!diffTerr.matches("^[1-5](\\.5)?/[1-5](\\.5)?$")) {
			throw new IllegalArgumentException("Cannot parse "+diffTerr);
		}
		// REGEX could probably be written more easy, but
		final String[] groups = diffTerr.split("/");
		return new DifficultyTerrain(Grade.parse(groups[0]), Grade.parse(groups[1]));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((difficulty == null) ? 0 : difficulty.hashCode());
		result = prime * result + ((terrain == null) ? 0 : terrain.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DifficultyTerrain other = (DifficultyTerrain) obj;
		if (difficulty != other.difficulty)
			return false;
		if (terrain != other.terrain)
			return false;
		return true;
	}
}
