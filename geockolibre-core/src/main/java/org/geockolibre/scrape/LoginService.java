package org.geockolibre.scrape;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.util.HashMap;

import javax.inject.Inject;
import javax.inject.Named;

import org.geockolibre.scrape.exceptions.AuthenticationFailedException;
import org.geockolibre.scrape.util.HtmlParser;
import org.geockolibre.scrape.util.UserAgentFaker;
import org.geockolibre.scrape.util.concurrent.ProgressListener;
import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;

@Named
public class LoginService {
	private static final Logger LOGGER = LoggerFactory.getLogger(LoginService.class);
    private static final String GSPKAUTH_COOKIE = "gspkauth";
    private static final String SESSION_ID_COOKIE = "ASP.NET_SessionId";
    private static final String LOGIN_STRING = "https://www.geocaching.com/login/default.aspx";
    @Inject
	private MessageSource messages;
    
	public Login authenticate(Login login, ProgressListener pl) {
		try {
			pl.updateTitle(messages.getMessage("loginProgress.title",null,null));
			pl.updateAction(messages.getMessage("loginProgress.title",null,null));
			pl.updateMessage(messages.getMessage("loginProgress.logginIn",null,null));
			pl.updateProgress(0, 100);
			authenticate(login);
			pl.updateProgress(40, 100);
			if (!login.isAuthenticated()) {
				return null;
			}
			pl.updateMessage(messages.getMessage("loginProgress.loadingUserDetails",null,null));
			login.loadUserID();
			pl.updateProgress(60,100);
			login.loadHomeLocation();
			pl.updateProgress(80,100);
			login.getDateFormat();
			pl.updateProgress(100,100);
			return login;
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}
	
	private Login authenticate(Login login) throws IOException {
		LOGGER.debug("Getting login page");
		String randomUserAgent = UserAgentFaker.getRandomUserAgent();
        final Connection detailConnection = Jsoup.connect(LOGIN_STRING);
		detailConnection.userAgent(randomUserAgent);
        Document page = detailConnection.get();

        HashMap<String, String> values = HtmlParser.getViewState(page);
        values.put("__EVENTTARGET", "");
        values.put("__EVENTARGUMENT", "");
        values.put("ctl00$ContentBody$tbUsername", login.getUserName());
        values.put("ctl00$ContentBody$tbPassword", login.getPassword());
        values.put("ctl00$ContentBody$cbRememberMe", "on");
        values.put("ctl00$ContentBody$btnSignIn", "Login");
        detailConnection.data(values);
		LOGGER.debug("Logging in");
        Connection.Response response = Jsoup.connect(LOGIN_STRING)
        		.userAgent(randomUserAgent)
        		.data(values).method(Method.POST).execute();
        String token = response.cookie(SESSION_ID_COOKIE);
        String gspkAuth = response.cookie(GSPKAUTH_COOKIE);
        if (token!=null && gspkAuth!=null) {
        	login.setToken(token);
        	login.setGspkAuth(gspkAuth);
        	login.setAuthenticated(true);
    		LOGGER.debug("Logged in");
        	return login;
        } else {
    		LOGGER.debug("Login failed : \n"+response.body());
        	throw new AuthenticationFailedException(messages.getMessage("login.failed",null,null));
        }
		
	}
}
