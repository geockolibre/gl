package org.geockolibre.scrape;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Enumeration for the cache types
 *
 */
public enum CacheType {
    // No javadoc: Enumeration names are self explaining
    // CHECKSTYLE:OFF:Javadoc FOR 5 LINES
    TRADITIONAL, MULTI, VIRTUAL, LETTERBOX, EVENT, PROJECTAPE1, MYSTERY, 
    PROJECTAPE2, UNUSED, WEBCAM, GCHQ, CITO, WHERIGO, EARTH_CACHE, LOST_AND_FOUND_EVENT, 
    MEGA_EVENT, OWN, FOUND, GPSADVENTURE, BENCHMARK,
    // estimates from map images
    ESTIMATE_TRAD(true), ESTIMATE_MYST(true), ESTIMATE_VIRT(true), ESTIMATE_EVENT(true),
    /**
     * There are multiple overlapping caches at this location, can't determine
     * type.
     */
    MULTIPLE_CACHES_AT_LOCATION(true);

    /**
     * NB: There is no small or medium estimate for multis, found, and owned.
     * 
     * These caches have their own unique icon regardless of zoom level.
     */

    /**
     * True if the cache type is estimated
     */
    private boolean estimated;

    /**
     * Constructor
     */
    CacheType() {
    }

    /**
     * Constructor
     * @param estimated if the cache type is estimated
     */
    CacheType(boolean estimated) {
        this.estimated = estimated;
    }

    public boolean isEstimated() {
        return this.estimated;
    }

    /**
     * Get the description of the cache type
     * @return the description
     */
    // CHECKSTYLE:OFF:JavaCSS FOR 1 LINE
    public String getDescription() {
        String res;
        switch (this) {
        case TRADITIONAL:
            res = "Traditional Cache";
            break;
        case EARTH_CACHE:
            res = "Earthcache";
            break;
        case MULTI:
            res = "Multi-cache";
            break;
        case MYSTERY:
            res = "Unknown Cache";
            break;
        case WEBCAM:
            res = "Webcam Cache";
            break;
        case WHERIGO:
            res = "Wherigo Cache";
            break;
        case VIRTUAL:
            res = "Virtual Cache";
            break;
        case EVENT:
            res = "Event Cache";
            break;
        case PROJECTAPE1:
        case PROJECTAPE2:
            res = "Project Ape Cache";
            break;
        default:
            res = name();
            break;
        }
        return res;
    }
}
