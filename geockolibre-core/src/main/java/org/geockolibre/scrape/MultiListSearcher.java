package org.geockolibre.scrape;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class MultiListSearcher extends Searcher {
	private final ListSearcher listSearcher;
	private int numberOfLocationsDone;
	private int numberOfListsDone;
	private boolean aborted;
	private int lastTotalNumber;
	private final MultiSearchProgressListener progressListener;

    public MultiListSearcher(Login login, MultiSearchProgressListener spl) {
        super(login);
        this.listSearcher = new ListSearcher(login);
        this.progressListener = spl;
        listSearcher.registerSearchCallback(new SearchCallback() {
			@Override
			public void totalNumber(int n) {
				lastTotalNumber += n;
				announceCount(lastTotalNumber);
			}
			
			@Override
			public void found(Cache cache) {
				announceCache(cache);
			}
		});
    }
    /**
     * Stop the search. The findCaches.... method will return.
     */
    public void abort() {
    	this.aborted = true;
    	listSearcher.abort();
    }

    /**
     * Find all caches within a given distance of the given location.
     * 
     * @param loc
     *            the location to search at.
     * @param maxDistance
     *            the maximum distance, in kilometres.
     * @param ignoreFoundAndOwn
     *            if true, caches the user has already found won't be listed.
     * @return
     * @throws IOException
     * @throws ParseException
     */
    public List<Cache> findCachesCloseTo(List<List<Location>> locs, double maxDistance, boolean ignoreFoundAndOwn)
            throws IOException, ParseException {
        return findCachesCloseTo(locs, maxDistance, ignoreFoundAndOwn, 0, true);
    }

    /**
     * Find all caches within a given distance of the given location.
     * 
     * @param loc
     *            the location to search at.
     * @param maxDistance
     *            the maximum distance, in kilometres.
     * @param ignoreFoundAndOwn
     *            if true, caches the user has already found won't be listed.
     * @param maxCaches
     *            the maximum number of caches to return.
     * @param doLocalCopy
     *            if true, fill in the return value, otherwise return an empty
     *            list and only use callbacks.
     * @return
     * @throws IOException
     * @throws ParseException
     */
    public List<Cache> findCachesCloseTo(List<List<Location>> locs, double maxDistance, boolean ignoreFoundAndOwn, int maxCaches,
            boolean doLocalCopy) throws IOException, ParseException {
    	List<Cache> res = new ArrayList<>();
    	numberOfListsDone = 0;
    	for (List<Location> list:locs) {
    		numberOfLocationsDone = 0;
	    	for (Location loc:list) {
	    		progressListener.updateStep(numberOfListsDone, numberOfLocationsDone, locs);
	    		int tryCount = 0;
	    		retryloop : while (tryCount++<4) {
		    		try {
		    			res.addAll(listSearcher.findCachesCloseTo(loc, maxDistance, ignoreFoundAndOwn, maxCaches, doLocalCopy));
		    			break retryloop;
		    		} catch (SocketTimeoutException e) {
		    			if (tryCount==3) {
		    				throw e;
		    			}
		    		}
	    		}
	    		numberOfLocationsDone++;
	    		if (aborted || progressListener.isCanceled()) {
	    			break;
	    		}
	    	}
	    	numberOfListsDone++;
	    	if (aborted || progressListener.isCanceled()) {
	    		break;
	    	}
    	}
        return res;
    }

    /**
     * Find all caches within a given distance of the given location.
     * 
     * @param loc
     * @param maxDistance
     *            the maximum distance, in kilometres.
     * @return
     * @throws IOException
     * @throws ParseException
     */
    public List<Cache> findCachesCloseTo(List<List<Location>> locs, double maxDistance) throws IOException, ParseException {
        return findCachesCloseTo(locs, maxDistance, false);
    }
	
}
