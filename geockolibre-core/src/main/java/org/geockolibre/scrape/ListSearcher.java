package org.geockolibre.scrape;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import org.geockolibre.scrape.util.HtmlParser;
import org.geockolibre.scrape.util.StringUtil;
import org.geockolibre.scrape.util.Tools;
import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.HttpStatusException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class that searches for caches by location, owner, or finder in lists
 * returned by the website.
 * 
 */
public class ListSearcher extends Searcher {
	private static final Logger LOGGER = LoggerFactory.getLogger(ListSearcher.class);
    private boolean aborted;

    public ListSearcher(Login login) {
        super(login);
    }

    /**
     * Find all caches within a given distance of the given location.
     * 
     * @param loc
     *            the location to search at.
     * @param maxDistance
     *            the maximum distance, in kilometres.
     * @param ignoreFoundAndOwn
     *            if true, caches the user has already found won't be listed.
     * @return
     * @throws IOException
     * @throws ParseException
     */
    public List<Cache> findCachesCloseTo(Location loc, double maxDistance, boolean ignoreFoundAndOwn)
            throws IOException, ParseException {
        return findCachesCloseTo(loc, maxDistance, ignoreFoundAndOwn, 0, true);
    }

    /**
     * Find all caches within a given distance of the given location.
     * 
     * @param loc
     *            the location to search at.
     * @param maxDistance
     *            the maximum distance, in kilometres.
     * @param ignoreFoundAndOwn
     *            if true, caches the user has already found won't be listed.
     * @param maxCaches
     *            the maximum number of caches to return.
     * @param doLocalCopy
     *            if true, fill in the return value, otherwise return an empty
     *            list and only use callbacks.
     * @return
     * @throws IOException
     * @throws ParseException
     */
    public List<Cache> findCachesCloseTo(Location loc, double maxDistance, boolean ignoreFoundAndOwn, int maxCaches,
            boolean doLocalCopy) throws IOException, ParseException {
        List<Cache> res = new ArrayList<Cache>();
        double mileMaxDistance = maxDistance / 1.609344;
        String queryString = "https://www.geocaching.com/seek/nearest.aspx?";
        queryString += "origin_lat=" + loc.getLatitude().getDegreeWithFraction();
        queryString += "&origin_long=" + loc.getLongitude().getDegreeWithFraction();
        queryString += "&dist=" + mileMaxDistance;
        if (ignoreFoundAndOwn) {
            queryString += "&f=1";
        }
        doSearch(res, queryString, maxCaches, doLocalCopy);
        return res;
    }

    /**
     * Find all caches within a given distance of the given location.
     * 
     * @param loc
     * @param maxDistance
     *            the maximum distance, in kilometres.
     * @return
     * @throws IOException
     * @throws ParseException
     */
    public List<Cache> findCachesCloseTo(Location loc, double maxDistance) throws IOException, ParseException {
        return findCachesCloseTo(loc, maxDistance, false);
    }

    private void doSearch(List<Cache> res, String queryString, int maxCaches, boolean doLocalSave)
            throws IOException, ParseException {

        doRandomDelay();
        Connection connection = login.getConnection(queryString);
        Document document = connection.get();
        int foundSoFar = 0;

        Element resultPanel = document.getElementById("ctl00_ContentBody_ResultsPanel");
        // check if the user has ever found any caches
        if (resultPanel != null) {
            Element recordCountElement = document.select("td.PageBuilderWidget b:first-of-type").first();
            if (recordCountElement != null) {
                int totalRecords = Integer.parseInt(recordCountElement.text());
                announceCount(totalRecords);
            }
            Element pageNrELement = recordCountElement.nextElementSibling();
            Element pageCountELement = pageNrELement.nextElementSibling();
            int pageNr = Integer.parseInt(pageNrELement.text());
            int pageCount = Integer.parseInt(pageCountELement.text());
            int totalPages = Math.max(pageNr, pageCount);

            foundSoFar = parseSearchResultPage(resultPanel, res, maxCaches, doLocalSave, foundSoFar);
            if (aborted) {
                return;
            }
            if (maxCaches != 0 && foundSoFar >= maxCaches) {
                while (res.size() > maxCaches) {
                    res.remove(res.size() - 1);
                }
                return;
            }

            for (int x = 2; x <= totalPages; x++) {
                HashMap<String, String> inputs = HtmlParser.getViewState(document);
                inputs.put("__EVENTTARGET", "ctl00$ContentBody$pgrTop$ctl08");
                doRandomDelay();
                connection = login.getConnection(queryString);
                connection.data(inputs);
                try {
                    connection.method(Method.POST);
                    Response r = connection.execute();
                    if (r.statusCode() != 200) {
                    	LOGGER.error("Posting for page " + x + " on request " + queryString + " returned "
                                + r.statusCode() + " - " + r.statusMessage());
                    	LOGGER.error("Inputs were : " + inputs);
                    	LOGGER.error("Last doc was : " + document);
                        throw new IllegalStateException("Error status code "+r.statusCode());
                    }
                    document = r.parse();
                } catch (HttpStatusException e) {
                    LOGGER.error("Error posting for page " + x);
                    LOGGER.error("Last doc was : " + document);
                    LOGGER.error("Inputs were : " + inputs);
                    throw e;
                }
                foundSoFar = parseSearchResultPage(document, res, maxCaches, doLocalSave, foundSoFar);
                if (aborted) {
                    return;
                }
                if (maxCaches != 0 && foundSoFar >= maxCaches) {
                    while (res.size() > maxCaches) {
                        res.remove(res.size() - 1);
                    }
                    return;
                }
            }
        }
    }

    /**
     * Get the caches described in a search result page.
     * 
     * @param content
     * @param res
     *            list of found caches, if doLocalSave is true - otherwise
     *            empty.
     * @param doLocalSave
     *            if true, save found cacehs to res.
     * @param foundSoFar
     *            the number of caches found so far.
     * 
     * @throws IOException
     * @throws ParseException
     * @return the number of found caches
     */
    private int parseSearchResultPage(Element resultPanel, List<Cache> res, int maxCaches, boolean doLocalSave,
            int foundSoFar) throws IOException, ParseException {
        int foundCount = foundSoFar;
        for (Element row : resultPanel.select("table.SearchResultsTable tbody tr")) {
            if (!row.select("th").isEmpty()) {
                continue;
            }
            Elements cells = row.select("td");
            String favouriteContent = cells.get(2).select("span").text().replaceAll("[,\\.]", "");
            Cache c = new Cache();
            c.setFavourited(Integer.parseInt(favouriteContent));

            Element el = cells.get(3).select("img[id*=uxFoundStatusIcon]").first();
            c.setFoundByUser(el != null && !el.attr("src").contains("placed"));

            Element typeImage = cells.get(4).select("img").first();
            if (typeImage != null) {
                c.setCacheType(StringUtil.getFirstGroup(typeImage.attr("src"), "/([^\\./]*)\\."));
            }

            Element mainCell = cells.get(5);
            if (!mainCell.select(".lnk.Strike").isEmpty()) {
                c.setDisabled(true);
            }
            c.setShortCutURL(mainCell.select("a").first().attr("href"));
            c.setName(mainCell.select("a span").first().text());

            Element details = mainCell.select("> span").first();
            String[] detail = details.text().split("\\|");
            c.setOwner(new Cacher(detail[0].trim().replaceFirst("(?i)by\\s+", "")));
            c.setCacheCode(detail[1].trim());
            c.setLocationDescription(detail[2].trim());

            Element attributeCell = cells.get(6);
            // check if the "needs maintenance" attribute is set

            if (!attributeCell.select("img[src*=maintenance.png]").isEmpty()) {
                c.getAttributes().add(Attribute.FIRSTAID_YES);
            }

            // check if this cache has trackables
            Element tbLink = attributeCell.select("[id*=_uxTravelBugList]").first();
            if (tbLink != null && tbLink.hasAttr("data-tbcount")) {
                c.setTrackableCount(Integer.parseInt(tbLink.attr("data-tbcount")));
            }

            if (!attributeCell.select("img[src*=premium_only.png]").isEmpty()) {
                c.setPremiumonly(true);
            }
            // get the size, difficulty, and terrain
            Element diffTerrEl = cells.get(7).select("span.small").first();
            if (diffTerrEl != null) {
                String[] diffTerr = StringUtil.getGroups(diffTerrEl.text(), "^([0-9\\.]+)/([0-9\\.]+)$");
                if (diffTerr.length == 2) {
                    c.setDifficultyRating(Double.parseDouble(diffTerr[0]));
                    c.setTerrainRating(Double.parseDouble(diffTerr[1]));
                }
            }
            Element sizeEl = cells.get(7).select("img").first();
            if (sizeEl != null) {
                String sizeContents = StringUtil.getFirstGroup(sizeEl.attr("src"), "/([[^\\./]*]*).gif");
                c.setCacheSize(CacheSize.valueOf(sizeContents.toUpperCase()));
            }

            c.setPlacedAt(getParsedFancyTime(cells.get(8).text(), login));
            c.setLastFoundDate(getParsedFancyTime(cells.get(9).text(), login));
            announceCache(c);
            if (doLocalSave) {
                res.add(c);
            }
            foundCount++;
            if (maxCaches > 0 && foundCount >= maxCaches) {
                break;
            }
            if (aborted) {
                break;
            }
        }
        return foundCount;
    }

    /**
     * Parse the hidden/found dates from the list view.
     * 
     * @param date
     * @param df
     * @return
     */
    private Long getParsedFancyTime(String date, Login login) {
        date = date.replace("*", "");
        Long res = null;
        try {
            Date d = login.parseDate(date);
            res = d.getTime();
        } catch (ParseException e) {
            if (Tools.isToday(date)) {
                res = new Date().getTime();
            } else if (Tools.isYesterday(date)) {
                Calendar c = new GregorianCalendar();
                c.add(Calendar.DATE, -1);
                res = c.getTimeInMillis();
            } else {
                // try to remove anything non-digit from date, and parse that
                String tmp = "";
                for (int x = 0; x < date.length(); x++) {
                    if (Character.isDigit(date.charAt(x))) {
                        tmp += date.charAt(x);
                    }
                }

                try {
                    int daysAgo = Integer.parseInt(tmp);
                    Calendar c = new GregorianCalendar();
                    c.add(Calendar.DATE, -daysAgo);
                    res = c.getTimeInMillis();
                } catch (NumberFormatException e2) {
                }

                if (res == null && date != null && date.length() > 0) {
                    LOGGER.error("Don't know how to parse date \"" + date + "\"");
                }
            }
        }
        return res;
    }

    /**
     * Find all caches owned by a certain cacher.
     * 
     * @param owner
     *            the owner of the caches.
     * @return an array list of caches.
     * @throws IOException
     * @throws ParseException
     */
    public List<Cache> findCachesOwnedBy(Cacher owner, int maxCaches) throws IOException, ParseException {
        return findCachesOwnedBy(owner, maxCaches, true);
    }

    /**
     * Find all caches owned by a certain cacher.
     * 
     * @param owner
     *            the owner of the caches.
     * @param doLocalCopy
     *            if true, the return value will contain a list of found caches.
     * @return an array list of caches.
     * @throws IOException
     * @throws ParseException
     */
    public List<Cache> findCachesOwnedBy(Cacher owner, int maxCaches, boolean doLocalCopy)
            throws IOException, ParseException {
        ArrayList<Cache> res = new ArrayList<Cache>();
        String queryString = "https://www.geocaching.com/seek/nearest.aspx?u=";
        String queryEnd = "&submit4=Go";
        String name = URLEncoder.encode(owner.getName(), "UTF-8");
        String query = queryString + name + queryEnd;
        doSearch(res, query, maxCaches, doLocalCopy);
        return res;
    }

    /**
     * Find all caches found by a certain cacher.
     * 
     * @param finder
     *            the finder of the caches.
     * @return an array list of caches.
     * @throws IOException
     * @throws ParseException
     */
    public List<Cache> findCachesFoundBy(Cacher finder) throws IOException, ParseException {
        return findCachesFoundBy(finder, 0);
    }

    /**
     * Find latest maxCaches caches found by a certain cacher.
     * 
     * @param finder
     *            the finder of the caches.
     * @param maxCaches
     *            the maximum number of caches to return, return all caches if
     *            maxCaches = 0.
     * @return an array list of caches.
     * @throws IOException
     * @throws ParseException
     */
    public List<Cache> findCachesFoundBy(Cacher finder, int maxCaches) throws IOException, ParseException {
        return findCachesFoundBy(finder, maxCaches, true);
    }

    /**
     * Find latest maxCaches caches found by a certain cacher.
     * 
     * @param finder
     *            the finder of the caches.
     * @param maxCaches
     *            the maximum number of caches to return, return all caches if
     *            maxCaches = 0.
     * @param doLocalCopy
     *            if true, the return value will contain the list of found
     *            caches.
     * @return an array list of caches.
     * @throws IOException
     * @throws ParseException
     */
    public List<Cache> findCachesFoundBy(Cacher finder, int maxCaches, boolean doLocalCopy)
            throws IOException, ParseException {
        ArrayList<Cache> res = new ArrayList<Cache>();
        String queryString = "https://www.geocaching.com/seek/nearest.aspx?ul=";
        String queryEnd = "&submit4=Go";
        String name = finder.getName();
        name = URLEncoder.encode(name, "UTF-8");
        String query = queryString + name + queryEnd;
        doSearch(res, query, maxCaches, doLocalCopy);
        return res;
    }

    /**
     * Stop the search. The findCaches.... method will return.
     */
    public void abort() {
        this.aborted = true;
    }
}
