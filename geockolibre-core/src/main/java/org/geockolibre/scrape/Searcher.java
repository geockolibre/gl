package org.geockolibre.scrape;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Random;

import org.geockolibre.scrape.util.Tools;

/**
 * General tool for searching.
 *
 */
public abstract class Searcher {

    private ArrayList<SearchCallback> callbacks = new ArrayList<SearchCallback>();
    protected Login login;
    private int minSeconds;
    private int maxSeconds;
    private boolean useRandomdelay;
    private Random rand;

    public Searcher(Login login) {
        this.login = login;
    }
    
    public abstract void abort();

    /**
     * Register a class that will receive a callback for all results as they
     * come in.
     * 
     * @param callback
     */
    public void registerSearchCallback(SearchCallback callback) {
        this.callbacks.add(callback);

    }

    public void unregisterSearchCallback(SearchCallback callback) {
        this.callbacks.remove(callback);
    }

    /**
     * Announce that a new cache has been found to all callback listeners.
     * 
     * @param c
     */
    protected void announceCache(Cache c) {
        for (SearchCallback sc : callbacks) {
            sc.found(c);
        }
    }

    protected void announceCount(int count) {
        for (SearchCallback sc : callbacks) {
            sc.totalNumber(count);
        }
    }

    public void doRandomDelay() {
        if (isUseRandomDelay()) {
            int range = maxSeconds - minSeconds;
            long sleep = rand.nextInt(range) + minSeconds;
            Tools.sleep(sleep);
        }
    }

    public void setMinSeconds(int minSeconds) {
        this.minSeconds = minSeconds;
    }

    public void setMaxSeconds(int maxSeconds) {
        this.maxSeconds = maxSeconds;
    }

    public void setUseRandomDelay(boolean b) {
        if (b && this.rand == null) {
            this.rand = new Random();
        }
        useRandomdelay = b;
    }

    public boolean isUseRandomDelay() {
        return this.useRandomdelay;
    }
}
