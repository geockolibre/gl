package org.geockolibre.scrape;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.geockolibre.scrape.util.concurrent.ProgressListener;

public interface ResultFilter {
	default Searcher createSearcher(Login login, SearchProgressListener spl) {return new ListSearcher(login);}
	/**
	 * Return the maximum number of caches to find
	 * @return the maximum number of caches to find
	 */
	int getMaxFind();
	/**
	 * Set the progress listener. This allows us the filter to cancel the search if needed
	 * @param pl the ProgressListener
	 */
	default void setProgressListener(ProgressListener pl) {};
	/**
	 * Init search
	 * @param cache the cache to check
	 * @return if the cache matches the filter and can be downloaded
	 */
	void startSearch(Searcher listSearcher);
	
	/**
	 * Check if a cache matches the filter from the list details
	 * @param cache the cache to check
	 * @return if the cache matches the filter and can be downloaded
	 */
	boolean checkPreDownload(Cache cache);
	
	/**
	 * Check if a cache matches the filter from the cache details
	 * @param cache the cache to check
	 * @return if the cache matches the filter and should be added to the result list
	 */
	boolean checkPostDownload(Cache cache);
}
