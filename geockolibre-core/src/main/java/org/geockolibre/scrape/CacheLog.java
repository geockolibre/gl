package org.geockolibre.scrape;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

import org.geockolibre.scrape.util.HtmlParser;
import org.geockolibre.util.json.JSONException;
import org.geockolibre.util.json.JSONObject;

/**
 * Defines a log for a cache.
 */
public class CacheLog implements Serializable {
    private static final long serialVersionUID = 3665815642473663594L;
    private long logTime;// logged time in UTC
    private Cacher loggedBy;
    private String text;
    private LogType logType;
    private Long id;

    /**
     * Default Constructor.
     * 
     * @param data the json object
     * @param login the login to connect to geocaching.com 
     * @throws JSONException in case the json can not be read
     * @throws ParseException in case the data is not correctly formatted
     */
    public CacheLog(JSONObject data, Login login) throws JSONException, ParseException {
        logType = LogType.parse(data.getString("LogType"));
        TimeZone laTimeZone = TimeZone.getTimeZone("America/Los_Angeles");
        SimpleDateFormat sdf = new SimpleDateFormat(login.getDateFormat(), Locale.ENGLISH);
        sdf.setTimeZone(laTimeZone);
        logTime = sdf.parse(data.getString("Visited")).getTime();
        loggedBy = new Cacher(HtmlParser.convertHtmlChar(data.getString("UserName")),
                "http://www.geocaching.com/profile/?guid=" + data.getString("AccountGuid"));
        loggedBy.setFoundCaches(data.getLong("GeocacheFindCount"));
        loggedBy.setId(data.getLong("AccountID"));

        setId(data.getLong("LogID"));
        // get the log text
        text = HtmlParser.convertHtmlChar(data.getString("LogText")).trim();
    }

    public long getLogTime() {
        return logTime;
    }

    public void setLogTime(long logTime) {
        this.logTime = logTime;
    }

    public Cacher getLoggedBy() {
        return loggedBy;
    }

    public void setLoggedBy(Cacher loggedBy) {
        this.loggedBy = loggedBy;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LogType getLogType() {
        return logType;
    }

    public void setLogType(LogType logType) {
        this.logType = logType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
