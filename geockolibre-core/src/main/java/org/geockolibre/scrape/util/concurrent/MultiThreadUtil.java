package org.geockolibre.scrape.util.concurrent;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import java.util.function.Function;

import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.geockolibre.scrape.util.concurrent.Execution.ExecutionState;

import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;


/**
 * Utility methods for running things asynchronously
 * @author sst
 */
public final class MultiThreadUtil {
    private static final int THREAD_TIMEOUT = 10;
    private static final String THREADNAME_PREFIX = "COT MTU ";
    private static final ListeningExecutorService EXECUTOR = MoreExecutors.listeningDecorator(
            Executors.newCachedThreadPool(
                new BasicThreadFactory.Builder()
                    .namingPattern(THREADNAME_PREFIX+"EXECTHREAD %d")
                    .uncaughtExceptionHandler(MultiThreadUtil::handleUncaughtException)
                    .build()));
    private static final ScheduledThreadPoolExecutor SCHEDULE_EXECUTOR = new ScheduledThreadPoolExecutor(2,
            new BasicThreadFactory.Builder()
                .namingPattern(THREADNAME_PREFIX+"SCHEDTHREAD %d")
                .uncaughtExceptionHandler(MultiThreadUtil::handleUncaughtException)
                .build());
    private static final AtomicLong EXECUTOR_COUNT=new AtomicLong();
    private static final AtomicLong SCHEDULER_COUNT=new AtomicLong();
    
    private static Thread.UncaughtExceptionHandler exceptionHandler; 
    /**
     * Private constructor to avoid instantiation
     */
    private MultiThreadUtil() {}

    /**
     * Handle an uncaught exception
     * @param t The thread where the exception occured
     * @param e The exception 
     */
    private static void handleUncaughtException(Thread t, Throwable e) {
        if (exceptionHandler==null) {
            System.err.println("Background job failed with uncaught exception: ");
            e.printStackTrace();
        } else {
            exceptionHandler.uncaughtException(t, e);
        }
    }
    
    public static void setUncaughtExceptionHandler(Thread.UncaughtExceptionHandler exceptionHandler) {
        MultiThreadUtil.exceptionHandler = exceptionHandler;
    }
    /**
     * Create and handle an execution that is already finished
     * @param result the result of the execution
     * @param callback the callback to call
     * @param <T> the type of the result value
     * @return the Execution
     */
    public static <T> Execution<T> executeImmediately(final T result, final Consumer<Execution<T>> callback) {
        final Execution<T> execution = new Execution<T>(result, callback);
        final Consumer<Execution<T>> realCallback = execution.getCallback();
        if (realCallback!=null) {
            realCallback.accept(execution);
        }
        return execution;
    }

    /**
     * Execute the given job in the background
     * @param task the task to execute
     * @param <T> the type of the result value
     * @return the execution
     */
    public static <T> Execution<T> executeInBackground(final Callable<T> task) {
        return executeWithCallback(task, e -> {
            if (e.getState()==ExecutionState.FAILED) {
                System.err.println("Background job failed with uncaught exception: ");
                e.getError().printStackTrace();
                Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), e.getError());
            }
        });
    }
    
    /**
     * Execute the given job in the background
     * @param task the task to execute
     * @return the execution
     */
    public static Execution<Void> executeInBackground(final Runnable task) {
        return executeInBackground(()->{task.run();return null;});
    }
    
    /**
     * Creates execution to run a task in the background and executes the task
     * @param task the task to execute
     * @param callback the callback to call
     * @return the Execution
     */
    public static Execution<Void> executeWithCallback(final Runnable task, final Runnable callback) {
        return executeWithCallback(
            ()->{task.run();return null;},
            (e)->callback.run());
    }

    /**
     * Creates execution to run a task in the background and executes the task
     * @param task the task to execute
     * @param callback the callback to call
     * @param <T> the type of the result value
     * @return the Execution
     */
    public static <T> Execution<T> executeWithCallback(final Callable<? extends T> task, final Consumer<Execution<T>> callback) {
        final Execution<T>execution = new Execution<T>(callback);
        execution.getAtomicState().set(ExecutionState.WAITING.ordinal());
        EXECUTOR_COUNT.incrementAndGet();
        EXECUTOR.execute(() -> {
            try {
                if (execution.getAtomicState().compareAndSet(ExecutionState.WAITING.ordinal(), ExecutionState.RUNNING.ordinal())) {
                    execution.setResult(task.call());
                }
                if (execution.getAtomicState().compareAndSet(ExecutionState.RUNNING.ordinal(), ExecutionState.FINISHED.ordinal())) {
                    final Consumer<Execution<T>> realCallback = execution.getCallback();
                    if (realCallback!=null) {
                        realCallback.accept(execution);
                    }
                }
            } catch (Throwable t) {
                execution.setError(t);
                execution.getAtomicState().set(ExecutionState.FAILED.ordinal());
                if (Thread.interrupted()) {
                    return;
                }
                final Consumer<Execution<T>> realCallback = execution.getCallback();
                if (realCallback!=null) {
                    callback.accept(execution);
                }
            } finally {
                EXECUTOR_COUNT.decrementAndGet();
            }
        });
        return execution;
    }
    
    /**
     * Creates execution to run a task in the background and executes the task
     * @param task the task to execute
     * @param callback the callback to call
     * @param <T> the type of the result value
     * @return the Execution
     */
    public static <T> Execution<T> executeWithCallback(final Function<ProgressListener, ? extends T> task, final Consumer<Execution<T>> callback, final ProgressListener l) {
    	final Execution<T>execution = new Execution<T>(callback);
    	execution.getAtomicState().set(ExecutionState.WAITING.ordinal());
    	l.updateState(ExecutionState.WAITING);
    	EXECUTOR_COUNT.incrementAndGet();
    	EXECUTOR.execute(() -> {
    		try {
    			if (l.isCanceled()) {
    				execution.getAtomicState().set(ExecutionState.CANCELLED.ordinal());
    		    	l.updateState(ExecutionState.CANCELLED);
    			} else if (execution.getAtomicState().compareAndSet(ExecutionState.WAITING.ordinal(), ExecutionState.RUNNING.ordinal())) {
    		    	l.updateState(ExecutionState.RUNNING);
    				execution.setResult(task.apply(l));
    			}
    			if (l.isCanceled()) {
    				execution.getAtomicState().set(ExecutionState.CANCELLED.ordinal());
    		    	l.updateState(ExecutionState.CANCELLED);
    			} else if (execution.getAtomicState().compareAndSet(ExecutionState.RUNNING.ordinal(), ExecutionState.FINISHED.ordinal())) {
    		    	l.updateState(ExecutionState.FINISHED);
    		    	l.updateState(ExecutionState.FINISHED);
    			}
    			final Consumer<Execution<T>> realCallback = execution.getCallback();
    			if (realCallback!=null) {
    				realCallback.accept(execution);
    			}
    		} catch (Throwable t) {
    			execution.setError(t);
    			execution.getAtomicState().set(ExecutionState.FAILED.ordinal());
		    	l.updateState(ExecutionState.FAILED);
    			final Consumer<Execution<T>> realCallback = execution.getCallback();
    			if (realCallback!=null) {
    				callback.accept(execution);
    			}
    		} finally {
    			EXECUTOR_COUNT.decrementAndGet();
    		}
    	});
    	return execution;
    }

    /**
     * Schedule to run a given runnable after a given timeout (in ms)
     * @param r the runnable
     * @param timeout the timeout
     * @return the ScheduledFuture
     */
    public static ScheduledFuture<?> schedule (Runnable r, long timeout) {
        return SCHEDULE_EXECUTOR.schedule(
                countRunnable(r), 
                timeout, TimeUnit.MILLISECONDS);
    }

    /**
     * Stops all pending executions
     */
    public static void stop() {
        EXECUTOR.shutdown();
        SCHEDULE_EXECUTOR.shutdown();
        try {
            // CHECKSTYLE:OFF:MagicNumber
            EXECUTOR.awaitTermination(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            EXECUTOR.shutdownNow();
            e.printStackTrace();
        }
        try {
            // CHECKSTYLE:OFF:MagicNumber
            SCHEDULE_EXECUTOR.awaitTermination(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            SCHEDULE_EXECUTOR.shutdownNow();
            e.printStackTrace();
        }
    }
    
    /**
     * Wraps a counter around the Runnable
     * @param originalRunnable the original Runnable to wrap
     * @return the wrapped Counter
     */
    private static Runnable countRunnable(Runnable originalRunnable) {
        EXECUTOR_COUNT.incrementAndGet();
        return ()-> {
            try {
                originalRunnable.run();
            } finally {
                EXECUTOR_COUNT.decrementAndGet();
            }
        };
    }
    
    /**
     * Check if there are running threads. Only meant during testing
     * @return if there are running threads 
     */
    public static boolean hasRunningJobs() {
        return EXECUTOR_COUNT.get()+SCHEDULER_COUNT.get()>0;
    }
    
    /**
     * Wait until all MTU threads have finished
     */
    public static void waitUntilCompleted() {
        while(hasRunningJobs()) {
            try {
                Thread.sleep(THREAD_TIMEOUT);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
