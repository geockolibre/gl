package org.geockolibre.scrape.util;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * 
 */
public class HtmlParser {
    /**
     * Get the "viewstate" parameters from the page source.
     * 
     * @param content
     *            the page source.
     * @return
     */
    public static HashMap<String, String> getViewState(Document content) {
        HashMap<String, String> res = new HashMap<String, String>();

        // get the first viewstate field
        Element viewState = content.getElementById("__VIEWSTATE");
        if (viewState != null) {
            res.put("__VIEWSTATE", viewState.val());
        }
        // get the first viewstate field
        Element viewStateGenerator = content.getElementById("__VIEWSTATEGENERATOR");
        if (viewStateGenerator != null) {
            res.put("__VIEWSTATEGENERATOR", viewStateGenerator.val());
        }
        // get the number of viewstate fields
        Element viewStateCount = content.getElementById("__VIEWSTATEFIELDCOUNT");
        if (viewStateCount != null) {
            res.put("__VIEWSTATEFIELDCOUNT", viewStateCount.val());
            int stateCount = Integer.parseInt(viewStateCount.val());
            for (int x = 1; x <= stateCount; x++) {
                Element viewStateElement = content.getElementById("__VIEWSTATE" + x);
                if (viewStateElement != null) {
                    res.put("__VIEWSTATE" + x, viewStateElement.val());
                }
            }
        }

        return res;
    }

    /**
     * Convert all html character codewords to corresponding characters. Also
     * remove unprintable characters
     * 
     * @param input
     * @return
     */
    public static String convertHtmlChar(String input) {
        HashMap<String, String> replace = new HashMap<String, String>();
        replace.put("&quot;", "\"");
        replace.put("&amp;", "&");
        replace.put("\u0007", "\u2407");// bell
        replace.put("\u0008", "");// backspace
        replace.put("\u001B", "\u241B");// esc
        replace.put("\u007F", "");// del
        Pattern p = Pattern.compile("&#.[a-fA-F0-9]+;");
        Matcher m = p.matcher(input);
        while (m.find()) {
            String match = input.substring(m.start(), m.end());
            if (replace.get(match) == null) {
                int number = 0;
                if (match.toLowerCase().startsWith("&#x")) {
                    number = Integer.parseInt(match.substring(3, match.length() - 1), 16);
                } else {
                    number = Integer.parseInt(match.substring(2, match.length() - 1));
                }
                String replacement = "" + (char) number;
                replace.put(match, replacement);
            }
        }
        for (String key : replace.keySet()) {
            String replacement = replace.get(key);
            input = input.replaceAll(key, replacement);
        }
        return input;
    }
}
