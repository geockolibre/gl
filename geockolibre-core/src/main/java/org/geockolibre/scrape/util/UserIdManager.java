package org.geockolibre.scrape.util;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

import org.geockolibre.scrape.Cacher;
import org.geockolibre.scrape.Login;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class keeps track of the userids of named cachers. Persistent storage in
 * the user's homedir.
 */
public class UserIdManager {
	private static final Logger LOGGER = LoggerFactory.getLogger(UserIdManager.class);

	private static final String ID_DATABASE_FILENAME = System.getProperty("user.home") + File.separator
			+ ".geockolibre/id.db";
	private HashMap<String, Long> userIds;
	private HashMap<String, Long> userNames;
	private Login login;

	public UserIdManager(Login login) {
		this.login = login;
	}

	public Long getId(Cacher cacher) throws IOException {
		Long res = null;
		// check cache for id
		if (cacher.getProfilePage() != null) {
			res = userIds.get(cacher.getProfilePage());
		}
		if (res == null) {
			if (cacher.getName() != null) {
				res = userNames.get(cacher.getName());
			}
			if (res == null) {
				// find the id
				LOGGER.debug("Getting id for user {}", cacher.getName());
				cacher.populate(login);
				// store it
				res = cacher.getId();
				setId(cacher, res);
			}
		}
		return res;
	}

	@SuppressWarnings("unchecked")
	public void loadDb()  {
		try {
			File f = new File(ID_DATABASE_FILENAME);
			if (f.exists()) {
				ObjectInputStream ois = new ObjectInputStream(new FileInputStream(f));
				userIds = (HashMap<String, Long>) ois.readObject();
				userNames = (HashMap<String, Long>) ois.readObject();
				ois.close();
			}
		} catch (IOException|ClassNotFoundException e) {
			// fail silently
		} finally {
			// don't leave this method without creating a user id database
			if (userIds == null) {
				userIds = new HashMap<String, Long>();
			}
			if (userNames == null) {
				userNames = new HashMap<String, Long>();
			}
		}
	}

	public void saveDb() {
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(ID_DATABASE_FILENAME, false));
			oos.writeObject(userIds);
			oos.writeObject(userNames);
			oos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param c
	 * @param id
	 */
	public void setId(Cacher c, Long id) {
		if (c.getProfilePage() != null) {
			userIds.put(c.getProfilePage(), id);
		}
		if (c.getName() != null) {
			userNames.put(c.getName(), id);
		}
	}
}
