package org.geockolibre.scrape.util;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

public class TopMatchEntry extends MatchEntry {
    private static final long serialVersionUID = -4863355031737789939L;
    private double terrain;
    private double difficulty;

    public TopMatchEntry(int[] match, double difficulty, double terrain) {
        super(match);
        this.difficulty = difficulty;
        this.terrain = terrain;
    }

    /**
     * @return the terrain
     */
    public double getTerrain() {
        return terrain;
    }

    /**
     * @return the difficulty
     */
    public double getDifficulty() {
        return difficulty;
    }
}