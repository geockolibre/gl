package org.geockolibre.scrape.util;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.text.Normalizer;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utility class for working with strings
 * 
 * @author sst
 */
public final class StringUtil {
    private static final String WORD_BREAK = "\\b";
    private static int NEWLINE_CODEPOINT  =  Character.codePointAt("\n", 0);

    /**
     * Private constructor to avoid instantiation
     */
    private StringUtil() {
    }

    /**
     * Check if a string matches one of given regular expressions
     * 
     * @param stringToMatch
     *            the string
     * @param regularExpressions
     *            the regular expressions
     * @return if the string matches one of the expressions
     */
    public static boolean matchesOneOf(String stringToMatch, Collection<String> regularExpressions) {
        for (String regex : regularExpressions) {
            if (stringToMatch.matches(regex)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if a string contains a word (as a while) containsWord(
     * "How much is that doggy in the window", "dog") == false containsWord(
     * "Who let the dog out?", "dog") == true
     *
     * @param stringToMatch
     *            the string to search in
     * @param wordToFind
     *            the word to find
     * @return if the string contains the given word
     */
    public static boolean containsWord(String stringToMatch, String wordToFind) {
        return Pattern.compile(WORD_BREAK + wordToFind + WORD_BREAK).matcher(stringToMatch).find();
    }

    /**
     * Get the first match of a given regular expression in a string
     *
     * @param stringToMatch
     *            the string to search in
     * @param regExToFind
     *            the regex to find
     * @return the first match or null if not found
     */
    public static String getFirstMatch(String stringToMatch, String regExToFind) {
        final Matcher matcher = Pattern.compile(regExToFind).matcher(stringToMatch);
        if (matcher.find()) {
            return matcher.group();
        }
        return null;
    }

    /**
     * Get the first group of the first match a given regular expression in a
     * string The regular expression should contain a group
     *
     * @param stringToMatch
     *            the string to search in
     * @param regExToFind
     *            the regex to find
     * @return the first match or null if not found
     */
    public static String getFirstGroup(String stringToMatch, String regExToFind) {
        final Matcher matcher = Pattern.compile(regExToFind).matcher(stringToMatch);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }

    /**
     * Get the all groups of the first match a given regular expression in a
     * string The regular expression should contain a group
     *
     * @param stringToMatch
     *            the string to search in
     * @param regExToFind
     *            the regex to find
     * @return the first match or null if not found
     */
    public static String[] getGroups(String stringToMatch, String regExToFind) {
        final Matcher matcher = Pattern.compile(regExToFind).matcher(stringToMatch);
        if (matcher.find()) {
            String[] returnValue = new String[matcher.groupCount()];
            for (int i = 0; i < matcher.groupCount(); i++) {
                returnValue[i] = matcher.group(i + 1);
            }
            return returnValue;
        }
        return null;
    }

    /**
     * Get the last match of a given regular expression in a string
     *
     * @param stringToMatch
     *            the string to search in
     * @param regExToFind
     *            the regex to find
     * @return the first match or null if not found
     */
    public static String getLastMatch(String stringToMatch, String regExToFind) {
        final Matcher matcher = Pattern.compile(regExToFind).matcher(stringToMatch);
        String lastMatch = null;
        while (matcher.find()) {
            lastMatch = matcher.group();
        }
        return lastMatch;
    }

    /**
     * Try to find the values of the groups of the first regularExpression and
     * match that produces a given number of groups
     * 
     * @param stringToMatch
     *            the string to match
     * @param regularExpressions
     *            the regularExpressions to use
     * @param numberOfGroups
     *            the number of expected groups
     * @return the group values
     */
    public static String[] getGroupsOfOneOf(String stringToMatch, Collection<String> regularExpressions,
            int numberOfGroups) {
        for (String regEx : regularExpressions) {
            final Matcher matcher = Pattern.compile(regEx).matcher(stringToMatch);
            while (matcher.find()) {
                // CHECKSTYLE:OFF:MagicNumber FOR 3 LINES
                if (matcher.groupCount() == numberOfGroups) {
                    String[] returnValue = new String[numberOfGroups];
                    for (int i = 0; i < numberOfGroups; i++) {
                        returnValue[i] = matcher.group(i + 1);
                    }
                    return returnValue;
                }
            }
        }
        return null;
    }

    /**
     * Replace accented characters by normal ones from a string (e.g.replace é
     * by e)
     * 
     * @param str
     *            the string to remove accented characters
     * @return the normalized string
     */
    public static String deAccent(String str) {
        final String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
        final Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(nfdNormalizedString).replaceAll("").replace('³', '3').replace('²', '2');
    }

    public static String trimWithNbsp(String string) {
        if (string == null) {
            return string;
        }
        return string.replace('\u00A0', ' ').trim();
    }
    
    public static String removeNonPrintables(String string) {
    	StringBuilder newString = new StringBuilder(string.length());
    	for (int offset = 0; offset < string.length();) {
    	    int codePoint = string.codePointAt(offset);
    	    offset += Character.charCount(codePoint);

    	    // Replace invisible control characters and unused code points
    	    switch (Character.getType(codePoint))
    	    {
    	        case Character.CONTROL:     // \p{Cc}
    	        case Character.FORMAT:      // \p{Cf}
    	        case Character.PRIVATE_USE: // \p{Co}
    	        case Character.SURROGATE:   // \p{Cs}
    	        case Character.UNASSIGNED:  // \p{Cn}
    	            newString.append(codePoint==NEWLINE_CODEPOINT?'\n':'?');
    	            break;
    	        default:
    	            newString.append(Character.toChars(codePoint));
    	            break;
    	    }
    	}
    	return newString.toString();
    }

    public static String removeNonPrintables2(String string) {
    	StringBuilder newString = new StringBuilder(string.length());
    	for (int offset = 0; offset < string.length();offset++) {
    		char c= string.charAt(offset);
    		int asInt = (int)c;
    		if (asInt>256 || (asInt<32 && asInt!=10)) {
    			newString.append('?');
    		} else {
    			newString.append(c);
    		}
    	}
    	return newString.toString();
    }
}
