package org.geockolibre.scrape.util;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.geockolibre.gpx.reader.GPX;
import org.geockolibre.gpx.reader.GPXRoutePoint;
import org.geockolibre.gpx.reader.GPXTrackPoint;
import org.geockolibre.gpx.reader.GPXTrackSeg;
import org.geockolibre.gpx.reader.GPXWayPoint;
import org.geockolibre.scrape.Location;

public class LocationUtil {
	public static double distanceInM(Location location1, Location location2) {
		return location1.distance(location2);
	}
	
	public static Location midPoint(Location location1, Location location2) {
		double lon1 = location1.getLongitude().getDegreeWithFraction();
		double lon2 = location2.getLongitude().getDegreeWithFraction();
		double lat1 = location1.getLatitude().getDegreeWithFraction();
		double lat2 = location2.getLatitude().getDegreeWithFraction();
	    double dLon = Math.toRadians(lon2 - lon1);

	    //convert to radians
	    lat1 = Math.toRadians(lat1);
	    lat2 = Math.toRadians(lat2);
	    lon1 = Math.toRadians(lon1);

	    double Bx = Math.cos(lat2) * Math.cos(dLon);
	    double By = Math.cos(lat2) * Math.sin(dLon);
	    double lat3 = Math.atan2(Math.sin(lat1) + Math.sin(lat2), Math.sqrt((Math.cos(lat1) + Bx) * (Math.cos(lat1) + Bx) + By * By));
	    double lon3 = lon1 + Math.atan2(By, Math.cos(lat1) + Bx);

	    return new Location(Math.toDegrees(lat3), Math.toDegrees(lon3));
	}		

	/**
	 * @param location1
	 * @param location2
	 * @param fraction
	 * @return
	 */
	public static Location intermediatePoint(Location location1, Location location2, double fraction) {
		if (fraction <= 0.0001) {
			return location1;
		}
		if (fraction >= 0.9999) {
			return location2;
		}
		double lon1 = location1.getLongitude().getDegreeWithFraction();
		double lon2 = location2.getLongitude().getDegreeWithFraction();
		double lat1 = location1.getLatitude().getDegreeWithFraction();
		double lat2 = location2.getLatitude().getDegreeWithFraction();
	    double dLon = Math.toRadians(lon2 - lon1);
	       // Convert microdegrees to radians
        // Calculate common variables
        double alatRadSin=Math.sin(Math.toRadians(lat1));
        double blatRadSin=Math.sin(Math.toRadians(lat2));
        double alatRadCos=Math.cos(Math.toRadians(lat1));
        double blatRadCos=Math.cos(Math.toRadians(lat2));
        double dlonCos=Math.cos(dLon);
        
        
        // Find distance from A to B
        double distance=Math.acos(alatRadSin*blatRadSin +
                                  alatRadCos*blatRadCos *
                                  dlonCos);
        // Find bearing from A to B
        double bearing=Math.atan2(
            Math.sin(dLon) * blatRadCos,
            alatRadCos*blatRadSin -
            alatRadSin*blatRadCos*dlonCos);
        // Find new point
        double angularDistance=distance*fraction;
        double angDistSin=Math.sin(angularDistance);
        double angDistCos=Math.cos(angularDistance);
        double xlatRad = Math.asin( alatRadSin*angDistCos +
                                   alatRadCos*angDistSin*Math.cos(bearing) );
        double xlonRad = Math.toRadians(lon1) + Math.atan2(
            Math.sin(bearing)*angDistSin*alatRadCos,
            angDistCos-alatRadSin*Math.sin(xlatRad));
        // Convert radians to microdegrees
        return new Location(Math.toDegrees(xlatRad), Math.toDegrees(xlonRad));
    }
	
	
	public static Location getLocationFromStart(Location start, Location end, double distance) {
		double totalDistance = distanceInM(start, end);
		double fraction = distance / totalDistance;
		return intermediatePoint(start, end, fraction);
	}
	
	public static List<List<Location>> getGpxTrackLocations(GPX gpx) {
		List<List<Location>> lists = new ArrayList<>();
		gpx.getTracks().stream()
			.map(t->t.stream()
				.flatMap(GPXTrackSeg::stream)
				.map(GPXTrackPoint::getLocation)
				.collect(Collectors.toList())
			).forEachOrdered(lists::add);
		gpx.getRoutes().stream()
			.map(r->r.stream()
				.map(GPXRoutePoint::getLocation)
				.collect(Collectors.toList())
			).forEachOrdered(lists::add);
	
		gpx.getWayPoints().stream()
			.map(GPXWayPoint::getLocation)
			.map(wp->Collections.singletonList(wp))
			.forEachOrdered(lists::add);
		return lists;
	}

	/**
	 * Defines all locations that have a distance of 'stepDistance' between them and
	 * are on the route/track defined by given location list 
	 * @param locations
	 * @param stepDistance
	 * @return step location
	 */
	public static List<Location> getStepLocations(List<Location> locations, double stepDistance) {
		if (locations.isEmpty()) {
			return Collections.emptyList(); 
		}
		if (locations.size()==1) {
			return locations;
		}
		List<Location> result = new ArrayList<>();
		Location lastLocation = locations.get(0);
		result.add(lastLocation);
		int nextIdxLocation = 1;
		double distanceLeft = stepDistance;
		while (nextIdxLocation<locations.size()) {
			Location nextLocation = locations.get(nextIdxLocation);
			double distance = distanceInM(lastLocation, nextLocation);
			if (distance>=distanceLeft) {
				Location step = intermediatePoint(lastLocation, nextLocation, distanceLeft/distance);
				result.add(step);
				distanceLeft = stepDistance;
				lastLocation = step;
				if (lastLocation.equals(nextLocation)) {
					nextIdxLocation++;
				}
			} else {
				distanceLeft -=distance;
				lastLocation = nextLocation;
				nextIdxLocation++;
			}
		}
		if (result.size()==1 || lastLocation.distance(locations.get(locations.size()-1)) > 10) {
			result.add(locations.get(locations.size()-1));
		}
		return result;
	}
	
	public static double getTotalDistanceInM(List<Location> locations) {
		Location last = null;
		double totalDistance = 0;
		for (Location loc : locations) {
			if (last!=null) {
				totalDistance += LocationUtil.distanceInM(last, loc);
			}
			last=loc;
		}
		return totalDistance;
	}

	public static double getTotalDistanceInMForList(List<List<Location>> locations) {
		return locations.stream().map(l->getTotalDistanceInM(l)).reduce(0.0, Double::sum);
	}
}
