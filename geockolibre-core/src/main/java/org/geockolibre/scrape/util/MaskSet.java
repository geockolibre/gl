package org.geockolibre.scrape.util;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.geockolibre.scrape.CacheType;

/**
 * Masks for image matching.
 * 
 * Holds the images for the templates of large, medium, and small icons.
 * 
 * Also holds the images for the templates used to detect the large and small
 * icon borders.
 * 
 */
public class MaskSet implements Serializable {
    private static final long serialVersionUID = -3755135390268678480L;
    private BufferedImage bigIconBorder;
    private BufferedImage smallIconBorder;

    /**
     * Large icons.
     */
    private BufferedImage largeOwn;
    private BufferedImage largeTrad;
    private BufferedImage largeApe;
    private BufferedImage largeLetter;
    private BufferedImage largeEvent;
    private BufferedImage largeVirtual;
    private BufferedImage largeEarth;
    private BufferedImage largeMulti;
    private BufferedImage largeCito;
    private BufferedImage largeMyst;
    private BufferedImage largeFound;
    private BufferedImage largeWherigo;
    private BufferedImage largeWebcam;
    private BufferedImage largeGpsAdv;
    private BufferedImage largeMega;

    /**
     * Medium icons.
     */
    private BufferedImage mediumTrad;
    private BufferedImage mediumMulti;
    private BufferedImage mediumEvent;
    private BufferedImage mediumVirtual;
    private BufferedImage mediumMyst;
    private BufferedImage mediumFound;
    private BufferedImage mediumOwn;

    /**
     * Small icons.
     */
    private BufferedImage smallTrad;
    private BufferedImage smallMulti;
    private BufferedImage smallMyst;
    private BufferedImage smallVirtual;
    private BufferedImage smallEvent;

    private List<MaskEntry> largeEntries = new ArrayList<MaskEntry>();
    private List<MaskEntry> mediumEntries = new ArrayList<MaskEntry>();
    private List<MaskEntry> smallEntries = new ArrayList<MaskEntry>();

    public MaskSet() {

    }

    /**
     * @param bigIconBorder
     *            the bigIconBorder to set
     */
    public void setBigIconBorder(BufferedImage bigIconBorder) {
        this.bigIconBorder = bigIconBorder;
    }

    /**
     * @return the bigIconBorder
     */
    public BufferedImage getBigIconBorder() {
        return bigIconBorder;
    }

    public void setSmallIconBorder(BufferedImage samllIconBorder) {
        this.smallIconBorder = samllIconBorder;
    }

    public BufferedImage getSmallIconBorder() {
        return this.smallIconBorder;
    }

    public void setLargeOwn(BufferedImage read) {
        this.largeOwn = read;
        largeEntries.add(new MaskEntry(read, CacheType.OWN));
    }

    public void setLargeTrad(BufferedImage read) {
        this.largeTrad = read;
        largeEntries.add(new MaskEntry(read, CacheType.TRADITIONAL));
    }

    public void setLargeApe(BufferedImage read) {
        this.largeApe = read;
        largeEntries.add(new MaskEntry(read, CacheType.PROJECTAPE1));
    }

    public void setLargeLetter(BufferedImage read) {
        this.largeLetter = read;
        largeEntries.add(new MaskEntry(read, CacheType.LETTERBOX));
    }

    public void setLargeEvent(BufferedImage read) {
        this.largeEvent = read;
        largeEntries.add(new MaskEntry(read, CacheType.EVENT));
    }

    public void setLargeVirtual(BufferedImage read) {
        this.largeVirtual = read;
        largeEntries.add(new MaskEntry(read, CacheType.VIRTUAL));
    }

    public void setLargeEarth(BufferedImage read) {
        this.largeEarth = read;
        largeEntries.add(new MaskEntry(read, CacheType.EARTH_CACHE));
    }

    public void setLargeMulti(BufferedImage read) {
        this.largeMulti = read;
        largeEntries.add(new MaskEntry(read, CacheType.MULTI));
    }

    public void setLargeCito(BufferedImage read) {
        this.largeCito = read;
        largeEntries.add(new MaskEntry(read, CacheType.CITO));
    }

    public void setLargeMyst(BufferedImage read) {
        this.largeMyst = read;
        largeEntries.add(new MaskEntry(read, CacheType.MYSTERY));
    }

    public void setLargeFound(BufferedImage read) {
        this.largeFound = read;
        largeEntries.add(new MaskEntry(read, CacheType.FOUND));
    }

    public void setLargeWherigo(BufferedImage read) {
        this.largeWherigo = read;
        largeEntries.add(new MaskEntry(read, CacheType.WHERIGO));
    }

    public void setLargeWebcam(BufferedImage read) {
        this.largeWebcam = read;
        largeEntries.add(new MaskEntry(read, CacheType.WEBCAM));
    }

    public void setLargeGpsAdv(BufferedImage read) {
        this.largeGpsAdv = read;
        largeEntries.add(new MaskEntry(read, CacheType.GPSADVENTURE));
    }

    public void setLargeMega(BufferedImage read) {
        this.largeMega = read;
        largeEntries.add(new MaskEntry(read, CacheType.MEGA_EVENT));
    }

    /**
     * @return the mediumTrad
     */
    public BufferedImage getMediumTrad() {
        return mediumTrad;
    }

    /**
     * @param mediumTrad
     *            the mediumTrad to set
     */
    public void setMediumTrad(BufferedImage read) {
        this.mediumTrad = read;
        this.mediumEntries.add(new MaskEntry(read, CacheType.ESTIMATE_TRAD));
    }

    /**
     * @return the mediumMulti
     */
    public BufferedImage getMediumMulti() {
        return mediumMulti;
    }

    /**
     * @param mediumMulti
     *            the mediumMulti to set
     */
    public void setMediumMulti(BufferedImage read) {
        this.mediumMulti = read;
        this.mediumEntries.add(new MaskEntry(read, CacheType.MULTI));
    }

    /**
     * @return the mediumEvent
     */
    public BufferedImage getMediumEvent() {
        return mediumEvent;
    }

    /**
     * @param mediumEvent
     *            the mediumEvent to set
     */
    public void setMediumEvent(BufferedImage read) {
        this.mediumEvent = read;
        this.mediumEntries.add(new MaskEntry(read, CacheType.ESTIMATE_EVENT));
    }

    /**
     * @return the mediumVirtual
     */
    public BufferedImage getMediumVirtual() {
        return mediumVirtual;
    }

    /**
     * @param mediumVirtual
     *            the mediumVirtual to set
     */
    public void setMediumVirtual(BufferedImage read) {
        this.mediumVirtual = read;
        this.mediumEntries.add(new MaskEntry(read, CacheType.ESTIMATE_VIRT));
    }

    /**
     * @return the mediumMyst
     */
    public BufferedImage getMediumMyst() {
        return mediumMyst;
    }

    /**
     * @param mediumMyst
     *            the mediumMyst to set
     */
    public void setMediumMyst(BufferedImage read) {
        this.mediumMyst = read;
        this.mediumEntries.add(new MaskEntry(read, CacheType.ESTIMATE_MYST));
    }

    /**
     * @return the mediumFound
     */
    public BufferedImage getMediumFound() {
        return mediumFound;
    }

    /**
     * @param mediumFound
     *            the mediumFound to set
     */
    public void setMediumFound(BufferedImage read) {
        this.mediumFound = read;
        this.mediumEntries.add(new MaskEntry(read, CacheType.FOUND));
    }

    /**
     * @return the mediumOwn
     */
    public BufferedImage getMediumOwn() {
        return mediumOwn;
    }

    /**
     * @param mediumOwn
     *            the mediumOwn to set
     */
    public void setMediumOwn(BufferedImage read) {
        this.mediumOwn = read;
        this.mediumEntries.add(new MaskEntry(read, CacheType.OWN));
    }

    /**
     * @return the smallTrad
     */
    public BufferedImage getSmallTrad() {
        return smallTrad;
    }

    /**
     * @param smallTrad
     *            the smallTrad to set
     */
    public void setSmallTrad(BufferedImage read) {
        this.smallTrad = read;
        this.smallEntries.add(new MaskEntry(read, CacheType.ESTIMATE_TRAD));
    }

    /**
     * @return the smallMulti
     */
    public BufferedImage getSmallMulti() {
        return smallMulti;
    }

    /**
     * @param smallMulti
     *            the smallMulti to set
     */
    public void setSmallMulti(BufferedImage read) {
        this.smallMulti = read;
        this.smallEntries.add(new MaskEntry(read, CacheType.MULTI));
    }

    /**
     * @return the smallMyst
     */
    public BufferedImage getSmallMyst() {
        return smallMyst;
    }

    /**
     * @param smallMyst
     *            the smallMyst to set
     */
    public void setSmallMyst(BufferedImage read) {
        this.smallMyst = read;
        this.smallEntries.add(new MaskEntry(read, CacheType.ESTIMATE_MYST));
    }

    /**
     * @return the smallVirtual
     */
    public BufferedImage getSmallVirtual() {
        return smallVirtual;
    }

    /**
     * @param smallVirtual
     *            the smallVirtual to set
     */
    public void setSmallVirtual(BufferedImage read) {
        this.smallVirtual = read;
        this.smallEntries.add(new MaskEntry(read, CacheType.ESTIMATE_VIRT));
    }

    /**
     * @return the smallEvent
     */
    public BufferedImage getSmallEvent() {
        return smallEvent;
    }

    /**
     * @param read
     *            the smallEvent to set
     */
    public void setSmallEvent(BufferedImage read) {
        this.smallEvent = read;
        this.smallEntries.add(new MaskEntry(read, CacheType.ESTIMATE_EVENT));
    }

    /**
     * @return the largeOwn
     */
    public BufferedImage getLargeOwn() {
        return largeOwn;
    }

    /**
     * @return the largeTrad
     */
    public BufferedImage getLargeTrad() {
        return largeTrad;
    }

    /**
     * @return the largeApe
     */
    public BufferedImage getLargeApe() {
        return largeApe;
    }

    /**
     * @return the largeLetter
     */
    public BufferedImage getLargeLetter() {
        return largeLetter;
    }

    /**
     * @return the largeEvent
     */
    public BufferedImage getLargeEvent() {
        return largeEvent;
    }

    /**
     * @return the largeVirtual
     */
    public BufferedImage getLargeVirtual() {
        return largeVirtual;
    }

    /**
     * @return the largeEarth
     */
    public BufferedImage getLargeEarth() {
        return largeEarth;
    }

    /**
     * @return the largeMulti
     */
    public BufferedImage getLargeMulti() {
        return largeMulti;
    }

    /**
     * @return the largeCito
     */
    public BufferedImage getLargeCito() {
        return largeCito;
    }

    /**
     * @return the largeMyst
     */
    public BufferedImage getLargeMyst() {
        return largeMyst;
    }

    /**
     * @return the largeFound
     */
    public BufferedImage getLargeFound() {
        return largeFound;
    }

    /**
     * @return the largeWherigo
     */
    public BufferedImage getLargeWherigo() {
        return largeWherigo;
    }

    /**
     * @return the largeWebcam
     */
    public BufferedImage getLargeWebcam() {
        return largeWebcam;
    }

    /**
     * @return the largeGpsAdv
     */
    public BufferedImage getLargeGpsAdv() {
        return largeGpsAdv;
    }

    /**
     * @return the largeMega
     */
    public BufferedImage getLargeMega() {
        return largeMega;
    }

    /**
     * @return the largeEntries
     */
    public List<MaskEntry> getLargeEntries() {
        return largeEntries;
    }

    /**
     * @return the mediumEntries
     */
    public List<MaskEntry> getMediumEntries() {
        return mediumEntries;
    }

    /**
     * @return the smallEntries
     */
    public List<MaskEntry> getSmallEntries() {
        return smallEntries;
    }

    public class MaskEntry {
        private CacheType entry;
        private BufferedImage mask;

        public MaskEntry(BufferedImage mask, CacheType entry) {
            this.mask = mask;
            this.entry = entry;
        }

        /**
         * @return the entry
         */
        public CacheType getEntry() {
            return entry;
        }

        /**
         * @return the mask
         */
        public BufferedImage getMask() {
            return mask;
        }

    }
}
