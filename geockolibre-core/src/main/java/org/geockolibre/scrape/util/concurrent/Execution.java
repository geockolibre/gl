package org.geockolibre.scrape.util.concurrent;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

/**
 * Execution of a task
 * @param <T> the type of return value
 */
public class Execution<T> {
    private final AtomicInteger atomicState = new AtomicInteger(0);
    private Consumer<Execution<T>> callback;
    private Throwable error;
    private T result;

    /**
     * Enum for execution state
     */
    public enum ExecutionState {
        /** No state (yet) */
        NONE,
        /** Waiting for execution, not run yet */
        WAITING,
        /** Cancelled */
        CANCELLED,
        /** Running */
        RUNNING,
        /** finished successfully */
        FINISHED,
        /** Failed execution */
        FAILED;
    };
    
    /**
     * Constructor for an already finished task
     * @param result the result
     * @param callback the callback
     */
    public Execution(T result, Consumer<Execution<T>> callback) {
        this.result = result;
        this.atomicState.set(ExecutionState.FINISHED.ordinal());
        this.callback = callback;
    }

    /**
     * Constructor for an already failed task
     * @param error the error
     * @param callback the callback
     */
    public Execution(Throwable error, Consumer<Execution<T>> callback) {
        this.error = error;
        this.atomicState.set(ExecutionState.FAILED.ordinal());
        this.callback = callback;
    }

    /**
     * Normal callback
     * @param callback the callback to run after execution
     */
    public Execution(Consumer<Execution<T>> callback) {
        this.callback = callback;
    }

    /**
     * Get the state
     * @return the state
     */
    public ExecutionState getState() {
        return ExecutionState.values()[atomicState.get()];
    }
    /**
     * get the callback
     * @return the callback
     */
    protected Consumer<Execution<T>> getCallback() {
        return callback;
    }
    public Throwable getError() {
        return error;
    }
    public T getResult() {
        return result;
    }
    protected AtomicInteger getAtomicState() {
        return atomicState;
    }
    protected void setError(Throwable error) {
        this.error = error;
    }
    protected void setResult(T result) {
        this.result = result;
    }
    
    /**
     * Cancel the task
     * @return if cancelling was successful. Cancelling should only happen if the task was in #WAITING state
     */
    public boolean cancel() {
        callback=null;
        return atomicState.compareAndSet(ExecutionState.WAITING.ordinal(), ExecutionState.CANCELLED.ordinal());
    }
}