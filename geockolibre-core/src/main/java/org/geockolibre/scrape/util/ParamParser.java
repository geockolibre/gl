package org.geockolibre.scrape.util;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Utility class for parsing command line parameters.
 *
 */
public class ParamParser {

    /**
     * Get the value of the given parameter. If the parameter is present, but
     * has no value, an empty string is returned. If the parameter is not
     * present, null is returned.
     * 
     * @param param
     * @param args
     * @return
     */
    public static String getParameter(String param, String[] args) {
        String res = null;
        for (int x = 0; x < args.length; x++) {
            if (args[x].startsWith("-")) {
                if (args[x].substring(1).equals(param)) {
                    // parameter found
                    if (x >= args.length - 1 || args[x + 1].startsWith("-")) {
                        // no value
                        res = "";
                    } else {
                        // value is next argument
                        res = args[x + 1];
                    }
                }
            }
        }
        return res;
    }

    /**
     * Check if a given parameter is present or not.
     * 
     * @param param
     * @param args
     * @return true if the parameter/switch is given, false otherwise
     */
    public static boolean isParameterPresent(String param, String[] args) {
        return getParameter(param, args) != null;
    }
}
