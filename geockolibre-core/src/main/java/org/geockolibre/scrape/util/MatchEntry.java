package org.geockolibre.scrape.util;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;

public class MatchEntry implements Serializable {
    private static final long serialVersionUID = 7125031987957125809L;
    private boolean unknown;
    private int[] match;

    public MatchEntry(int[] match) {
        this.match = match;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof MatchEntry) {
            MatchEntry otherMatch = (MatchEntry) other;
            for (int x = 0; x < match.length; x++) {
                if (match[x] != otherMatch.match[x]) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    public int[] getMatch() {
        return match;
    }

    /**
     * @return the unknown
     */
    public boolean isUnknown() {
        return unknown;
    }

    /**
     * @param unknown
     *            the unknown to set
     */
    public void setUnknown(boolean unknown) {
        this.unknown = unknown;
    }
}
