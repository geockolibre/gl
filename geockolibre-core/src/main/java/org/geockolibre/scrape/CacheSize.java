package org.geockolibre.scrape;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Enumerates the different CacheSizes
 */
public enum CacheSize {
    // No javadoc: Enumeration names are self explaining
    // CHECKSTYLE:OFF:Javadoc FOR 1 LINE
    NANO, MICRO, SMALL, REGULAR, LARGE, UKNOWN, OTHER, VIRTUAL, NOT_CHOSEN;

    /**
     * Parse the cache size from a string
     * @param name the name of the cache size
     * @return the CacheSize
     */
    public static CacheSize parse(String name) {
        return CacheSize.valueOf(name.toUpperCase().replaceAll(" ", "_"));
    }

    @Override
    public String toString() {
        // get the name
        String name = super.toString();
        // convert to lowercase
        name = name.toLowerCase();
        // change underscore to space
        name = name.replaceAll("_", " ");
        // capitalise first letter
        String firstLetter = name.substring(0, 1).toUpperCase();
        String rest = name.substring(1);
        return firstLetter + rest;

    }
}
