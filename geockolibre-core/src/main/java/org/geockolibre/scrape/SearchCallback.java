package org.geockolibre.scrape;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Objects that want notification of found caches should implement this
 * interface and register with the Searcher object.
 *
 */
public interface SearchCallback {
    /**
     * This is called every time a cache is found.
     * 
     * This method should not block, wait or perform time-consuming
     * computations. This method should return as fast as possible.
     * 
     * @param cache
     */
    public void found(Cache cache);

    /**
     * This is called when a search is initiated.
     * 
     * This method should not block, wait or perform time-consuming
     * computations. This method should return as fast as possible.
     * 
     * @param n
     *            the number of caches to be returned by this search.
     */
    public void totalNumber(int n);
}
