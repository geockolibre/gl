package org.geockolibre.scrape;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

import org.geockolibre.scrape.util.HtmlParser;
import org.geockolibre.scrape.util.UserAgentFaker;
import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * A class that maintains a login/password pair and, once authenticated to the
 * site, tracks the credentials.
 * 
 */
public class Login {
    private static final String GSPKAUTH_COOKIE = "gspkauth";

    private static final String SESSION_ID_COOKIE = "ASP.NET_SessionId";

    private static final String LOGIN_STRING = "https://www.geocaching.com/login/default.aspx";

    private String userName;
    private String password;
    private String token;
    private String gspkAuth;

    private String sessionToken;
    private String magic;

    private Long id;
    private boolean authenticated = false;

    // default date format
    private String dateFormat = null;

    private Location homeLocation;

    public Login(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @return true if the Login is authenticated
     */
    public boolean isAuthenticated() {
        return authenticated;
    }

    /**
     * Attempt to authenticate
     * 
     * @throws IOException
     */
    public void authenticate() throws IOException {
        final Connection detailConnection = withRandomUseragent(Jsoup.connect(LOGIN_STRING));
        Document page = detailConnection.get();

        HashMap<String, String> values = HtmlParser.getViewState(page);
        values.put("__EVENTTARGET", "");
        values.put("__EVENTARGUMENT", "");
        values.put("ctl00$ContentBody$tbUsername", this.getUserName());
        values.put("ctl00$ContentBody$tbPassword", this.getPassword());
        values.put("ctl00$ContentBody$cbRememberMe", "on");
        values.put("ctl00$ContentBody$btnSignIn", "Login");
        detailConnection.data(values);
        Connection.Response response = withRandomUseragent(Jsoup.connect(LOGIN_STRING)).data(values).method(Method.POST).execute();
        token = response.cookie(SESSION_ID_COOKIE);
        gspkAuth = response.cookie(GSPKAUTH_COOKIE);
        if (token!=null && gspkAuth!=null) {
        	authenticated = true;
        }
    }

	public void loadHomeLocation() throws IOException {
		// get the home location
		Document homeLocationDoc = getDocument("https://www.geocaching.com/account/settings/homelocation");
		String homeLocationText = homeLocationDoc.getElementById("Query").val();
		if (homeLocationText != null) {
		    homeLocation = new Location(homeLocationText);
		}
	}

	public void loadUserID() throws IOException {
		// get the user id
		Document membershipDoc = getDocument("https://www.geocaching.com/account/settings/membership");
		String memberIdText = membershipDoc.select("dl.membership-details dd").first().text();
		if (memberIdText != null) {
		    memberIdText = memberIdText.replaceAll(" ", "");
		    id = Long.parseLong(memberIdText);
		    authenticated = true;
		}
	}

    public Connection getConnection(String url) {
        return addCookies(withRandomUseragent(Jsoup.connect(url)));
    }
    public Connection withRandomUseragent(Connection connection) {
    	return connection.userAgent(UserAgentFaker.getRandomUserAgent());
    }

    public Document getDocument(String url) throws IOException {
        for (int i = 0; i < 3; i++) {
            try {
                return getConnection(url).get();
            } catch (SocketTimeoutException e) {
                if (i < 2) {
                    // Ignore and try again
                } else {
                    throw e;
                }
            }
        }
        throw new RuntimeException("Unaccessable. Can not come here");
    }

    public Connection addCookies(Connection connection) {
        return connection.cookie(SESSION_ID_COOKIE, token).cookie(GSPKAUTH_COOKIE, gspkAuth).cookie("Culture", "en-US");
    }

    /**
     * Get the date format used by the current user on the site.
     * 
     * @return the dateFormat
     */
    public String getDateFormat() {
        if (dateFormat == null) {
            checkDateFormat();
        }
        return dateFormat;
    }

    public Location getHomeLocation() {
        return homeLocation;
    }

    /**
     * Load the user account page and decode the date format.
     * 
     */
    private void checkDateFormat() {
        try {
            Document dateFormatDoc = getDocument("https://www.geocaching.com/account/settings/preferences");
            dateFormat = dateFormatDoc.select("select#SelectedDateFormat option[selected]").val();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Date parseDate(String someDate) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(getDateFormat(), Locale.ENGLISH);
        sdf.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles"));
        return sdf.parse(someDate.trim());
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() throws IOException {
        return id;
    }

    public void setToken(String token) {
		this.token = token;
	}
    
    public void setGspkAuth(String gspkAuth) {
		this.gspkAuth = gspkAuth;
	}
    public String getGspkAuth() {
		return gspkAuth;
	}
    public void setAuthenticated(boolean authenticated) {
		this.authenticated = authenticated;
	}
    public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}
    public void setHomeLocation(Location homeLocation) {
		this.homeLocation = homeLocation;
	}
    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    public String getMagic() {
        return magic;
    }

    public void setMagic(String magic) {
        this.magic = magic;
    }

}
