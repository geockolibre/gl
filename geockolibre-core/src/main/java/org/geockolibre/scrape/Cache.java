package org.geockolibre.scrape;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

import org.apache.commons.lang3.RandomUtils;
import org.geockolibre.scrape.util.StringUtil;
import org.geockolibre.scrape.util.Tools;
import org.geockolibre.util.json.JSONArray;
import org.geockolibre.util.json.JSONException;
import org.geockolibre.util.json.JSONObject;
import org.geockolibre.util.json.JSONTokener;
import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Model and import for caches TODO Remove the actual import to another object.
 * Cache should be a plain data object
 */
// CHECKSTYLE:OFF:MagicNumber
// CHECKSTYLE:OFF:StringLiteral
public class Cache extends BasePlace implements Serializable {
	private static final Logger LOGGER = LoggerFactory.getLogger(Cache.class);
    private static final long serialVersionUID = 4163848032478184372L;
    private static final String CACHE_SEARCH_BASE = "https://www.geocaching.com/seek/cache_details.aspx?wp=";

    private String cacheCode;
    private Long id;
    private String guid;
    private long placedAt;
    private CacheType cacheType;
    private CacheSize cacheSize;
    private double difficultyRating;
    private double terrainRating;
    private int favourited;
    private String shortDescription;
    private String longDescription;
    private String hint;
    private String locationDescription;
    private String shortCutURL;

    private List<CacheLog> logs = new ArrayList<CacheLog>();
    private Set<Attribute> attributes = new HashSet<Attribute>();
    private List<WayPoint> waypoints = new ArrayList<WayPoint>();
    private Boolean found;
    private boolean foundByUser;
    private boolean disabled;
    private boolean archived;
    private boolean premiumonly;
    private String userToken;
    private int pageCount;
    private String mapRequestCode;
    private int estimatelevel;
    private int trackableCount;
    private Long lastFoundDate;
    private boolean unavailabletous;

    /**
     * Constructor
     */
    public Cache() {
    }

    /**
     * Constructor
     * @param cacheCode the code of the cache (GCxxxxx)
     */
    public Cache(String cacheCode) {
        this.cacheCode = cacheCode;
    }

    /**
     * Populate the cache details.
     * 
     * @param login The login to connect to geocaching.com
     * @param fullLogs true if full logs, otherwise only recent logs will be loaded.
     * @throws IOException in case we cannot connect correctly
     * @throws ParseException in case the read data can not be parsed
     */
    public void populate(Login login, boolean fullLogs) throws IOException, ParseException {
        if (cacheCode == null) {
            if (this.mapRequestCode != null) {
                try {
                    this.populateFromMap(login);
                } catch (JSONException e) {
                    throw new IOException(e);
                }
            } else {
                throw new IllegalStateException("cacheCode must be set before calling populate.");
            }
        }
        // get the location of the cache description
        String urlString = CACHE_SEARCH_BASE + getCacheCode();
        if (shortCutURL != null) {
            urlString = shortCutURL;
        }

        Document dateFormatDoc = login.getDocument(urlString);
        userToken = StringUtil.getFirstGroup(dateFormatDoc.html(), "userToken\\s*=\\s*['\"]([A-Za-z0-9]+)['\"]");

        // parse the HTML
        parse(dateFormatDoc, login);
        if (fullLogs) {
            // CHECKSTYLE:OFF:EmptyBlock FOR 2 LINES
            while (retrieveMoreLogs(login, Integer.MAX_VALUE)) {}
        }
    }

    /**
     * Calls the GC.com site to get more logs. Returns true if more logs are
     * available, false otherwise.
     * 
     * @param login the login to connect to geocaching.com
     * @return false if all logs are loaded.
     * @throws IOException in case the site can not be contacted
     * @throws ParseException is case the received HTML is invalid
     */
    public boolean retrieveMoreLogs(Login login, int max) throws IOException, ParseException {
        boolean res = false;
        if (userToken != null) {
            pageCount = logs.size() / 25;
            StringBuilder request = new StringBuilder("https://www.geocaching.com/seek/geocache.logbook?tkn=");
            request.append(userToken).append("&idx=").append(Integer.toString(1 + pageCount))
                    .append("&num=25&decrypt=true");
            Connection connection = login.getConnection(request.toString());
            connection.method(Method.GET);
            connection.ignoreContentType(true);
            Response response = connection.execute();
            String contents = response.body();
            try {
                res = parseJsonLogs(contents, login, max);
            } catch (JSONException e) {
            	LOGGER.error("Problem reading logs for cache " + this.getCacheCode(), e);
                throw new IOException(e);
            }
        }

        return res;
    }

    /**
     * Parses the json logs
     * @param contents the logs in json format
     * @param login the login to connect to geocaching.com
     * @throws JSONException in case the received json is invalid
     * @throws ParseException is case the received HTML is invalid
     * @return If there are more logs to be fetched and parsed
     */
    private boolean parseJsonLogs(String contents, Login login, int max) throws ParseException, JSONException {
        boolean res = false;
        int found = 0;
        // de-tokenise the string
        JSONTokener jtok = new JSONTokener(contents);
        while (jtok.more()) {
            Object o = jtok.nextValue();

            if (o instanceof JSONObject) {
                JSONObject obj = (JSONObject) o;
                String status = (String) obj.get("status");
                if (status.equals("success")) {
                    JSONArray data = obj.getJSONArray("data");
                    for (int index = 0; index < data.length(); index++) {
                        JSONObject tmp = data.getJSONObject(index);
                        CacheLog log = new CacheLog(tmp, login);
                        this.getLogs().add(log);
                        found++;
                        if (getId() == null) {
                            this.setId(tmp.getLong("CacheID"));
                        }
                        if (found>=max) {
                        	break;
                        }
                    }
                    JSONObject pageinfo = obj.getJSONObject("pageInfo");
                    if (pageinfo != null) {
                        if (pageinfo.has("totalRows")) {
                            int totalLogs = pageinfo.getInt("totalRows");
                            if (totalLogs > logs.size()) {
                                // there are still more pages to return
                                res = true;
                            }
                        }
                    }
                }
            } else {
                LOGGER.error("Non-JSON Object: {} ",o);
            }
            if (found>=max) {
            	break;
            }
        }
        return res;
    }

    /**
     * Parse a cache from it's details page
     * @param contents the Document to parse
     * @param login the login to connect to geocaching.com
     * @throws ParseException in case it fails
     */
    // CHECKSTYLE:OFF:JavaNCSS FOR 1 LINE
    private void parse(Document contents, Login login) throws ParseException {
        populateOwnerAndHiddenDate(contents, login);
        if (this.isUnavailableToUs()) {
            // Only for premium and we're not premium
            // Parse premium only
            Element sizeEl = contents.select("#ctl00_ContentBody_lblSize + span").first();
            if (sizeEl != null) {
                setCacheSize(CacheSize.valueOf(sizeEl.text().toUpperCase().replace(' ', '_')));
            }
            Element typeEl = contents.select("img#ctl00_ContentBody_uxWptTypeImage").first();
            if (typeEl != null) {
                setCacheType(StringUtil.getFirstGroup(typeEl.attr("src"), "/([^\\./]*)\\."));
            }
            Element difficultyEl = contents.select("#ctl00_ContentBody_lblDifficulty + span").first();
            if (difficultyEl != null) {
                setDifficultyRating(Double.parseDouble(difficultyEl.text().replaceAll("[^0-9.]", "")));
            }
            Element terrainEl = contents.select("#ctl00_ContentBody_lblTerrain + span").first();
            if (terrainEl != null) {
                setTerrainRating(Double.parseDouble(terrainEl.text().replaceAll("[^0-9.]", "")));
            }
            Element nameEl = contents.select("#ctl00_divContentMain H1").first();
            if (nameEl != null) {
                setName(nameEl.text());
            }
            Element favouriteEl = contents.select("#ctl00_ContentBody_lblFavoritePoints + span").first();
            if (favouriteEl != null) {
                setFavourited(Integer.parseInt(favouriteEl.text().replaceAll("[^0-9]", "")));
            }
        } else {
            // Readable cache
            Element sizeEl = contents.select("#ctl00_ContentBody_size img").first();
            if (sizeEl != null) {
                String sizeContents = StringUtil.getFirstGroup(sizeEl.attr("src"), "/([[^\\./]*]*).gif");
                setCacheSize(CacheSize.valueOf(sizeContents.toUpperCase()));
            }
            Element typeEl = contents.select("img[src~=(?i).*/images/wpttypes/[^\\./]*.gif").first();
            if (typeEl != null) {
                setCacheType(StringUtil.getFirstGroup(typeEl.attr("src"), "/([[^\\./]*]*).gif"));
            }
            Element difficultyEl = contents.select("#ctl00_ContentBody_uxLegendScale img").first();
            if (difficultyEl != null) {
                setDifficultyRating(
                        Double.parseDouble(StringUtil.getFirstGroup(difficultyEl.attr("src"), "/([[^\\./]*]*).gif")
                                .replace('_', '.').replaceAll("[^0-9\\.]", "")));
            }
            Element terrainEl = contents.select("#ctl00_ContentBody_Localize12 img").first();
            if (terrainEl != null) {
                setTerrainRating(
                        Double.parseDouble(StringUtil.getFirstGroup(terrainEl.attr("src"), "/([[^\\./]*]*).gif")
                                .replace('_', '.').replaceAll("[^0-9\\.]", "")));
            }
            Element locationEl = contents.select("#ctl00_ContentBody_Location").first();
            if (locationEl != null) {
                setLocationDescription(locationEl.text().replaceFirst("^[^ ]* ", ""));
            }

            // get name
            Element nameEl = contents.select("#ctl00_ContentBody_CacheName").first();
            if (nameEl != null) {
                setName(nameEl.text());
            }
            Element favouriteEl = contents.select("#uxFavContainerLink span.favorite-value").first();
            if (favouriteEl != null) {
                setFavourited(Integer.parseInt(favouriteEl.text().trim()));
            }
            Element shortDescriptionEl = contents.select("#ctl00_ContentBody_ShortDescription").first();
            if (shortDescriptionEl != null) {
                setShortDescription(shortDescriptionEl.html());
            }

            Element longDescriptionEl = contents.select("#ctl00_ContentBody_LongDescription").first();
            if (longDescriptionEl != null) {
                setLongDescription(longDescriptionEl.html());
            }
            Element hintEl = contents.select("#div_hint").first();
            if (hintEl != null && hintEl.hasText()) {
                String decodedHint = Tools.rot13(hintEl.text());
                setHint(decodedHint);
            } else {
                setHint("");
            }
            Element logVisitLink = contents.select("a[href~=.*/seek/log.aspx\\?ID=.*]").first();
            if (logVisitLink != null) {
                setId(Long.parseLong(StringUtil.getFirstGroup(logVisitLink.attr("href"), "ID=([0-9]+)")));
            }
            Element bookmarkLink = contents.select("a[href~=.*/bookmarks/mark.aspx\\?guid=.*]").first();
            if (bookmarkLink != null) {
                setGuid(StringUtil.getFirstGroup(bookmarkLink.attr("href"), "guid=([^&]+)&"));
            }
            Element coordEl = contents.select("#uxLatLon").first();
            if (coordEl != null) {
                this.setEstimateLevel(-1);
                setLocation(new Location(coordEl.text()));
            }
            populateWaypoints(contents);
            // populateTrackables(contents);
            populateAttributes(contents);

            try {
                populateInitialLogs(contents, login, Integer.MAX_VALUE);
            } catch (IOException e) {
                e.printStackTrace();
            }
            setFoundByUser(contents.select("#ctl00_ContentBody_GeoNav_foundStatus img[src~/2.png]").first() != null);

            setArchived(contents.select("li:contains(This cache has been archived)").first() != null);
        }
    }

    /**
     * parse the initial logs from the original cache details
     * @param contents the cache details page
     * @param login the login to connect to geocaching.com
     * @throws ParseException is case the received HTML is invalid
     * @throws IOException in case the json is not correct
     */
    private void populateInitialLogs(Document contents, Login login, int max) throws IOException, ParseException {
        for (Element scripts : contents.select("script")) {
            String initialLogs = StringUtil.getFirstGroup(scripts.html(), "(?is)initalLogs=(\\{.+?\\});");
            if (initialLogs != null) {
                try {
                    parseJsonLogs(initialLogs, login, max);
                } catch (JSONException e) {
                	LOGGER.error("Problem reading logs for cache " + this.getCacheCode(), e);
                    throw new IOException(e);
                }
            }
        }
    }

    private void setFound(Boolean found) {
        this.found = found;
    }

    public Boolean isFound() {
        return this.found;
    }

    public void setShortCutURL(String url) {
        this.shortCutURL = url;
    }

    public void setFoundByUser(boolean found) {
        this.foundByUser = found;
    }

    public boolean isFoundByUser() {
        return this.foundByUser;
    }

    /**
     * Populate the attribute from the cache details page
     * @param contents the cache details page
     */
    private void populateAttributes(Document contents) {
        for (Element attributeEl : contents.select("ctl00_ContentBody_detailWidget img[src~=/images/attributes/.*]")) {
            String attributeString = StringUtil.getFirstGroup(attributeEl.attr("src"), "/([^/]+).gif");
            if (!"attribute-blank".equals(attributeString)) {
                Attribute attr = Attribute.valueOf(attributeString.toUpperCase().replace("[^A-Z]", "_"));
                if (attr != null) {
                    this.attributes.add(attr);
                }
            }
        }
    }

    /**
     * Parse the waypoints, if there are any.
     * 
     * @param contents the Document to parse
     */
    private void populateWaypoints(Document contents) {
        for (Element waypointRow : contents.select("#ctl00_ContentBody_Waypoints tbody tr")) {
            Elements cells = waypointRow.select("td");
            if (cells.size() >= 7) {
                WayPoint wp = new WayPoint();
                wp.setPartOf(this);
                wp.setViewable(cells.get(1).select("img").attr("title"));
                wp.setType(cells.get(2).select("img").attr("title"));
                wp.setPrefix(cells.get(3).select("span").text());
                wp.setLookup(cells.get(4).select("span").text());
                wp.setName(cells.get(5).select("a").text());
                String waypointLocation = StringUtil.trimWithNbsp(cells.get(6).text());
                if (!"???".equals(waypointLocation)) {
                    wp.setLocation(new Location(waypointLocation));
                }
                this.waypoints.add(wp);
                Element noteRow = waypointRow.nextElementSibling();
                Elements noteCells = noteRow.select("td");
                if (noteCells.size() >= 3) {
                	wp.setNote(noteCells.get(2).text());
                }
            }
        }
    }

    /**
     * Populate the owner and the hidden date
     * @param contents the Document to parse
     * @param login the Login to use
     * @throws ParseException in case the document can not be parsed
     */
    // CHECKSTYLE:OFF:JavaNCSS FOR 1 LINE
    private void populateOwnerAndHiddenDate(Document contents, Login login) throws ParseException {
        Element hiderLink = contents.select("div#ctl00_ContentBody_mcd1 a").first();
        if (hiderLink != null) {
            String hiderUrl = hiderLink.attr("href").replaceAll("&.*", "");
            String hiderName = hiderLink.text();
            this.setHider(new Cacher(hiderName, hiderUrl));
        }

        Element hiddenDateElement = contents.select("div#ctl00_ContentBody_mcd2").first();
        if (hiddenDateElement != null) {
            String hiddenDateText = hiddenDateElement.text();
            hiddenDateText = hiddenDateText.replaceFirst(".*:", "");
            SimpleDateFormat sdf = new SimpleDateFormat(login.getDateFormat(), Locale.ENGLISH);
            sdf.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles"));
            Date d = sdf.parse(hiddenDateText.trim());
            this.setPlacedAt(d.getTime());
        }

        Element ownerLink = contents.select("a[href~=(?i).*/seek/nearest\\.aspx\\?u=.*]").first();
        if (ownerLink != null) {
            try {
                String ownerName = URLDecoder.decode(StringUtil.getFirstGroup(ownerLink.attr("href"), ".*u=(.*)"),
                        "UTF-8");
                if (hiderLink != null && getHider().getName().equals(ownerName)) {
                    setOwner(getHider());
                } else {
                    setOwner(new Cacher(ownerName));
                }
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException(e);
            }
        }

        this.setUnavailableToUs(hiderLink == null || hiddenDateElement == null || ownerLink == null);
        if (isUnavailableToUs()) {
            this.setPremiumonly(true);
            Element ownerNameEl = contents.select("#ctl00_ContentBody_uxCacheBy").first();
            if (ownerNameEl != null) {
                // member-only cache, and we're not logged in as premium member
                String ownerName = ownerNameEl.text().replace("A cache by ", "");
                this.setHider(new Cacher(ownerName));
                setOwner(getHider());
            }
        }
    }

    public void setUnavailableToUs(boolean b) {
        this.unavailabletous = b;
    }

    public boolean isUnavailableToUs() {
        return this.unavailabletous;
    }

    /**
     * Use the map request code to populate a limited subset of the cache data.
     * This is the same info that is displayed when a cache is clicked in the
     * website map.
     * 
     * If the map request code is not set this method will fail.
     * @param login the login to use to connect to the site
     * 
     * @throws IOException in case the site can not be contacted
     * @throws JSONException in case the received json is invalid
     * @throws ParseException is case the received HTML is invalid
     */
    public void populateFromMap(Login login) throws IOException, JSONException, ParseException {
        if (this.mapRequestCode == null) {
            throw new IllegalStateException("Map request code not set.");
        }
        
        // CHECKSTYLE:OFF:StringLiteral
        // CHECKSTYLE:OFF:MagicNumber FOR 4 LINES

        StringBuilder request = new StringBuilder(
                "https://tiles0" + RandomUtils.nextInt(1, 5) + ".geocaching.com/map.details?i=");
        request.append(this.mapRequestCode);
        request.append("&k=");
        request.append(login.getMagic());
        request.append("&st=");
        request.append(login.getSessionToken());
        request.append("&_=");
        request.append(Long.toString(System.currentTimeMillis()));
        Connection connection = login.getConnection(request.toString());
        connection.method(Method.GET);
        connection.ignoreContentType(true);
        Response response = connection.execute();
        String contents = response.body();
        if (contents != null) {
            contents = contents.trim();
            // de-tokenise the string
            JSONTokener jtok = new JSONTokener(contents);
            if (jtok.more()) {
                JSONObject o = (JSONObject) jtok.nextValue();

                String status = o.getString("status");
                if (!"success".equals(status)) {
                    LOGGER.error("Warning: status was \"" + status + "\"");
                }
                JSONArray array = o.getJSONArray("data");
                JSONObject obj = array.getJSONObject(0);
                this.setCacheCode(obj.getString("gc"));
                this.setGuid(obj.getString("g"));
                this.setDisabled(!obj.getBoolean("available"));
                this.setArchived(obj.getBoolean("archived"));
                this.setPremiumonly(obj.getBoolean("subrOnly"));
                this.setFavourited(obj.getInt("fp"));
                JSONObject difficulty = obj.getJSONObject("difficulty");
                this.setDifficultyRating(difficulty.getDouble("text"));
                JSONObject terrain = obj.getJSONObject("terrain");
                this.setTerrainRating(terrain.getDouble("text"));
                String hiddenDateString = obj.getString("hidden");
                Date hiddenDate = login.parseDate(hiddenDateString);
                this.setPlacedAt(hiddenDate.getTime());
                JSONObject container = obj.getJSONObject("container");
                String containerType = container.getString("text");
                this.setCacheSize(CacheSize.parse(containerType));
                JSONObject type = obj.getJSONObject("type");
                int typeValue = type.getInt("value");
                this.setCacheType(typeValue);
                JSONObject owner = obj.getJSONObject("owner");
                Cacher cowner = new Cacher(owner.getString("text"));
                cowner.setProfilePage("https://www.geocaching.com/profile/?guid=" + owner.getString("value"));
                this.setOwner(cowner);
                this.setHider(cowner);
            }
        }
        // CHECKSTYLE:ON:StringLiteral
    }

    public String getCacheCode() {
        return cacheCode;
    }

    public void setCacheCode(String cacheCode) {
        this.cacheCode = cacheCode;
    }

    public long getPlacedAt() {
        return placedAt;
    }

    public void setPlacedAt(long placedAt) {
        this.placedAt = placedAt;
    }

    public CacheType getCacheType() {
        return cacheType;
    }

    public void setCacheType(CacheType cacheType) {
        this.cacheType = cacheType;
    }

    /**
     * Set the cache type based on the cache image index.
     * 
     * @param cacheImageIndex the cache index
     */
    public void setCacheType(int cacheImageIndex) {
        // CHECKSTYLE:OFF:MagicNumber FOR 13 LINES
        if (cacheImageIndex == 3653) {
            setCacheType(CacheType.LOST_AND_FOUND_EVENT);
        } else if (cacheImageIndex == 1858) {
            setCacheType(CacheType.WHERIGO);
        } else if (cacheImageIndex == 137) {
            setCacheType(CacheType.EARTH_CACHE);
        } else if (cacheImageIndex == 453) {
            setCacheType(CacheType.MEGA_EVENT);
        } else if (cacheImageIndex == 1304) {
            setCacheType(CacheType.GPSADVENTURE);
        } else {
            setCacheType(CacheType.values()[cacheImageIndex - 2]);
        }
    }

    /**
     * Setter for the cacheTylpe
     * @param cacheType the cache type as string
     */
    public void setCacheType(String cacheType) {
        // CHECKSTYLE:OFF:StringLiteral FOR 1 LINE
        if (cacheType.equals("earthcache")) {
            setCacheType(CacheType.EARTH_CACHE);
        } else {
            setCacheType(Integer.parseInt(cacheType));
        }
    }

    public CacheSize getCacheSize() {
        return cacheSize;
    }

    public void setCacheSize(CacheSize cacheSize) {
        this.cacheSize = cacheSize;
    }

    public double getDifficultyRating() {
        return difficultyRating;
    }

    public void setDifficultyRating(double difficultyRating) {
        this.difficultyRating = difficultyRating;
    }

    public double getTerrainRating() {
        return terrainRating;
    }

    public void setTerrainRating(double terrainRating) {
        this.terrainRating = terrainRating;
    }

    public int getFavourited() {
        return favourited;
    }

    public void setFavourited(int favourited) {
        this.favourited = favourited;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean b) {
        this.disabled = b;
    }

    public String getLocationDescription() {
        return locationDescription;
    }

    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }

    public List<CacheLog> getLogs() {
        return logs;
    }
    
    public void limitLogs(int maxLogs) {
    	if (logs.size()>maxLogs) {
    		logs.subList(maxLogs,logs.size()).clear();
    	}
    }

    public Set<Attribute> getAttributes() {
        return attributes;
    }

    public List<WayPoint> getWaypoints() {
        return waypoints;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public boolean isPremiumonly() {
        return premiumonly;
    }

    public void setPremiumonly(boolean premiumonly) {
        this.premiumonly = premiumonly;
    }

    /**
     * Set the string used to request short info via the map interface.
     * 
     * @param magicString the magicString
     */
    public void setMapRequestCode(String magicString) {
        this.mapRequestCode = magicString;
    }

    /**
     * Set the zoom level at which the location was estimated.
     * 
     * @param zoom the zoom level
     */
    public void setEstimateLevel(int zoom) {
        this.estimatelevel = zoom;
    }

    /**
     * Get the zoom level at which the location was estimated. If the zoom level
     * is negative the location is accurate.
     * 
     * @return the zoom level
     */
    public int getEstimateLevel() {
        return this.estimatelevel;
    }

    public void setTrackableCount(int trackables) {
        this.trackableCount = trackables;
    }

    public int getTrackableCount() {
        return this.trackableCount;
    }

    /**
     * Set the last found date
     * Automatically sets the found flag as well (lastFoundDate != null)
     * @param time the date (as long)
     */
    public void setLastFoundDate(Long time) {
        lastFoundDate = time;
        setFound(time != null);
    }

    public Long getLastFoundDate() {
        return lastFoundDate;
    }
}