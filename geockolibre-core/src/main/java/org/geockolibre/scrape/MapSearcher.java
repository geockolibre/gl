package org.geockolibre.scrape;

/*
 * #%L
 * This file is part of GEOCKOLIBRE
 * ==================================================
 * Copyright (C) 2016 GEOCKOLIBRE
 * ==================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.geockolibre.scrape.util.CacheEstimator;
import org.geockolibre.scrape.util.ImageToolbox;
import org.geockolibre.scrape.util.MaskSet;
import org.geockolibre.scrape.util.MaskSet.MaskEntry;
import org.geockolibre.scrape.util.StringUtil;
import org.geockolibre.util.json.JSONArray;
import org.geockolibre.util.json.JSONException;
import org.geockolibre.util.json.JSONObject;
import org.geockolibre.util.json.JSONTokener;
import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.HttpStatusException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.LoggerFactory;

/**
 * 
 * Object that searches for caches using the map.
 * 
 */
public class MapSearcher extends Searcher {
    private String sessionToken;
    private String magic;
    private String subscriberType;
    private final int EDGE_THICKNESS_BIG = 7;
    private final int EDGE_THICKNESS_MID = 7;
    private final int EDGE_THICKNESS_SML = 3;
    private ImageToolbox imageToolbox;

    public MapSearcher(Login login) {
        super(login);

        MaskSet maskset = new MaskSet();
        imageToolbox = new ImageToolbox(maskset);
        try {
            maskset.setBigIconBorder(
                    ImageIO.read(MapSearcher.class.getResourceAsStream("/templates/largeiconframe.png")));
            maskset.setSmallIconBorder(
                    ImageIO.read(MapSearcher.class.getResourceAsStream("/templates/smalliconframe.png")));
            maskset.setLargeTrad(ImageIO.read(MapSearcher.class.getResourceAsStream("/templates/large/trad.png")));
            maskset.setLargeApe(ImageIO.read(MapSearcher.class.getResourceAsStream("/templates/large/ape.png")));
            maskset.setLargeLetter(ImageIO.read(MapSearcher.class.getResourceAsStream("/templates/large/letter.png")));
            maskset.setLargeMulti(ImageIO.read(MapSearcher.class.getResourceAsStream("/templates/large/multi.png")));
            maskset.setLargeEvent(ImageIO.read(MapSearcher.class.getResourceAsStream("/templates/large/event.png")));
            maskset.setLargeMega(ImageIO.read(MapSearcher.class.getResourceAsStream("/templates/large/megaevent.png")));
            maskset.setLargeCito(ImageIO.read(MapSearcher.class.getResourceAsStream("/templates/large/cito.png")));
            maskset.setLargeGpsAdv(
                    ImageIO.read(MapSearcher.class.getResourceAsStream("/templates/large/adventure.png")));
            maskset.setLargeVirtual(
                    ImageIO.read(MapSearcher.class.getResourceAsStream("/templates/large/virtual.png")));
            maskset.setLargeWebcam(ImageIO.read(MapSearcher.class.getResourceAsStream("/templates/large/webcam.png")));
            maskset.setLargeEarth(ImageIO.read(MapSearcher.class.getResourceAsStream("/templates/large/earth.png")));
            maskset.setLargeMyst(ImageIO.read(MapSearcher.class.getResourceAsStream("/templates/large/myst.png")));
            maskset.setLargeWherigo(
                    ImageIO.read(MapSearcher.class.getResourceAsStream("/templates/large/wherigo.png")));
            maskset.setLargeFound(ImageIO.read(MapSearcher.class.getResourceAsStream("/templates/large/found.png")));
            maskset.setLargeOwn(ImageIO.read(MapSearcher.class.getResourceAsStream("/templates/large/own.png")));

            maskset.setMediumEvent(ImageIO.read(MapSearcher.class.getResourceAsStream("/templates/medium/event.png")));
            maskset.setMediumFound(ImageIO.read(MapSearcher.class.getResourceAsStream("/templates/medium/found.png")));
            maskset.setMediumMulti(ImageIO.read(MapSearcher.class.getResourceAsStream("/templates/medium/multi.png")));
            maskset.setMediumMyst(ImageIO.read(MapSearcher.class.getResourceAsStream("/templates/medium/myst.png")));
            maskset.setMediumOwn(ImageIO.read(MapSearcher.class.getResourceAsStream("/templates/medium/own.png")));
            maskset.setMediumTrad(ImageIO.read(MapSearcher.class.getResourceAsStream("/templates/medium/trad.png")));
            maskset.setMediumVirtual(
                    ImageIO.read(MapSearcher.class.getResourceAsStream("/templates/medium/virtual.png")));

            maskset.setSmallMulti(ImageIO.read(MapSearcher.class.getResourceAsStream("/templates/small/multi.png")));
            maskset.setSmallTrad(ImageIO.read(MapSearcher.class.getResourceAsStream("/templates/small/trad.png")));
            maskset.setSmallMyst(ImageIO.read(MapSearcher.class.getResourceAsStream("/templates/small/myst.png")));
            maskset.setSmallVirtual(ImageIO.read(MapSearcher.class.getResourceAsStream("/templates/small/virt.png")));
            maskset.setSmallEvent(ImageIO.read(MapSearcher.class.getResourceAsStream("/templates/small/event.png")));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Load the map interface, parse data we need.
     * 
     * @throws IOException
     */
    private void getUserSession() throws IOException {
        doRandomDelay();
        Document document = login.getDocument("https://www.geocaching.com/map/default.aspx");
        for (Element scripts : document.select("script")) {
            String groups[] = StringUtil.getGroups(scripts.html(), "(?is)new Groundspeak.UserSession\\('([^']*)'[^\\)]*"
                    + "sessionToken *: *'([^']*)'[^\\)]*" + "subscriberType *: *(\\d+) *,");
            if (groups != null && groups.length == 3) {
                this.magic = groups[0];
                this.sessionToken = groups[1];
                this.subscriberType = groups[2];
                login.setMagic(this.magic);
                login.setSessionToken(this.sessionToken);
            }
        }
    }

    @Override
    public void abort() {
    	// TODO Auto-generated method stub
    }
    /**
     * Find all caches within a grid square.
     * 
     * @param loc
     *            the centre of the search.
     * @param zoom
     *            the zoom level, higher zoom covers a smaller area
     * @return an array list of caches, the caches have not been populated.
     * @throws IOException
     */
    public List<Cache> findCachesAtLocation(Location loc, int zoom) throws IOException {
        int lat = loc.getLatitudeGsJson(zoom);
        int lon = loc.getLongitudeGsJson(zoom);
        return findCachesAtGridSquare(lon - 1, lat - 1, 3, 3, zoom);
    }

    /**
     * Search all caches within a grid square using the map.
     * 
     * @param lon
     *            he horizontal grid square index.
     * @param lat
     *            the vertical grid square index.
     * @param width
     *            the width of the square to search, in number of grid squares -
     *            must be less than 5.
     * @param height
     *            the height of the square to search, in number of grid squares
     *            - must be less than 5.
     * @param zoom
     *            the zoom level - greater zoom, greater magnification.
     * @return null if the search failed on the server side.
     * @throws IOException
     */
    public List<Cache> findCachesAtGridSquare(int lon, int lat, int width, int height, int zoom) throws IOException {
        ArrayList<Cache> res = null;
        if (sessionToken == null) {
            getUserSession();
        }

        List<BufferedImage> tileImages = getTileImages(lon, lat, width, height, zoom);

        boolean[] validSquares = new boolean[width * height];
        List<CacheEstimator> foundCaches = getCacheEstimates(lon, lat, width, height, zoom, validSquares);

        BufferedImage tileImage = stitchImage(tileImages, validSquares, width, height);

        if (foundCaches != null) {
            announceCount(foundCaches.size());
            res = new ArrayList<Cache>();

            if (tileImage != null) {
                ImageIO.write(tileImage, "PNG", new File("foo.png"));
                // load caches
                for (CacheEstimator ce : foundCaches) {
                    Cache c = new Cache();
                    c.setName(ce.getName());
                    c.setMapRequestCode(ce.getMagicString());
                    if (!ce.isOverlapping()) {
                        Rectangle rect = ce.getTileImageRect();
                        int edgeThickness = getEdgeThickness(zoom);
                        // for zoom < 12, only spots
                        // for zoom < 14, small icons
                        // otherwise, large icons
                        int x1 = rect.x - edgeThickness;
                        int y1 = rect.y - edgeThickness;
                        int x2 = rect.x + rect.width + edgeThickness;
                        int y2 = rect.y + rect.height + edgeThickness;
                        x1 = x1 < 0 ? 0 : x1;
                        y1 = y1 < 0 ? 0 : y1;
                        x2 = x2 > tileImage.getWidth() ? tileImage.getWidth() : x2;
                        y2 = y2 > tileImage.getHeight() ? tileImage.getHeight() : y2;
                        BufferedImage tmp = new BufferedImage(x2 - x1, y2 - y1, BufferedImage.TYPE_INT_ARGB);
                        Graphics2D g = tmp.createGraphics();

                        g.drawImage(tileImage, 0, 0, x2 - x1, y2 - y1, x1, y1, x2, y2, null);

                        if (zoom > 13) {
                            // large icons
                            Point location = imageToolbox.getLargeIconLocation(tmp);
                            if (location != null) {
                                tmp = imageToolbox.getLargeIcon(tmp, location);
                                // all large locations are 20x20 pixels
                                // add half width and height
                                Point absLocation = new Point(location.x + x1 + 10, location.y + y1 + 10);
                                ce.setPixelLocation(absLocation);
                                c.setCacheType(imageToolbox.getType(tmp, imageToolbox.getMasks().getLargeEntries()));
                                c.setEstimateLevel(zoom);
                                c.setLocation(ce.estimateLocation());
                                // check if a particular pixel is white and
                                // opaque
                                c.setDisabled((tmp.getRGB(2, 1) != 0xFFFFFFFF));
                                res.add(c);
                                this.announceCache(c);
                            } else if (tmp.getWidth() == tmp.getHeight() && tmp.getWidth() == 26) {
                                c.setCacheType(CacheType.MULTIPLE_CACHES_AT_LOCATION);
                                c.setEstimateLevel(zoom);
                                c.setLocation(ce.estimateLocation());
                                res.add(c);
                                this.announceCache(c);
                            }
                        } else if (zoom > 11) {
                            // medium icons
                            Point location = null;
                            List<MaskEntry> smallMasks = imageToolbox.getMasks().getMediumEntries();
                            for (MaskEntry me : smallMasks) {
                                Point p = imageToolbox.findLocation(tmp, me.getMask());
                                if (p != null) {
                                    location = p;
                                    location.x += me.getMask().getWidth() / 2;
                                    location.y += me.getMask().getHeight() / 2;
                                    c.setCacheType(me.getEntry());
                                    break;
                                }
                            }
                            if (location != null) {
                                Point absLocation = new Point(location.x + x1, location.y + y1);
                                ce.setPixelLocation(absLocation);
                                c.setEstimateLevel(zoom);
                                c.setLocation(ce.estimateLocation());
                                res.add(c);
                                this.announceCache(c);
                            } else if (tmp.getWidth() == tmp.getHeight() && tmp.getWidth() == 26) {
                                c.setCacheType(CacheType.MULTIPLE_CACHES_AT_LOCATION);
                                c.setEstimateLevel(zoom);
                                c.setLocation(ce.estimateLocation());
                                res.add(c);
                                this.announceCache(c);
                            }
                        } else {
                            // small icons
                            Point location = imageToolbox.getSmallIconLocation(tmp);
                            if (location != null) {
                                tmp = imageToolbox.getSmallIcon(tmp, location);
                                // all small icons are 8x8 pixels, add half size
                                Point absLocation = new Point(location.x + x1 + 4, location.y + y1 + 4);
                                ce.setPixelLocation(absLocation);
                                c.setCacheType(imageToolbox.getType(tmp, imageToolbox.getMasks().getSmallEntries()));
                                c.setEstimateLevel(zoom);
                                c.setLocation(ce.estimateLocation());
                                res.add(c);
                                this.announceCache(c);
                            } else if (tmp.getWidth() == tmp.getHeight() && tmp.getWidth() == 18) {
                                c.setCacheType(CacheType.MULTIPLE_CACHES_AT_LOCATION);
                                c.setEstimateLevel(zoom);
                                c.setLocation(ce.estimateLocation());
                                res.add(c);
                                this.announceCache(c);
                            }
                        }

                    } else {
                        c.setCacheType(CacheType.MULTIPLE_CACHES_AT_LOCATION);
                        c.setEstimateLevel(zoom);
                        c.setLocation(ce.estimateLocation());
                        res.add(c);
                        this.announceCache(c);
                    }
                }
            }
        }
        return res;
    }

    private BufferedImage stitchImage(List<BufferedImage> tileImages, boolean[] validSquares, int width, int height) {
        BufferedImage res = new BufferedImage(width * 256, height * 256, BufferedImage.TYPE_INT_ARGB);
        Graphics2D grph = res.createGraphics();
        int squareOffset = 0;
        for (int lonoffset = 0; lonoffset < width; lonoffset++) {
            for (int latoffset = 0; latoffset < height; latoffset++) {
                if (validSquares[squareOffset]) {
                    // if this square contains actual data, render it
                    grph.drawImage(tileImages.get(squareOffset), lonoffset * 256, latoffset * 256,
                            (lonoffset + 1) * 256, (latoffset + 1) * 256, 0, 0, 256, 256, null);
                }
                squareOffset++;
            }
        }
        return res;
    }

    private int getEdgeThickness(int zoom) {
        int res = EDGE_THICKNESS_BIG;
        if (zoom < 14) {
            res = EDGE_THICKNESS_MID;
        }
        if (zoom < 12) {
            res = EDGE_THICKNESS_SML;
        }
        return res;
    }

    private List<BufferedImage> getTileImages(int lon, int lat, int width, int height, int zoom) throws IOException {
        List<BufferedImage> res = new ArrayList<BufferedImage>();
        for (int lonoffset = 0; lonoffset < width; lonoffset++) {
            for (int latoffset = 0; latoffset < height; latoffset++) {
                // get the image
                StringBuilder imageTileSearch = new StringBuilder("https://www.geocaching.com/map/map.tile?x=");
                imageTileSearch.append(lon + lonoffset);
                imageTileSearch.append("&y=");
                imageTileSearch.append(lat + latoffset);
                imageTileSearch.append("&z=");
                imageTileSearch.append(Integer.toString(zoom));
                imageTileSearch.append("&k=");
                imageTileSearch.append(magic);
                imageTileSearch.append("&st=");
                imageTileSearch.append(sessionToken);
                // no idea what ep is, possibly subscriber type
                imageTileSearch.append("&ep=");
                imageTileSearch.append(subscriberType);
                Connection connection = login.getConnection("imageTileSearch.toString()");
                connection.ignoreContentType(true);
                Response response = connection.execute();
                InputStream in = new ByteArrayInputStream(response.bodyAsBytes());
                BufferedImage image = ImageIO.read(in);
                // draw the image onto the composite image
                res.add(image);
            }
        }

        return res;
    }

    /**
     * Search the map interface for a list of cache names and approximate
     * locations in the area.
     * 
     * @param lon
     * @param lat
     * @param width
     * @param height
     * @param zoom
     * @return null if the search failed on the server side.
     * @throws IOException
     */
    private List<CacheEstimator> getCacheEstimates(int lon, int lat, int width, int height, int zoom,
            boolean[] validSquares) throws IOException {
        List<CacheEstimator> foundCaches = null;
        int squareOffset = 0;
        int count = 1;
        for (int lonoffset = 0; lonoffset < width; lonoffset++) {
            for (int latoffset = 0; latoffset < height; latoffset++) {

                String jsonContent = null;
                // query the search page
                StringBuilder locationSearch = new StringBuilder(
                        "https://tiles0" + count + ".geocaching.com/map.info?x=");
                count = (count % 4) + 1;
                locationSearch.append(lon + lonoffset);
                locationSearch.append("&y=");
                locationSearch.append(lat + latoffset);
                locationSearch.append("&z=");
                locationSearch.append(Integer.toString(zoom));
                locationSearch.append("&k=");
                locationSearch.append(magic);
                locationSearch.append("&st=");
                locationSearch.append(sessionToken);
                // no idea what ep is, possibly subscriber type:
                locationSearch.append("&ep=");
                locationSearch.append(subscriberType);
                // this is probably a timestamp:
                locationSearch.append("&_=");
                locationSearch.append(Long.toString(System.currentTimeMillis()));
                doRandomDelay();
                try {
                    Connection connection = login.getConnection(locationSearch.toString());
                    connection.method(Method.GET);
                    connection.ignoreContentType(true);
                    Response response = connection.execute();
                    jsonContent = response.body();
                    // de-tokenise the string
                    JSONTokener jtok = new JSONTokener(jsonContent);
                    try {
                        while (jtok.more()) {
                            validSquares[squareOffset] = true;
                            if (foundCaches == null) {
                                foundCaches = new ArrayList<CacheEstimator>();
                            }
                            Object o = jtok.nextValue();

                            if (o instanceof JSONObject) {
                                JSONObject obj = (JSONObject) o;
                                JSONArray keys = (JSONArray) obj.get("keys");
                                JSONObject data = (JSONObject) obj.get("data");
                                for (int keyIndex = 0; keyIndex < keys.length(); keyIndex++) {
                                    String key = keys.getString(keyIndex);
                                    if (key.length() > 0) {
                                        JSONArray jobj = data.getJSONArray(key);
                                        boolean isOverlapping = jobj.length() > 1;
                                        for (int objectIndex = 0; objectIndex < jobj.length(); objectIndex++) {
                                            JSONObject entry = jobj.getJSONObject(objectIndex);
                                            CacheEstimator ce = new CacheEstimator(entry.getString("n"));
                                            String[] parts = StringUtil.getGroups(key, "([0-9]+), *([0-9]+)");
                                            int x = Integer.parseInt(parts[0].trim()) + lonoffset * 64;
                                            int y = Integer.parseInt(parts[1].trim()) + latoffset * 64;
                                            if (!foundCaches.contains(ce)) {
                                                foundCaches.add(ce);
                                                ce.setMagicString(entry.getString("i"));
                                                ce.setGridSquare(new Point(lon, lat));
                                                ce.setZoomlevel(zoom);
                                            } else {
                                                ce = foundCaches.get(foundCaches.indexOf(ce));
                                            }
                                            ce.addPoint(new Point(x, y));
                                            if (!ce.isOverlapping()) {
                                                ce.setOverlapping(isOverlapping);
                                            }
                                        }
                                    }
                                }
                            } else {
                                LoggerFactory.getLogger(MapSearcher.class).error("Non-JSON Object:" + o);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } catch (HttpStatusException e) {
                    // No content
                }
                squareOffset++;
            }
        }
        return foundCaches;
    }
}
