GEOCKOLIBRE
===========

Geockolibre is a fork of PQLibre.

After noticing that PQLibre no longer works, and the PQLibre project seemed to
be dead, I decided to take matters into my own hands and create fork of the 
PQLibre project.

Geockolibre (Merge of Geocaching, Gecko and Libre) requires Java 8 (and javaFX) 
to run correctly.

Downloads are available from the wiki: https://gitlab.com/geockolibre/gl/wikis/home

Geockolibre is an advanced geocache search tool. 

It allows all geocache members to easily search and download multiple caches.
Geockolibre is free and open source and should run on Windows, Linux and Mac.

If you don't know what geocaching is, please visit https://www.geocaching.com



